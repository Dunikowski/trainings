window.addEventListener('DOMContentLoaded', () => {

    document.querySelector('#submit').addEventListener('click', () => {
        const quantity = document.querySelector('input[name="num_people"]').value;
        const bundleId = document.querySelector('input[name="id_bundle"]').value;
        if (isLogged == 1) {
            
         t_addToCartBundle(bundleId, quantity);
        } else {
            var modal = document.getElementById("myModal");
			//var btn = document.getElementById("submit");
			var span = document.getElementsByClassName("close")[0];
			// When the user clicks on the button, open the modal
			$( "#myModal" ).addClass( "bl" );
				var textModal = document.getElementById("head-title-modal");
					textModal.innerHTML= "Zaloguj się lub załóż konto aby dodać pakiet do koszyka";
					  
			// When the user clicks on <span> (x), close the modal
				span.onclick = function() {
					  $( "#myModal" ).addClass( "none" );
				}	
        }
    });
   
    function t_addToCartBundle(id, qty) {


        var formData = new FormData();
        formData.append('method', 't_addToCartBundle');
        formData.append('id', id);
        formData.append('qty', qty);
        
        fetch(bundle_ajaxurl, {
                method: 'post',
                body: formData
            })
            .then(res => res.text())
            .then(res => {
                if (res == 'add_to_cart_ok') {

                   var modal = document.getElementById("myModal");
					//var btn = document.getElementById("submit");
					var span = document.getElementsByClassName("close")[0];

					// When the user clicks on the button, open the modal
					var textModal = document.getElementById("head-title-modal");
						textModal.innerHTML= "Pomyślnie dodano do koszyka pakiet:";
						var name = document.getElementById("name").innerHTML;
						var nameAdd = document.getElementById("names-add").innerHTML = name;
					  modal.style.display = "block";
			
					// When the user clicks on <span> (x), close the modal
					span.onclick = function() {
					  modal.style.display = "none";
					}
					// When the user clicks anywhere outside of the modal, close it
					window.onclick = function(event) {
					  if (event.target == modal) {
						modal.style.display = "none";
					  }
					} 
                } else if (res == 'different_payment') {
					window.location.reload(true);
                var modal = document.getElementById("myModal");
					//var btn = document.getElementById("submit");
					var span = document.getElementsByClassName("close")[0];

					// When the user clicks on the button, open the modal
					var textModal = document.getElementById("head-title-modal");
						textModal.innerHTML= "W koszyku znajdują się pakiety z inną metodą płatności"
					  modal.style.display = "block";
			
					// When the user clicks on <span> (x), close the modal
					span.onclick = function() {
					  modal.style.display = "none";
					}
					// When the user clicks anywhere outside of the modal, close it
					window.onclick = function(event) {
					  if (event.target == modal) {
						modal.style.display = "none";
					  }
					} 
         
                } else {
                    console.log('k',res);
                
                }
            })
            .catch(error => console.dir("Błąd: ", error));
    }

});