window.addEventListener('DOMContentLoaded', () => {

    cartSumEl = document.getElementById('cartSum');
    cartSum = document.querySelectorAll('.cart-training .price').forEach((el) => {
        el.innerHTML;
    });

    deleteBtn = document.querySelectorAll('.trainingCartDelete');
    addBtn = document.querySelectorAll('.trainingCartQtyUp');
    minusBtn = document.querySelectorAll('.trainingCartQtyDown');

    deleteBtn.forEach(btn => {
        btn.addEventListener('click', e => {
            const id = e.target.closest('.cart-training').getAttribute('data-id_training');
            const id_b = e.target.closest('.cart-training').getAttribute('data-id_bundle');
            const id_c = e.target.closest('.cart-training').getAttribute('data-id_cart');
            const currPrice = parseFloat(e.target.closest('.cart-training').querySelector('.price').innerHTML);
			
            if(id){
               
                t_deleteFromCart(id, currPrice, id_c);
            } else if(id_b){
               
                b_deleteFromCart(id_b, currPrice,id_c);
            }            
        })
    });

    addBtn.forEach(btn => {
        btn.addEventListener('click', e => {
           
            const id = e.target.closest('.cart-training').getAttribute('data-id_training');
            const id_b = e.target.closest('.cart-training').getAttribute('data-id_bundle');
            const currQty = e.target.closest('.cart-training').querySelector('.quantity');
            const currPrice = e.target.closest('.cart-training').querySelector('.price');
            const singlePrice = parseFloat(e.target.closest('.cart-training').querySelector('.single-price').innerHTML);
           
            if(id){
                t_updateCart(id, 1, currQty, currPrice, singlePrice);
            } else if(id_b){
               
                b_updateCart(id_b, 1, currQty, currPrice, singlePrice);
            } 
        })
    });

    minusBtn.forEach(btn => {
        btn.addEventListener('click', e => {
            
            const id = e.target.closest('.cart-training').getAttribute('data-id_training');
            const id_b = e.target.closest('.cart-training').getAttribute('data-id_bundle');
            const currQty = e.target.closest('.cart-training').querySelector('.quantity');
            const currPrice = e.target.closest('.cart-training').querySelector('.price');
            const singlePrice = parseFloat(e.target.closest('.cart-training').querySelector('.single-price').innerHTML);
            if(id){
               
                if (parseInt(currQty.innerHTML) > 1) t_updateCart(id, -1, currQty, currPrice, singlePrice); 
            } else if(id_b){
               
                if (parseInt(currQty.innerHTML) > 1) b_updateCart(id_b, -1, currQty, currPrice, singlePrice); 
            }
            
        })
    });
    
     function t_deleteFromCart(id, currPrice, id_c) {
         const formData = new FormData();
         formData.append('method', 't_deleteFromCart');
         formData.append('id', id);
         formData.append('id_cart', id_c);
         fetch(training_ajaxurl, {
                 method: 'post',
                 body: formData
             })
         
             .then(res => {
                cartSumEl.innerHTML = (parseFloat(cartSumEl.innerHTML) - currPrice).toFixed(2)
                document.querySelectorAll(`tr[data-id_training="${id}"]`).forEach(el => el.remove());
				document.querySelectorAll(`div[data-id_training="${id}"]`).forEach(el => el.remove());
         });
     }
     function b_deleteFromCart(id, currPrice,id_c ) {
        const formData = new FormData();
        formData.append('method', 'b_deleteFromCart');
        formData.append('id', id);
        formData.append('id_cart', id_c);
        fetch(training_ajaxurl, {
                method: 'post',
                body: formData
            })
            .then(res => {
				cartSumEl.innerHTML = (parseFloat(cartSumEl.innerHTML) - currPrice).toFixed(2)
				document.querySelectorAll(`tr[data-id_bundle="${id}"]`).forEach(el => el.remove());
				document.querySelectorAll(`div[data-id_bundle="${id}"]`).forEach(el => el.remove());
        });
    }
    function t_updateCart(id, qty, currQty, currPrice, singlePrice) {
        const formData = new FormData();
        formData.append('method', 't_updateCart');
        formData.append('id', id);
        formData.append('qty', qty);
        
        fetch(training_ajaxurl, {
                method: 'post',
                body: formData
            })
            .then(res => res.text())
            .then(res => {
                console.log('update',res);
                const participantsList = document.querySelector(`div[data-training-list="${id}"]`);
                const lastEl = participantsList.lastElementChild;
                const participantsListItems = participantsList.querySelectorAll('.training-list-item');
                // increment participants number
                currQty.innerHTML = parseInt(currQty.innerHTML) + qty;

                if (qty > 0) {
                    currPrice.innerHTML = (parseFloat(currPrice.innerHTML) + singlePrice).toFixed(2);
                    cartSumEl.innerHTML = (parseFloat(cartSumEl.innerHTML) + singlePrice).toFixed(2);
                } else {
                    currPrice.innerHTML = (parseFloat(currPrice.innerHTML) - singlePrice).toFixed(2);
                    cartSumEl.innerHTML = (parseFloat(cartSumEl.innerHTML) - singlePrice).toFixed(2)
                }
                // check if new quantity is bigger or lower than participants list length
                const listLength = participantsListItems.length;

                if (parseInt(currQty.innerHTML) < listLength) {
                    lastEl.remove();
                } else {
                    const newEl = lastEl.cloneNode(true);
                    const searchNum = newEl.getAttribute('data-participant');
                    const newNum = parseInt(searchNum) + 1;

                    newEl.querySelectorAll('input').forEach(el => {
                        el.name = el.name.replace(`[${searchNum}]`, `[${newNum}]`);
                        el.value = '';
                    });
                    newEl.querySelectorAll('label').forEach(el => {
                        const newAttr = el.getAttribute('for').replace(`[${searchNum}]`, `[${newNum}]`);
                        el.setAttribute('for', newAttr);
                    });
                    newEl.querySelector('.participant-num').innerHTML = parseInt(newEl.querySelector('.participant-num').innerHTML) + 1;
                    newEl.setAttribute('data-participant', parseInt(searchNum) + 1);
                    participantsList.append(newEl);
                }
            });
    }
    function b_updateCart(id, qty, currQty, currPrice, singlePrice) {
        const formData = new FormData();
        formData.append('method', 'b_updateCart');
        formData.append('id', id);
        formData.append('qty', qty);
        console.log('articipantsList',id);
        fetch(training_ajaxurl, {
                method: 'post',
                body: formData
            })
            .then(res => res.text())
            .then(res => {
                console.log('update',res);
                const participantsList = document.querySelector(`div[data-bundle-list="${id}"]`);
              
                const lastEl = participantsList.lastElementChild;
                const participantsListItems = participantsList.querySelectorAll('.training-list-item');
                // increment participants number
                currQty.innerHTML = parseInt(currQty.innerHTML) + qty;

                if (qty > 0) {
                    currPrice.innerHTML = (parseFloat(currPrice.innerHTML) + singlePrice).toFixed(2);
                    cartSumEl.innerHTML = (parseFloat(cartSumEl.innerHTML) + singlePrice).toFixed(2);
                } else {
                    currPrice.innerHTML = (parseFloat(currPrice.innerHTML) - singlePrice).toFixed(2);
                    cartSumEl.innerHTML = (parseFloat(cartSumEl.innerHTML) - singlePrice).toFixed(2)
                }
                // check if new quantity is bigger or lower than participants list length
                const listLength = participantsListItems.length;

                if (parseInt(currQty.innerHTML) < listLength) {
                    lastEl.remove();
                } else {
                    const newEl = lastEl.cloneNode(true);
                    const searchNum = newEl.getAttribute('data-participant');
                    const newNum = parseInt(searchNum) + 1;

                    newEl.querySelectorAll('input').forEach(el => {
                        el.name = el.name.replace(`[${searchNum}]`, `[${newNum}]`);
                        el.value = '';
                    });
                    newEl.querySelectorAll('label').forEach(el => {
                        const newAttr = el.getAttribute('for').replace(`[${searchNum}]`, `[${newNum}]`);
                        el.setAttribute('for', newAttr);
                    });
                    newEl.querySelector('.participant-num').innerHTML = parseInt(newEl.querySelector('.participant-num').innerHTML) + 1;
                    newEl.setAttribute('data-participant', parseInt(searchNum) + 1);
                    participantsList.append(newEl);
                }
            });
    }


});