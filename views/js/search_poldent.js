$(document).ready(function(){

let SearchPoldent = {

    $this:{},
    pagination:{},
    page: 0,
    name: '',
    max:0,
    active_page:1,
    changeActive : function(pagination, _this, name, page, original_query, selected){

        pagination.find('>li').removeClass('active current');
        _this.closest('li').addClass('active current');
        
        SearchPoldent.getSearch(name, page, original_query, selected);
     
            SearchPoldent.active_page = page;
        
    },
    getSearch: function(name, page, original_query, selected = '') {

        var formData = new FormData();
        formData.append('method', 'search_poldent');
        formData.append('name', name);
        formData.append('page', page);
        formData.append('original_query', original_query);

        if(selected != ''){
            formData.append('selected', selected);
        }
        
        fetch(search_poldent_ajaxurl, {
                method: 'post',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
            
                  const className = '.w-poldent-'+ data[0].name;
                
                if(data.length > 0){

                  switch(data[0].name){
                    
                      case 'trainings':
                           $(className + ' > .w-items .w-content').empty().append(SearchPoldent.createTrainings(data[0].results));
                          break;

                      case 'blog':
                          $(className + ' > .w-items .sdsblog-box-content').empty().append(SearchPoldent.createBlog(data[0].results));
                          break;

                      case 'products':
                          $(className + ' > .w-items > .product_list').empty().append(SearchPoldent.createProducts(data[0].results));
                          break;
                  }
               }  
            })
            .catch(error => {
                console.dir("Błąd: ", error);
               
            });
    },
    changeNumber : function(button){

        let main_buttons = SearchPoldent.pagination.find("[data-page]");
        if(button.length > 0){

            if(button.hasClass('pagination_next')){

               let nb = $(main_buttons).last().attr("data-page");
               
               if(SearchPoldent.max - nb > 0) {
                SearchPoldent.updateButtons(main_buttons);
               } 

            } else if(button.hasClass('pagination_previous')){

                let nb = $(main_buttons).first().attr("data-page");
               
                if(nb - 1 > 0) {
                 SearchPoldent.updateButtons(main_buttons,false);
                } 
            }
            SearchPoldent.updateActive();
            SearchPoldent.updateDisabled(button);
        }
        return;
    },
    updateActive: function(){
        SearchPoldent.pagination.find('>li').removeClass('active current');
        SearchPoldent.pagination.find("[data-page]").each(function(){
            $this = $(this);

            if($this.attr("data-page") == SearchPoldent.active_page){
                $this.addClass('active current');
            }
        })
    },
    updateButtons: function(main_buttons = false, add = true){

        if(main_buttons){

            if(add){
                $(main_buttons).each(function(){
                    $this = $(this);
                    const page = parseInt($this.attr("data-page")) + 1;
                    $this.attr('data-page',page);
                    const span = $this.find('> span > span');
                    $(span).text(page);
                });
            } else {
                $(main_buttons).each(function(){
                    $this = $(this);
                    const page = parseInt($this.attr("data-page")) - 1;
                    $this.attr('data-page',page);
                    const span = $this.find('> span > span');
                    $(span).text(page);
                }); 
            }
       }
    },
    resetButtons: function(main_buttons = false){

        if(main_buttons){
            $(main_buttons).each(function(index){
                $this = $(this);
                const page = index + 1;
                $this.attr('data-page',page);
                const span = $this.find('> span > span');
                $(span).text(page);
            });  
       }
    },
    updateDisabled: function(button){

        const main_buttons = SearchPoldent.pagination.find("[data-page]");
        const first = main_buttons.first();
        const last = $(main_buttons).last();
       
        if(button.hasClass('pagination_next')){
            
             if(last.attr("data-page") == SearchPoldent.max){
                 button.addClass("disabled");
             } else {
                 button.removeClass("disabled");
             }

             if(first.attr("data-page") > 1){
                SearchPoldent.pagination.find('.pagination_previous').removeClass("disabled");
             }

        } else {

            if(first.attr("data-page") == 1){
                button.addClass("disabled");
            } else {
                button.removeClass("disabled");
            }

            if(last.attr("data-page") < SearchPoldent.max){
                SearchPoldent.pagination.find('.pagination_next').removeClass("disabled");
             }
        }
        
    },
    createBlog: function(a = []){

        let result = '';
        a.forEach(element => {
            result +=`
                        <div id="sds_blog_post" class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 col-12 ">
                            <span class="news_module_image_holder news_home_image_holder">
                            <a href="${base_dir}blog/${element.link_rewrite}.html">
                            `;

                            if(element.id_smart_blog_post && element.link_rewrite){
                                result +=` 
                                <img class="replace-2x img-responsive" src="${base_dir}blog/${element.id_smart_blog_post}-home-default/${element.link_rewrite}.jpg" alt="${element.link_rewrite}" title="${element.link_rewrite}"   itemprop="image" />
                                `;
                            }
                            
                result +=` 
                        </a> 
                            </span>
                            <h4 class="sds_post_title sds_post_title_home">${element.meta_title}</h4>
                            <span class="sds_post_title_home read_more"><a href="${base_dir}blog/${element.link_rewrite}.html"> Czytaj całość </a></span> 						    
                        </div>
                    ` ; 
        });

        return result;
    },
    createTrainings: function(a = []){

        let result = '';
        a.forEach(element => {
            result +=`

                        <div class="wrap upcoming_trainings_line">
                            <div class="col-xl-1 col-lg-1 col-md-2 col-xs-12 date">
                                ${element.start_time}
                            </div>
                            <div class="col-xl-1 col-lg-1 col-md-1 col-xs-12 logo">
                            `;

                            if(element.logo){
                                result +=`<img style="width:35px" src="${base_dir}modules/${element.logo}"/>`;
                            }
                            
                            result +=`     
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12 training_name">
                                <p class="training_name_line"><a href="${base_dir}szkolenie/${element.alias}/${element.id_training}?page=1">${element.name}</a> </p>
                                <p class="place-and-date">
                           
                                <span>  <a href="${base_dir}wykladowca/${element.id_training_instructor}">${element.instructor_name}</a></span>
                                <span>
                                <i class="icon-map-marker"></i> ${element.place_of_training}</span></p>
                                <p>
                                <span class="span-orange"> Ostatnie miejsca </span>
                                <span class="span-new"> Nowość </span>
                                </p>
                                
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-2 col-xs-12 price_training">
                            Cena: ${element.price} zł
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-12 col-xs-12 checking">
                                <a class="checking" href="${base_dir}szkolenie/${element.alias}/${element.id_training}?page=1">Sprawdź</a> 
                            </div>
                        </div>
                    `  
        });

        return result;
    },
    createProducts: function(a = []){
        let result = '';
        a.forEach(element => {
            result +=`

            <li class="ajax_block_product col-xl-4 col-lg-4 col-xs-12 col-sm-6 col-md-6 first-in-line last-line first-item-of-tablet-line first-item-of-mobile-line last-mobile-line">
			<div class="product-container product" itemscope="" itemtype="https://schema.org/Product">
				<div class="left-block">
                    <div class="product-image-container">
						<a class="product_img_link" href="${base_dir}${element.category_link_rewrite}/${element.id_product}-${element.link_rewrite}.html" title="Printed Dress" itemprop="url">
                        `;
                            if(element.id_image){
                                result +=`<img class="replace-2x img-responsive" 
                                src="${base_dir}/${element.id_image}-home_default/${element.link_rewrite}.jpg" alt="${element.name}" 
                                title="${element.name}" width="250" height="250" itemprop="image">
                                `;
                            }
                            
                        result +=`     
						</a>
                        <div class="content_price" itemprop="offers" itemscope="" itemtype="https://schema.org/Offer">
                            <span itemprop="price" class="price product-price">${element.price}</span>
                            <meta itemprop="priceCurrency" content="PLN">
                        </div>
					</div>					
				</div>
				<div class="right-block">
                    <h5 itemprop="name">
                        <a class="product-name" href="${base_dir}${element.category_link_rewrite}/${element.id_product}-${element.link_rewrite}.html" title="Printed Dress" itemprop="url">${element.name}</a>
					</h5>
                    <div class="content_price">
                        <span class="price product-price">${element.price}</span>
                    </div>
				</div>
              
			</div>
		</li>
                    `  
        });

        return result;

    }
};

//Events

    $('.js-pagination-poldent-search > li').on('click',function(e){
        e.stopPropagation();
        SearchPoldent.$this = $(this);
        const button = SearchPoldent.$this.closest('li');
        $('#selectorId option:selected').val();

        if(!button.hasClass('bpn')){
            SearchPoldent.pagination = SearchPoldent.$this.closest('.js-pagination-poldent-search');
            SearchPoldent.page = SearchPoldent.$this.attr("data-page");
            SearchPoldent.name = SearchPoldent.pagination.attr("data-name");
            let selected = '';

            if(SearchPoldent.name){
                const i = '#productsSortForm_'+SearchPoldent.name+' option:selected'
                selected = $(i).val();
            } else {
               selected = 'all';
             }

            SearchPoldent.max= SearchPoldent.pagination.attr("data-max");
            SearchPoldent.changeActive(SearchPoldent.pagination, SearchPoldent.$this, SearchPoldent.name, SearchPoldent.page, original_query, selected);
        } else {
            SearchPoldent.changeNumber(button);
        }  
    });

    $('.js-selectPoldent').on('change',function(e){

        e.stopPropagation();
        const name =$(this).attr('data-select');
        const pagination = $(this).closest('.w-poldent-'+ name).find('.js-pagination-poldent-search');
        const button = pagination.find('li:not(.bpn)');
        SearchPoldent.resetButtons(button);
        button['0'].click();
        
    });
})

