<div class="row">
    <div class="col-xs-12">
        <div class="panel">
            <div class="panel-heading">Szczegóły zamówienia nr {$order->id_training_order}
            </div>
            <div class="panel">
                <div class="panel-heading">Dane kupującego:</div>
                <div class="well list-detail">
                    <dt>Imię i nazwisko: </dt>
                    <dd>{$customer->firstname} {$customer->lastname}</dd>
                    <dt>Email: </dt>
                    <dd>{$customer->email}</dd>
                    <dt>Faktura do zamówienia:</dt>
                    <dd> 
                    <form id="save-invoice" class="form-horizontal" method="POST" enctype="multipart/form-data">
                            <input type="file" name="invoice">
                         <input type="submit" value="Zapisz" class="btn btn-primary" name="save-invoice">
                    </form>
                    </dd>
                    {$invoice|@var_dump}
                </div>
            </div>
            
            <div class="panel">
                <div class="panel-heading">Kupione szkolenia: </div>

                    {foreach from=$trainings item=training}
              
                        <div class="well list-detail">
                            <dt>Nazwa: </dt>
                            <dd><a style="color: #555" rel="nofollow" target="_blank"                    
                                href="{$link_training}&id_training={$training.id_training}&updatetraining&token={$token_training}">{$training.training_name}</a>
                            </dd>
                            <dt>Ilość: </dt>
                            <dd>{$training.quantity}</dd>
                            <dt>Cena:</dt>
                            <dd>{$training.price}</dd>
                            <dt>Zapisane osoby: </dt>
                            
                            {foreach from=$training.signedParticipants item=participant}
                            <ul>
                                <li> <a style="color: #555" rel="nofollow" target="_blank"                    
                                        href="{$link}&id_training_participant={$participant.id_training_participant}&updatetraining_participant&token={$token}">{$participant.name} {$participant.surname} </a>
                                </li>
                                <li>Email: {$participant.email}</li>
                                <li>Potwierdzono: {$participant.rodo_ok}</li>
                                <li>Dodaj certyfikat:
                                <form enctype="multipart/form-data" id="save-certificate" style="display:flex; align-items: center;" method="POST">
                                <input type="file" name="certificate">
                                <input type="hidden" name="id_training_participant" value="{$participant.id_training_participant}"/>
                                     <input type="submit" value="Dodaj certyfikat" class="btn btn-primary" name="save-certificate">
                                </form></li>
                                <li> Dodany certyfikat:
                                    {foreach from=$certyficates item=certyficate}
                                    {$part = strripos($certyficate, '-')}
                                        {if $participant.id_training_participant == substr($certyficate, 0, $part) }
                                            <p> <a rel="nofollow" target="_blank" href="http://swiatwebmasterow.pl/poldent/modules/trainings/certificates/{$order->id_training_order}/{$certyficate}">{$certyficate}</a>
                                             <form id="remove_certyficate_training_order" class="form-horizontal" method="POST">
                                                <input type="hidden" name="remove_certyficate_name" value="{$certyficate}">
                                                    <input type="submit" value="Usuń plik"  name="remove_certyficate_training_order">
                                                </form>
                                            </p>
                                        {/if}
                                    {/foreach}
                                </li>
                            </ul>
                            {/foreach}
                        </div>
                    {/foreach}
            </div>
           
        </div>
    </div>
</div>
{$bundle|@var_dump}
