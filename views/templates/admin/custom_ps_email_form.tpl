<div class="row">
    <div class="col-xs-12">
        <div class="panel">
           <div class="panel-heading">
				<i class="icon-envelope"></i> {l s='Message'} 
			</div>
				<div id="messages" class="well hidden-print">
					<form action="{$smarty.server.REQUEST_URI|escape:'html':'UTF-8'}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}" method="post" onsubmit="if (getE('visibility').checked == true) return confirm('{l s='Do you want to send this message to the customer?'}');">
						<div id="message" class="form-horizontal">
							
							<div class="form-group">
								<label class="control-label col-lg-3">{l s='Temat'}</label>
								<div class="col-lg-9">
									<input type="text" id="topic_msg"  name="topic" required>
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label col-lg-3">{l s='Message'}</label>
								<div class="col-lg-9">
									<textarea id="txt_msg" class="textarea-autosize" name="message" required></textarea>
									<p id="nbchars"></p>
								</div>
							</div>
							<button type="submit" id="submitMessage" class="btn btn-primary pull-right" name="submitMessage">
								{l s='Wyślij wiadomość'}
							</button>
							
						</div>
					</form>
				</div>
        </div>
    </div>
</div>
