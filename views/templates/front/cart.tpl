{capture name="path"}Koszyk szkoleń{/capture}
<section id="cart-training" class="background-white">

{if isset($dotpaySummary)}
<h1 class="page-heading"> Podsumowanie </h1>
    Dziękujemy, za chwilę zostaniesz przekierowany na stronę płatności
    <form id="dotpay" action="https://ssl.dotpay.pl/test_payment/">
        <input type="hidden" name="api_version" value="dev">
        <input type="hidden" name="id" value="{$shop_id}">
        <input type="hidden" name="amount" value="{$amount}">
        <input type="hidden" name="currency" value="PLN">
        <input type="hidden" name="description" value="{$desc}">
        <input type="hidden" name="control" value="{$control}">
        <input type="hidden" name="url" value="{$url}">
        <input type="hidden" name="type" value="{$type}">
        <input type="hidden" name="chk" value="{$chk}">
    </form>
    <script>
        $(document).ready(function() {
            setTimeout($('#dotpay').submit(), 5000);
        })
    </script>
    
    {elseif isset($bankSummary)}
        Dane do przelewu: <br>
        Nazwa banku: {$bankName} <br>
        Numer konta: {$bankAccount} <br>
        Tytuł przelewu: do wymyślenia
    {else}
<h1 class="page-heading">Koszyk szkoleń </h1>
    {if $trainings|@count > 0}
	
        <div class="table-responsive">
		<table class="table table-bordered">
				<tr class="head-table">
					<td>Nazwa</td>
					<td>Ilość uczestników</td>
                    <td>Cena jednostkowa</td>
					<td>Razem</td>
					<td>Usuń</td>
				</tr>
			{foreach from=$trainings item=training}
			
            {if $training.id_bundle == null}
				<tr class="cart-training" data-id_training="{$training.id_training}" data-id_cart="{$id_cart}">
					
					<td>{$training.training_name}</td>
					<td class="quantity-buttons">
						<button class="btn button-minus training-remove trainingCartQtyDown">-</button><span class="quantity">{$training.quantity}</span><button class="btn button-plus training-add trainingCartQtyUp">+</button>
					</td>
					<td><span class="single-price">{$training.price}</span> zł</td>
                    <td><span class="price">{$training.price * $training.quantity}</span> zł</td>
					<td>
						<button class="btn training-delete trainingCartDelete">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							width="15px" height="15px" viewBox="0 0 31.112 31.112" style="enable-background:new 0 0 31.112 31.112;" xml:space="preserve">
							<polygon points="31.112,1.414 29.698,0 15.556,14.142 1.414,0 0,1.414 14.142,15.556 0,29.698 1.414,31.112 15.556,16.97 
								29.698,31.112 31.112,29.698 16.97,15.556 "/>
							</svg>
						</button>
					</td>
				</tr>
				{else}
				<tr class="cart-training" data-id_bundle="{$training.id_bundle}"  data-id_cart="{$id_cart}">
                    
                    <td>{$training.training_name}</td>
					<td class="quantity-buttons">
					<button class="btn button-minus boundle-remove trainingCartQtyDown">-</button><span class="quantity">{$training.quantity}</span><button class="btn button-plus boundle-add trainingCartQtyUp">+</button>
					</td>
					<td><span class="single-price">{$training.price}</span> zł</td>
					<td><span class="price">{$training.price * $training.quantity}</span> zł</td>
                    <td>
						<button class="btn boundle-delete trainingCartDelete">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							width="15px" height="15px" viewBox="0 0 31.112 31.112" style="enable-background:new 0 0 31.112 31.112;" xml:space="preserve">
							<polygon points="31.112,1.414 29.698,0 15.556,14.142 1.414,0 0,1.414 14.142,15.556 0,29.698 1.414,31.112 15.556,16.97 
							29.698,31.112 31.112,29.698 16.97,15.556 "/>
							</svg>
						</button>
					</td>
				</tr>
			{/if}
                
            {/foreach}
			<tr class="footer-table">
				<td colspan="3" class="bold text-right transform">
					W sumie w koszyku: 
				</td>
				<td colspan="2" class="bold">
					<span id="cartSum">{$cartSum}</span> zł
				</td>
			</tr>
        </table> 

            
        </div>
	{if $propose_add_training}
	<div class="info-bundles-box col-xs-12 info-bundles">
		<p><svg id="Capa_1" enable-background="new 0 0 524.235 524.235" width="20px" height="20px" viewBox="0 0 524.235 524.235" xmlns="http://www.w3.org/2000/svg">
			<path style="fill:#006da0" d="m262.118 0c-144.53 0-262.118 117.588-262.118 262.118s117.588 262.118 262.118 262.118 262.118-117.588 262.118-262.118-117.589-262.118-262.118-262.118zm17.05 417.639c-12.453 2.076-37.232 7.261-49.815 8.303-10.651.882-20.702-5.215-26.829-13.967-6.143-8.751-7.615-19.95-3.968-29.997l49.547-136.242h-51.515c-.044-28.389 21.25-49.263 48.485-57.274 12.997-3.824 37.212-9.057 49.809-8.255 7.547.48 20.702 5.215 26.829 13.967 6.143 8.751 7.615 19.95 3.968 29.997l-49.547 136.242h51.499c.01 28.356-20.49 52.564-48.463 57.226zm15.714-253.815c-18.096 0-32.765-14.671-32.765-32.765 0-18.096 14.669-32.765 32.765-32.765s32.765 14.669 32.765 32.765c0 18.095-14.668 32.765-32.765 32.765z"/></svg> 
		</p>
		<p style="padding-left: 20px">
			{assign var=params value=[
				'id_training' => $propose_add_training.id_training,
				'alias' => $propose_add_training.alias,
				'page' => 1
			]}
			Dodałeś szkolenia wchodzące w skład pakietu. Dołącz brakujące szkolenie <a href="{$link->getModuleLink('trainings', 'single', $params)}">{$propose_add_training.name}</a> 
			do koszyka szkoleń i zapłać mniej. Twoje szkolenia zamienią się na pakiet szkoleń. 
		</p>
		
	</div>
	{/if}
<div class="your-data">
        <p class="title-block">Twoje dane</p>
            <p>Imię: {$buyer.firstname}</p>
            <p>Nazwisko: {$buyer.lastname}</p>
            <p>Email: {$buyer.email}</p>
            <p>Numer telefonu: {$buyer.number}</p>
  
</div>
        
            <form id="submit-list" class="form-horizontal" method="POST">
                <fieldset> 
                    <!-- Form Name -->
                  
                    {if isset($registrationErrors) && $registrationErrors|@count > 0}
                        <div class="alert alert-danger">
                            <p>Popraw następujące błędy:</p>
                            <ol>
                                {foreach $registrationErrors as $error}
                                    {foreach $error as $msg}
                                        {$msg} <br>
                                    {/foreach}
                                {/foreach}
                            </ol>
                            
                        </div>
                    {/if}
                    {foreach from=$trainings item=training}
                        {assign var="counter" value=1}
                       
                            {if  $training.id_training}
						
                             <div class="training" data-id_training="{$training.id_training}">
							<p class="title-block">Wprowadź dane uczestników</p>
                            <p class="title-block-training-name">Szkolenie: {$training.training_name}</p>
                            <div class="training-list col-xl-12 col-lg-12 col-md-12 col-xs-12 col-12" data-training-list="{$training.id_training}">
                                {for $i=1 to $training.quantity}
                                    <div class="training-list-item col-lg-6 col-xl-6 col-md-12 col-xs-12 col-12" data-participant={$i}>
                                        <p class="participant-label">Uczestnik <span class="participant-num">{$i}</span></p>

                                        {assign var="postTraining" value="training_{$training.id_training}"}
                                        <div class="form-group">
                                         
                                            <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12">
                                                <input  name="training_{$training.id_training}[{$i}][firstname]"
													placeholder="Imię"
                                                        type="text"
                                                        class="form-control input-md"
                                                        required
                                                        {strip}value="{if isset($smarty.post.$postTraining.$i.firstname)}{$smarty.post.$postTraining.$i.firstname}{/if}"{/strip}>
                                            </div>
                                        </div>
                                        <!-- Text input-->
                                        <div class="form-group">
                                           
                                            <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12">
                                                <input  name="training_{$training.id_training}[{$i}][lastname]"
														placeholder="Nazwisko"
                                                        type="text"
                                                        class="form-control input-md"
                                                        required
                                                        {strip}value="{if isset($smarty.post.$postTraining.$i.lastname)}{$smarty.post.$postTraining.$i.lastname}{/if}"{/strip}>
                                            </div>
                                        </div>
                                        
                                        <!-- Text input-->
                                        <div class="form-group">
                                           
                                            <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12">
                                                <input  name="training_{$training.id_training}[{$i}][email]"
														placeholder="E-mail"
                                                        type="text"
                                                        class="form-control input-md"
                                                        required
                                                        {strip}value="{if isset($smarty.post.$postTraining.$i.email)}{$smarty.post.$postTraining.$i.email}{/if}"{/strip}>
                                            </div>
                                        </div>
                                        <!-- Text input-->
                                        <div class="form-group">
                                           
                                            <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12">
                                                <input  name="training_{$training.id_training}[{$i}][phone]"
														placeholder="Telefon"
                                                        type="text"
                                                        class="form-control input-md"
                                                        required
                                                        {strip}value="{if isset($smarty.post.$postTraining.$i.phone)}{$smarty.post.$postTraining.$i.phone}{/if}"{/strip}>
                                            </div>
                                        </div>
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <div class="col-xl-1 col-lg-1 col-md-1 col-xs-2 col-2">
                                                <input  name="training_{$training.id_training}[{$i}][send_info_mail]"
                                                        type="checkbox"
                                                        class="form-control input-md check"
                                                        checked>
                                            </div>
											<label class="col-xl-4 col-lg-4 col-md-4 col-xs-10 col-10 control-label" for="training_{$training.id_training}[{$i}][send_info_mail]">Powiadom uczestnika</label>
                                        </div> 
                                    </div>
                                {/for}
                            </div>
                           
						</div>
                            {elseif $training.id_bundle}
					
                             <div class="training" data-id_boundle="{$training.id_bundle}" data-id_cart="{$id_cart}">
							   <p class="title-block">Wprowadź dane uczestników</p>
                                <p class="title-block-training-name">Pakiet: {$training.training_name}</p>
                                <div class="training-list col-xl-12 col-lg-12 col-md-12 col-xs-12 col-12" data-bundle-list="{$training.id_bundle}">
                                    {for $i=1 to $training.quantity}
                                        <div class="training-list-item col-lg-6 col-xl-6 col-md-12 col-xs-12 col-12" data-participant={$i}>
                                            <p class="participant-label">Uczestnik <span class="participant-num">{$i}</span></p>

                                            {assign var="postBundle" value="bundle_{$training.id_bundle}"}

                                            <!-- Text input-->
                                            <div class="form-group">
                                                <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12">
                                                    <input  name="bundle_{$training.id_bundle}[{$i}][firstname]"
															placeholder="Imię"
                                                            type="text"
                                                            class="form-control input-md"
                                                            required
                                                            {strip}value="{if isset($smarty.post.$postBundle.$i.firstname)}{$smarty.post.$postBundle.$i.firstname}{/if}"{/strip}>
                                                </div>
                                            </div>
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12">
                                                    <input  name="bundle_{$training.id_bundle}[{$i}][lastname]"
													placeholder="Nazwisko"
                                                            type="text"
                                                            class="form-control input-md"
                                                            required
                                                            {strip}value="{if isset($smarty.post.$postBundle.$i.lastname)}{$smarty.post.$postBundle.$i.lastname}{/if}"{/strip}>
                                                </div>
                                            </div>
                                            
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12">
                                                    <input  name="bundle_{$training.id_bundle}[{$i}][email]"
													placeholder="E-mail"
                                                            type="text"
                                                            class="form-control input-md"
                                                            required
                                                            {strip}value="{if isset($smarty.post.$postBundle.$i.email)}{$smarty.post.$postBundle.$i.email}{/if}"{/strip}>
                                                </div>
                                            </div>
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12">
                                                    <input  name="bundle_{$training.id_bundle}[{$i}][phone]"
															placeholder="Telefon"
                                                            type="text"
                                                            class="form-control input-md"
                                                            required
                                                            {strip}value="{if isset($smarty.post.$postBundle.$i.phone)}{$smarty.post.$postBundle.$i.phone}{/if}"{/strip}>
                                                </div>
                                            </div>
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <div class="col-xl-1 col-lg-1 col-md-1 col-xs-2 col-2">
                                                    <input  name="bundle_{$training.id_bundle}[{$i}][send_info_mail]"
                                                            type="checkbox"
                                                            class="form-control input-md check"
                                                            checked>
                                                </div>
												<label class="col-xl-4 col-lg-4 col-md-4 col-xs-10 col-10 control-label " for="bundle_{$training.id_bundle}[{$i}][send_info_mail]">Powiadom uczestnika</label>
                                            </div> 
                                        </div>
                                    {/for}
                                </div>
                            </div>
							
                            {/if}
                       
                    {/foreach}
                </fieldset>
                <p class="button-form">
                <input type="submit" value="Zapisz" class="btn" name="submit-list">
				</p>
            </form>
			

			
        </div>
    {else}
        Brak produktów w koszyku
    {/if}
{/if}

</section>

{addJsDef training_ajaxurl=$ajaxurl}
{addJsDef bundle_ajaxurl=$ajaxurl}
