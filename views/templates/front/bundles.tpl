{capture name="path"}
	<a href="{$link->getPageLink('module-trainings-frontpage', true)|escape:'html':'UTF-8'}">
		{l s='Szkolenia'}
	</a>
	<span class="navigation-pipe">{$navigationPipe}</span>
	Pakiety szkoleń
{/capture}

<section id="section-trainings-bundles" class="background-white">
<h1 class="page-heading"> Pakiety szkoleń </h1>
<div class="row">
<div class="col-md-12">
    {foreach from=$bundles item=bundle}
	   {assign var=params value=[
				'id_instructor' => $instructor.id_training_instructor
			]}

    {assign var=params_bundle value=[
        'id_bundle' => $bundle.id_bundle,
        'page' => 1
    ]}
		<div class="col-md-4">
			<div class="box-training-bundle">
			<!-- Logo szkolenia -->
				{if $bundle.bundle_image}
				<div class="img-training">
					<img src="{$base_dir}modules/{$bundle.bundle_image}"/>
				</div>
			{/if}
				
			<!-- Koniec logo szkolenia -->
				<p class="text-center"><a class="training-name" href="{$link->getModuleLink('trainings', 'bundle', $params_bundle)}">{$bundle.title}</a></p>
				<p class="training-desc" itemprop="description">
						{$bundle.description|strip_tags:'UTF-8'|truncate:150:'...'}
				</p>
				<p class="text-instructor"> 
					<span> Prelegenci: </span>
					 {foreach from=$bundle.trainings item=t}
				
					<a href="#">{$t.instructor_name} {$t.instructor_surname}</a>
					 {/foreach}
				</p> 
			<!-- Cena pakietu -->
				<p class="trainings-price">
				<span class="original-price">250.00 zł </span>
				<span>100.00 zł </span>
				</p>
			<!-- Koniec ceny pakietu -->
				<p class="text-center"><a class="view-training" href="{$link->getModuleLink('trainings', 'bundle', $params)}">Dowiedz się więcej >></a></p>
			</div>
		</div>	
     
    {/foreach} 
</div>
</div>
<div class="pagination-page col-md-12">
	<ul class="pagination">
		{for $count=1 to $nb_pages}
			{assign var=params value=[
			'page' => $count
			]}
			{if $page ne $count}
				<li>
					<a href="{$link->getModuleLink('trainings', 'bundles', $params)}">
						<span>{$count}</span>
					</a>
				</li>
			{else}
				<li class="active current">
					<span><span>{$count}</span></span>
				</li>
			{/if}
		{/for}
	</ul>
</div>
</section>
{*{$bundles|@var_dump}*}

