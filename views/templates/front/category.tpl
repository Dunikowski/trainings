{capture name="path"}
	<a href="{$link->getPageLink('module-trainings-frontpage', true)|escape:'html':'UTF-8'}">
		{l s='Szkolenia'}
	</a>
	<span class="navigation-pipe">{$navigationPipe}</span>
	{$category->name}{/capture}
	
	

<div class="upcoming_trainings background-white">

<h1 class="page-subheading">{$category->name}</h1>
<form method="POST" id="filter-front-page">
	<select name="voivodeship-for-category-select" class="select-name-voivodeship-training">
	<option value="" hidden>Województwo</option>
	<option value="All">Wszystkie</option>
		{foreach from=$voivodeship item=voi}
			<option value="{$voi.voivodeship_name}" {if $voi.voivodeship_name == $select_voivodeshop} selected {/if}>{$voi.name}</option>
		{/foreach}
		</select>
		<input type="submit" class="button-gray btn" name="voivodeship-for-category" value="Wybierz">
	</form>
{if $trainings}

    {foreach from=$trainings item=training}
    {assign var=params value=[
        'id_training' => $training.id_training,
        'alias' => $training.alias,
        'page' => 1
    ]}
	{if $training.active == 1}
	<div class="wrap upcoming_trainings_line">
		<div class="col-xl-1 col-lg-1 col-md-2 col-xs-12 col-12 date">
			{$training.start_time|date_format:"%d.%m.%Y"}
		</div>
		<div class="col-xl-1 col-lg-1 col-md-1 col-xs-12 col-12 logo">
			{if $training.logo}
				<img width="35px" src="{$base_dir}modules/{$training.logo}"/>
			{/if}
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12 col-12 training_name">
			<p class="training_name_line"><a href="{$link->getModuleLink('trainings', 'single', $params)}">{$training.name}</a> </p>
			<p class="place-and-date"><span> TODO prowadzący </span><span><i class="icon-map-marker"></i> {$training.place_of_training}</span></p>
			{if $training.slots == 2 || $training.slots < 2}<p><span class="span-orange"> Ostatnie miejsca </span> </p>{/if}
		</div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-xs-12 col-12 price_training">
		Cena: {$training.price} zł
		</div>
		<div class="col-xl-2 col-lg-2 col-md-12 col-xs-12 col-12 checking">
			<a class="checking" href="{$link->getModuleLink('trainings', 'single', $params)}">Sprawdź</a> 
		</div>
	</div>
	{/if}
    {/foreach}

</div>
{else}
	<p>Przepraszamy w tej chwili nie posiadamy żadnego szkolenia z tej kategorii.</p>
{/if}
</div>
<div class="pagination-page col-md-12">
	<ul class="pagination">
		{for $count=1 to $nb_pages}
			{assign var=params value=[
			'page' => $count,
			'id_category' => $category->id_training_category,
			'name' => $category->alias
			]}
			{if $page ne $count}
				<li>
					<a href="{$link->getModuleLink('trainings', 'category', $params)}">
						<span>{$count}</span>
					</a>
				</li>
			{else}
				<li class="active current">
					<span><span>{$count}</span></span>
				</li>
			{/if}
		{/for}
	</ul>
</div>
