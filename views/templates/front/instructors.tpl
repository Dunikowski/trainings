{capture name="path"}
	<a href="{$link->getPageLink('module-trainings-frontpage', true)|escape:'html':'UTF-8'}">
		{l s='Szkolenia'}
	</a>
	<span class="navigation-pipe">{$navigationPipe}</span>
	Wykładowcy
{/capture}

<section id="section-trainings-instructors" class="background-white">
<h1 class="page-heading"> Wykładowcy </h1>
<div class="wrap">

    {foreach from=$instructors item=instructor}
	   {assign var=params value=[
				'id_instructor' => $instructor.id_training_instructor
			]}

		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="instructor-box">
			<!-- Logo szkolenia -->
				<img src="{$base_dir}modules/trainings/img/instructors/{$instructor.instructor_image}"/>
			<!-- Koniec zdjęcia instructora -->
				<p class="text-center"><a class="training-name" href="{$link->getModuleLink('trainings', 'singleInstructor', $params)}">{$instructor.name}</a></p>
				<p class="training-desc" itemprop="description">
						{$instructor.description|strip_tags:'UTF-8'|truncate:330:'...'}
				</p>
		
				<p class="text-center"><a class="view-training" href="{$link->getModuleLink('trainings', 'singleInstructor', $params)}">Dowiedz się więcej >></a></p>
			</div>
		</div>	
     
    {/foreach}

</div>
</section>
