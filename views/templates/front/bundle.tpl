{capture name="path"}
	<a href="{$link->getPageLink('module-trainings-frontpage', true)|escape:'html':'UTF-8'}">
		{l s='Szkolenia'}
	</a>
	<span class="navigation-pipe">{$navigationPipe}</span>
	{$bundle.title}{/capture}
<section id="section-bundle">

<div class="wrap">
	<div class="col-xl-8 col-lg-8 col-md-8 col-xs-12 col-12 box-left">

		<p>Pakiet szkoleń</p>
		<p class="name" id="name">{$bundle.title}</p>
		<p class="text-trainings description">{$bundle.description}</p>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-xs-12 col-12 box-right">
			<!-- Cena pakietu -->
		    <p class="trainings-price" >Cena za pakiet szkoleń: 
			<span class="original-price">{$price} zł </span>
			<span>{$priceD} zł </span>
			</p>
			<!-- Koniec ceny pakietu -->
		<input type="number" value="1" name="num_people" min=1>
		<input type="hidden" value=" {$bundle.id_bundle}" name="id_bundle">
		<button id="submit" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
			Kup pakiet
		</button>
	</div>
</div>
<div class="wrap bundle-trainings">

    {if $trainings_bundle}
        {foreach from=$trainings_bundle item=bundle}    
        {assign var=params value=[
            'id_training' => $bundle.id_training,
            'alias' => $bundle.alias,
            'page' => 1
        ]}
		<div class="col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12 wrap-box">
			<div class="box-training-bundle">
			<!-- Logo szkolenia -->
			{if $bundle.logo}
				
					<img src="{$base_dir}modules/{$bundle.logo}"/>
				
			{/if}
			<!-- Koniec logo szkolenia -->
				<p class="text-center"><a class="training-name" href="{$link->getModuleLink('trainings', 'single', $params)}">{$bundle.name}</a></p>
				<p class="text-instructor"> 
					<span> Prelegent: </span>
		{assign var=instructor value=[
			'id_instructor' => {$bundle.instructors.id_training_instructor}
		]}
					<a href="{$link->getModuleLink('trainings', 'singleInstructor', $instructor)}">{$bundle.instructors.name}</a>
				</p>
				
				<p>{if $bundle.for_whom = 1}
					<span class="text-bold">Szkolenie dla:</span> studentów
				{elseif $bundle.for_whom = 2}
					 <span class="text-bold">Szkolenie dla:<s/pan> lekarzy
				{/if} </p>
				<div class="time-training"><p class="text-bold time-left">Data i godzina: </p> 
					<p class="time-right"><span>{$bundle.start_time|date_format:"%d.%m.%Y %H:%M" } - </span>
					<span>{$bundle.end_time|date_format:"%d.%m.%Y %H:%M" }</span>
					</p>
				</div>
				    <p><span class="text-bold">Miejsce:</span> {$bundle.voivodeship}, {$bundle.place_of_training}</p>
				<p class="text-center"><a class="view-training" href="{$link->getModuleLink('trainings', 'single', $params)}">Zobacz szkolenie >></a></p>
			</div>
		</div>		
        {/foreach}
    {/if}
</div>

	<div id="trainings-features">
		<ul class="nav" role="tablist">
			<li role="presentation" class="active"><a href="#prod-desc" aria-controls="prod-desc" role="tab" data-toggle="tab">Program</a></li>
            <li role="presentation"><a href="#prod-info" aria-controls="prod-info" role="tab" data-toggle="tab">Materiały</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="prod-desc">
             	
				<section class="page-product-box">
					{foreach from=$trainings_bundle item=bundle}   
					<div class="accordion col-md-12" id="accordionExample_{$bundle.id_training}">
						  <div class="card">
							<div class="card-header" id="heading_{$bundle.id_training}">
								<a class="bundle-name btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse_{$bundle.id_training}" aria-expanded="false" aria-controls="collapse_{$bundle.id_training}">
									<p><span> Szkolenie: </span> {$bundle.name} </p>
								</a>
							</div>
							<div id="collapse_{$bundle.id_training}" class="class-end collapse" aria-labelledby="heading_{$bundle.id_training}" data-parent="#accordionExample_{$bundle.id_training}">
							  <div class="card-body">
								{if $bundle.long_desc}
									{$bundle.long_desc}
								{else}
									<p> Brak informacji </p>
								{/if}
							  </div>
							</div>
						  </div>
						</div>
					{/foreach}   
				</section>
				
             </div>
            <div role="tabpanel" class="tab-pane" id="prod-info">
			  
				<section class="page-product-box">
					<div class="rte">
					//TODO
					<p> Brak materiałów </p>
					</div>
				</section>
            </div>
	</div>

</section>

{addJsDef bundle_ajaxurl=$ajaxurl}
{addJsDef bundleId=$bundle.id_bundle}

