<section id="front-page-trainings">
<div class="wrap">
<div class="col-md-3">
	<ul class="category-trainings">
		{foreach $categories as $category}
				<li class="depth_{$category.level_depth}">
					<a href="{$category.link}">{$category.name} </a> 
					{if $category.children|@count > 0 }
						{include file="./category-tree-branch.tpl" categories=$category.children}
					{/if}
				</li>
		{/foreach}
	</ul>
</div>

<div class="col-md-9">
	{hook h='displaySliderTrainings'}

</div>
</div>
{capture name="path"}Szkolenia{/capture}


<div class="upcoming_trainings background-white">
    <h2>Najbliższe szkolenia: </h2>

	<form method="POST" id="filter-front-page">
		<select name="select-category-training" class="select-name-category-training">
		<option value="" hidden>Kategorie</option>
		<option value="0">Wszystkie</option>
			{foreach from=$categories item=category}
				<option value="{$category.id_training_category}" {if $category.id_training_category == $selected_upcoming_trainings} selected {/if}>{$category.name}</option>
			{/foreach}
			</select>
			<select name="select-voivodeship-training" class="select-name-voivodeship-training">
		<option value="All" hidden>Województwo</option>
			{foreach from=$voivodeship item=voi}
				<option value="{$voi.voivodeship}" {if $voi.voivodeship == $select_voivodeshop} selected {/if}>{$voi.voivodeship}</option>
			{/foreach}
			</select>
			<input type="submit" class="button-gray btn-filter-front-page btn" name="category-upcoming_trainings" value="Sortuj">
	</form>
	
{if $upcoming_trainings}
    {foreach from=$upcoming_trainings item=training}
    {assign var=params value=[
        'id_training' => $training.id_training,
        'alias' => $training.alias,
        'page' => 1
    ]}

	<div class="wrap upcoming_trainings_line">
		<div class="col-xl-1 col-lg-1 col-md-2 col-xs-12 date">
			{$training.start_time|date_format:"%d.%m.%Y"}
		</div>
		<div class="col-xl-1 col-lg-1 col-md-1 col-xs-12 logo">
				{if $training.logo}
				<img style="width:35px" src="{$base_dir}modules/{$training.logo}"/>
				{/if}
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12 training_name">
			<p class="training_name_line"><a href="{$link->getModuleLink('trainings', 'single', $params)}">{$training.name}</a> </p>
			<p class="place-and-date">
		   {assign var=instructor value=[
				'id_instructor' => {$training.instructors.id_training_instructor}
			]}
			<span>  <a href="{$link->getModuleLink('trainings', 'singleInstructor', $instructor)}">{$training.instructors.name}</a></span>
			<span>
			<i class="icon-map-marker"></i> {$training.place_of_training}</span></p>
			<p>
			{if $training.slots == 2 || $training.slots < 2}<span class="span-orange"> Ostatnie miejsca </span>{/if}
			{if $smarty.now <= strtotime($training.training_time_create)+(60*60*24*20)} <span class="span-new"> Nowość </span>{/if}
			</p>
			 
		</div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-xs-12 price_training">
		Cena: {$training.price} zł
		</div>
		<div class="col-xl-2 col-lg-2 col-md-12 col-xs-12 checking">
			<a class="checking" href="{$link->getModuleLink('trainings', 'single', $params)}">Sprawdź</a> 
		</div>
	</div>
    {/foreach}
	{else}
	<p> Brak nadchodzących szkoleń </p>
	{/if}
</div>



{if $bundle}
<div class="bundles background-white">
    <div class="header-bundles"><h2>Pakiety szkoleń:</h2>
			<a class="a-block-shop" href="{{$base_dir}}pakiety-szkolen" rel="nofollow">pokaż wszystkie</a></div>
    <div class="col-md-12 bundles-slick">
    {foreach from=$item item=training}
    {assign var=params value=[
        'id_bundle' => $training.id_bundle,
        'page' => 1
    ]}

		<div class="col-md-4">
			<div class="box-training-bundle">
			<!-- Logo szkolenia -->
			{if $training.bundle_image}
				<div class="img-training">
					<img src="{$base_dir}modules/{$training.bundle_image}"/>
				</div>
			{/if}
			<!-- Koniec logo szkolenia -->
				<p class="text-center"><a class="training-name" href="{$link->getModuleLink('trainings', 'bundle', $params)}">{$training.title}</a></p>
				<p class="training-desc" itemprop="description">
						{$training.description|strip_tags:'UTF-8'|truncate:150:'...'}

				</p>
				<p class="text-instructor"> 
					<span> Prelegenci: </span>
					 {foreach from=$training.trainings item=t}
						{assign var=instructor_bundle value=[
							'id_instructor' => {$t.id_training_instructor}
						]}
						 <a href="{$link->getModuleLink('trainings', 'singleInstructor', $instructor_bundle)}">{$t.instructor_name} {$t.instructor_surname}</a>
					 {/foreach}
				</p> 
			<!-- Cena pakietu -->
				<p class="trainings-price">
				<span class="original-price">{if $training.price} {$training.price} zł {/if}</span>
				<span>{if $training.priceD} {$training.priceD} zł {/if} </span>
				</p>
			<!-- Koniec ceny pakietu -->
				<p class="text-center"><a class="view-training" href="{$link->getModuleLink('trainings', 'bundle', $params)}">Dowiedz się więcej >></a></p>
			</div>
		</div>	
     
    {/foreach}
	</div>
	
</div>
{/if}
<div class="wrap">
<div class="col-xl-8 col-lg-8 col-md-12 col-xs-12 col-12">

{if $promoted_trainings}
<div class="promoted_trainings background-white">
    <h2>Wyróżnione szkolenia: </h2>
    <div class="promoted">
    {foreach from=$promoted_trainings item=training}
    {assign var=params value=[
        'id_training' => $training.id_training,
        'alias' => $training.alias,
        'page' => 1
    ]}
	{if $training.active == 1}
	<div class="col-xl-4 col-lg-4 col-md-6 col-xs-12 col-12">
		{if $training.image}
		<div class="img-training">
			<a href="{$link->getModuleLink('trainings', 'single', $params)}">
				<img src="{$base_dir}modules/{$training.image}"/>
			</a>
		</div>
		{/if}
		<p class="date-promoted information"> {$training.start_time|date_format:"%d %B %Y"}</p>
		<p class="information title"> <a href="{$link->getModuleLink('trainings', 'single', $params)}">{$training.name}</a> </p>
		<p class="name-instructor information">
		{assign var=instructor value=[
			'id_instructor' => {$training.instructors.id_training_instructor}
		]}
			<span>
		<a href="{$link->getModuleLink('trainings', 'singleInstructor', $instructor)}">{$training.instructors.name}</a>
		</span>
		<span class="place-promoted">{$training.place_of_training}</span></p>
	</div>
	{/if}
     
    {/foreach}
	</div>
</div>
</div>

<div class="col-xl-4 col-lg-4 col-md-12 col-xs-12 col-12 links">

	<div class="front-page-link first">

		<img src="http://swiatwebmasterow.pl/poldent/modules/trainings/img/wykladowcy.png" />
		<div class="absolute">
			<svg width="40px" height="40px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
					<path style="fill: #fff" d="M256,149.333c41.167,0,74.667-33.5,74.667-74.667C330.667,33.5,297.167,0,256,0s-74.667,33.5-74.667,74.667
						C181.333,115.833,214.833,149.333,256,149.333z M256,21.333c29.417,0,53.333,23.927,53.333,53.333S285.417,128,256,128
						s-53.333-23.927-53.333-53.333S226.583,21.333,256,21.333z"/>
					<path style="fill: #fff" d="M448.111,277.345c0.099-7.089,0.243-13.928,0.41-20.272c1.108-43.6,1.518-73.423-25.336-104.189
						c2.155-4.302,3.482-9.086,3.482-14.217c0-17.646-14.354-32-32-32s-32,14.354-32,32c0,17.646,14.354,32,32,32
						c4.728,0,9.182-1.095,13.225-2.943c20.69,24.365,20.349,48.204,19.296,88.797c-0.169,6.508-0.314,13.533-0.413,20.813h-21.441
						V258.24c0-24.104-13.292-45.958-34.667-57.042c-26.854-13.927-68.542-30.531-114.667-30.531s-87.813,16.604-114.667,30.531
						c-21.375,11.083-34.667,32.938-34.667,57.042v19.094H86.142c-0.099-7.28-0.243-14.305-0.413-20.813
						c-1.053-40.592-1.395-64.432,19.296-88.797c4.043,1.848,8.497,2.943,13.225,2.943c17.646,0,32-14.354,32-32
						c0-17.646-14.354-32-32-32s-32,14.354-32,32c0,5.131,1.327,9.915,3.482,14.217c-26.854,30.766-26.444,60.589-25.336,104.189
						c0.167,6.341,0.311,13.177,0.41,20.26H64c-23.521,0-42.667,19.135-42.667,42.667v42.667c0,23.531,19.146,42.667,42.667,42.667
						h1.923l19.619,98.094C86.563,508.5,91,512,95.979,512c0.708,0,1.417-0.063,2.104-0.208c5.792-1.156,9.542-6.771,8.375-12.552
						l-18.781-93.906h336.646l-18.781,93.906c-1.167,5.781,2.583,11.396,8.375,12.552c0.688,0.146,1.396,0.208,2.104,0.208
						c4.979,0,9.417-3.5,10.438-8.573l19.618-98.094H448c23.521,0,42.667-19.135,42.667-42.667V320
						C490.667,296.507,471.581,277.408,448.111,277.345z M394.667,149.333c-5.875,0-10.667-4.781-10.667-10.667
						c0-5.885,4.792-10.667,10.667-10.667s10.667,4.781,10.667,10.667C405.333,144.552,400.542,149.333,394.667,149.333z M118.25,128
						c5.875,0,10.667,4.781,10.667,10.667c0,5.885-4.792,10.667-10.667,10.667s-10.667-4.781-10.667-10.667
						C107.583,132.781,112.375,128,118.25,128z M128,258.24c0-16.104,8.875-30.698,23.167-38.104C175.896,207.302,214.188,192,256,192
						s80.104,15.302,104.833,28.135c14.292,7.406,23.167,22,23.167,38.104v19.094H128V258.24z M469.333,362.667
						c0,11.76-9.563,21.333-21.333,21.333H64c-11.771,0-21.333-9.573-21.333-21.333V320c0-11.76,9.563-21.333,21.333-21.333h384
						c11.771,0,21.333,9.573,21.333,21.333V362.667z"/>
			</svg>
			<a href="{$link->getPageLink('module-trainings-instructors', true)|escape:'html':'UTF-8'}">
		
			<div>
				<p class="first-word">Nasi</p>
				<p class="secound-word">wykładowcy</p>
			</div>
			</a>
		</div>
	
	</div>
	
	<div class="front-page-link last">
	
		<img src="http://swiatwebmasterow.pl/poldent/modules/trainings/img/regulamin.png" />
		<div class="absolute">
			<svg idth="40px" height="40px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
				<path style="fill: #fff" d="M447.123,83.218L366.535,2.538C364.911,0.913,362.708,0,360.411,0H70.974c-4.78,0-8.656,3.876-8.656,8.656v494.688
					c0,4.78,3.876,8.656,8.656,8.656H441c4.78,0,8.656-3.876,8.655-8.656V89.336C449.655,87.042,448.744,84.841,447.123,83.218z
					 M432.344,494.688H79.631V17.312h277.192l75.521,75.606V494.688z"/>
				<path style="fill: #fff" d="M447.147,83.191L366.512,2.557c-2.477-2.475-6.199-3.215-9.434-1.876c-3.234,1.34-5.343,4.496-5.343,7.997v80.635
					c0,4.78,3.876,8.656,8.656,8.656h80.635c3.501,0,6.656-2.111,7.996-5.345C450.363,89.39,449.622,85.666,447.147,83.191z
					 M369.047,80.657v-51.08l51.082,51.08H369.047z"/>
				<path style="fill: #fff" d="M182.562,43.529h-71.637c-4.78,0-8.656,3.876-8.656,8.656s3.876,8.656,8.656,8.656h71.637c4.78,0,8.656-3.876,8.656-8.656
					S187.342,43.529,182.562,43.529z"/>
				<path style="fill: #fff" d="M229.573,95.217H110.925c-4.78,0-8.656,3.876-8.656,8.656s3.876,8.656,8.656,8.656h118.648
					c4.781,0,8.656-3.876,8.656-8.656S234.353,95.217,229.573,95.217z"/>
				<path style="fill: #fff" d="M315.651,152.303H197.004c-4.78,0-8.656,3.876-8.656,8.656s3.876,8.656,8.656,8.656h118.648
					c4.78,0,8.656-3.876,8.656-8.656S320.432,152.303,315.651,152.303z"/>
				<path style="fill: #fff" d="M401.048,408.905H282.402c-4.78,0-8.656,3.876-8.656,8.656s3.876,8.656,8.656,8.656h118.647
					c4.78,0,8.656-3.876,8.656-8.656S405.829,408.905,401.048,408.905z"/>
				<path style="fill: #fff" d="M401.048,451.159h-59.322c-4.78,0-8.656,3.876-8.656,8.656s3.876,8.656,8.656,8.656h59.322
					c4.78,0,8.656-3.876,8.656-8.656S405.829,451.159,401.048,451.159z"/>
				<path style="fill: #fff" d="M401.048,199.322H111.606c-4.78,0-8.656,3.876-8.656,8.656s3.876,8.656,8.656,8.656h289.443
					c4.78,0,8.656-3.876,8.656-8.656S405.829,199.322,401.048,199.322z"/>
				<path style="fill: #fff" d="M401.048,241.295H111.606c-4.78,0-8.656,3.876-8.656,8.656s3.876,8.656,8.656,8.656h289.443
					c4.78,0,8.656-3.876,8.656-8.656S405.829,241.295,401.048,241.295z"/>
				<path style="fill: #fff" d="M256.328,283.268H111.606c-4.78,0-8.656,3.876-8.656,8.656s3.876,8.656,8.656,8.656h144.723
					c4.78,0,8.656-3.876,8.656-8.656S261.108,283.268,256.328,283.268z"/>
				<path style="fill: #fff" d="M401.048,325.241H111.606c-4.78,0-8.656,3.876-8.656,8.656s3.876,8.656,8.656,8.656h289.443
					c4.78,0,8.656-3.876,8.656-8.656S405.829,325.241,401.048,325.241z"/>
				<path style="fill: #fff" d="M256.328,367.214H111.606c-4.78,0-8.656,3.876-8.656,8.656c0,4.78,3.876,8.656,8.656,8.656h144.723
					c4.78,0,8.656-3.876,8.656-8.656C264.984,371.089,261.108,367.214,256.328,367.214z"/>
			</svg>
			<a href="http://swiatwebmasterow.pl/poldent/content/8-regulamin-szkolen">
			<div>
				<p class="first-word">Regulamin</p>
				<p class="secound-word">szkoleń</p>
			</div>
			</a>
		</div>
	
</div>

</div>

</div>

{/if}
<pre>
{$categories|var_dump}
</pre>
</section>
