{if $categories && $categories.level_depth < 3}
	{foreach $categories as $category}
		<ul>
			<li class="depth_{$category.level_depth}">
				<a href="{$category.link}">{$category.name} </a> 
				{if $category.children|@count > 0 }
					{include file="./category-tree-branch.tpl" categories=$category.children}
				{/if}
			</li>
		 </ul>
	{/foreach}
{/if}
