{capture name="path"}Podsumowanie{/capture}
<div class="background-white confirm">
{if $status == 'OK'}
    <p class="title-block-confirm">Dziękujemy, wszystko przebiegło pomyślnie.</p>
{else}
    <p class="title-block-confirm">Coś poszło nie tak, spróbuj ponownie.</p>
{/if}