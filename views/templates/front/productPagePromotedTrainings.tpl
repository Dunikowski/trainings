{if $page_name == 'category' }

	<div class="trainings col-lg-4 col-xl-4 col-md-12 col-sm-12 col-xs-12">
	<div id="trainings">
		<div class="trainings-right white-block">
			{if $promoted_trainings}
				<h3 class="title">Wyróżnione szkolenia: </h3>
				
				<div class="slick-promoted">
				{foreach from=$promoted_trainings item=training}
				<div class="promoted-content">
				
				
				{assign var=params value=[
					'id_training' => $training.id_training,
					'alias' => $training.alias,
					'page' => 1
				]}
				{if $training.image}
				<div class="img-training">
				
					<a href="{$link->getModuleLink('trainings', 'single', $params)}">
						<img src="{$base_dir}modules/{$training.image}"/>
					</a>
				</div>
				{/if}
				<p class="date-promoted information"> {$training.start_time|date_format:"%d %B %Y"}</p>
				<p class="information"> <a href="{$link->getModuleLink('trainings', 'single', $params)}">{$training.name}</a> </p>
				<p class="name-instructor information">
					<span>
					{assign var=instructor value=[
						'id_instructor' => {$training.instructors.id_training_instructor}
					]}
					 <a href="{$link->getModuleLink('trainings', 'singleInstructor', $instructor)}">{$training.instructors.name}</a>
					 </span>
				<span class="place-promoted">{$training.place_of_training}</span></p>
				
			
				</div>
				{/foreach}
				
				</div>
			{/if}
		</div>
	</div>		
</div>
{else}
		<div class="trainings-right">
			{if $promoted_trainings}
				<h3 class="title">Wyróżnione szkolenia: </h3>
				
				<div class="slick-promoted">
				{foreach from=$promoted_trainings item=training}
				<div class="promoted-content">
				
				
				{assign var=params value=[
					'id_training' => $training.id_training,
					'alias' => $training.alias,
					'page' => 1
				]}
				{if $training.image}
				<div class="img-training">
					<a href="{$link->getModuleLink('trainings', 'single', $params)}">
						<img src="{$base_dir}modules/{$training.image}"/>
					</a>
				</div>
				{/if}
				<p class="date-promoted information"> {$training.start_time|date_format:"%d %B %Y"}</p>
				<p class="information"> <a href="{$link->getModuleLink('trainings', 'single', $params)}">{$training.name}</a> </p>
				<p class="name-instructor information">
					<span>
					{assign var=instructor value=[
						'id_instructor' => {$training.instructors.id_training_instructor}
					]}
					 <a href="{$link->getModuleLink('trainings', 'singleInstructor', $instructor)}">{$training.instructors.name}</a>
					 </span>
				<span class="place-promoted">{$training.place_of_training}</span></p>
				
			
				</div>
				{/foreach}
				
				</div>
			{/if}
		</div>

{/if}
