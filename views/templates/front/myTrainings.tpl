{capture name=path}
	<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
		Moje konto
	</a>
	<span class="navigation-pipe">{$navigationPipe}</span>
	<span class="navigation_page">Moje szkolenia</span>
{/capture}
{* {$instructors|@var_dump} *}
<h1 class="page-subheading">{l s='Moje szkolenia'}</h1>
{if $results}
{foreach from=$results key=tk item=training}
	
	{if $training.id_cart != $b.id_cart}
	{assign var=params value=[
		'id_training' => $training.id_training,
		'alias' => $training.alias,
		'page' => 1
	]}

	<div class="accordion" id="accordionExample">
	  <div class="card">
		<div class="card-header" id="heading_{$training.index}">
		<div class="table-responsive">
		<table class="table table-bordered">
		  <thead>
			<tr>
				<th>Nr zamówienia {$training.id_cart}</th> 
				<th>Nazwa</th>
				<th>Razem</th>
				<th>Metoda płatności</th>
				<th>Status</th>
				<th>Faktura</th>
				<th>Szczegóły</th>
				
			</tr>
		  </thead>
		  <tbody>
			<tr>
				<td class="bold">{$training.index}</td>
				<td>{foreach from=$training.cart_items key=key item=t}
					{if $key == "trainings"}
						{foreach from=$t item=i} 
						{assign var=training_params value=[
							'id_training' => $i.id_training,
							'alias' => $i.alias,
							'page' => 1
						]}
							<p><span class="bold"><a href="{$link->getModuleLink('trainings', 'single', $training_params)}">{$i.name}</a></span> ({$i.quantity}x / {$i.price} zł) </p>
						{/foreach}
					{elseif $key == "bundles"}
						{foreach from=$t item=b} 
						{assign var=bundle_params value=[
							'id_training' => $b.id_bundle,
							'page' => 1
						]}
							<p><span class="bold"><a class="training-name" href="{$link->getModuleLink('trainings', 'bundle', $bundle_params)}">{$b.training_name}</a></span> ({$b.quantity}x / {$b.total_price} zł)</p>
						{/foreach}
					{/if}
					{/foreach}
				</td>
				<td>{$training.total_price_cart} zł</td>
				<td>{if $training.dotpay_payment == '0'}
					Przelew tradycyjny
					{elseif $training.dotpay_payment == '1'}
					dotpay
					{/if}
				</td>
				<td>{$training.status}</td>
				<td> //TODO </td>
				<td>

					<button class="btn button-gray btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse_{$training.index}" aria-expanded="false" aria-controls="collapse_{$training.index}">
						Szczegóły
					</button>

				</td>

			</tr>
		  </tbody>
		</table>
	</div>
	</div>
	
		<div id="collapse_{$training.index}" class="class-end collapse" aria-labelledby="heading_{$training.index}" data-parent="#accordionExample">
		  <div class="card-body">
		   <div class="table-responsive">    
			<table class="table table-bordered">
			{foreach from=$training.cart_items key=key item=t}
				{if $key == "trainings"}
				{foreach from=$t item=i}
			  <thead>
			  <tr><th colspan="7" class="text-center head-table">Szczegóły szkolenia "{$i.name}"</th></tr>
			   <tr><th colspan="7" class="text-right">
			   		<form action="" method="POST" id="remove-order-{$tk}">
					<input type="hidden" value="{$training.index}" name="index">
					<input type="hidden" value="{$training.id_cart}" name="id_cart">
					<input type="hidden" value="{$i.id_training_participant}" name="id_training_participant">
					<input type="hidden" value="{$i.id_cart_detail}" name="id_cart_detail">
					<input type="submit" class="button-gray btn" name="remove-order" value="Anuluj rezerwacje">
					</form>
					</th></tr>
				<tr>
					<th>Nazwa:</th>
					<th>Data rozpoczęcia</th>
					<th>Data zakończenia</th>
					<th>Miejsce szkolenia</th>
					<th>Cena</th>
					<th>Załączniki do szkolenia</th>
				</tr>
			  </thead>
			  <tbody>
				
				<tr>
					{assign var=a value=[
							'id_training' => $i.id_training,
							'alias' => $i.alias,
							'page' => 1
					]}
					<td><a href="{$link->getModuleLink('trainings', 'single', $a)}">{$i.name}</a> </td>
					<td>{$i.start_time}</td>
					<td>{$i.end_time}</td>
					<td>{$i.voivodeship},{$i.place_of_training}</td>
					<td>{$i.price} zł</td>
					<td>
					  {foreach from=$files item=file}
						  {foreach from=$file item=f}
						  {$part = strripos($f, '-')}
							  {if $i.id_training  == substr($f, 0, $part) }
								 <p> <a rel="nofollow" target="_blank" href="http://swiatwebmasterow.pl/poldent/modules/trainings/files/{$f}">{$f}</a></p>
							  {/if}
						  {/foreach}
					{/foreach}
					</td>
				</tr>
					
				{/foreach}
			  </tbody>
			  {elseif $key == "bundles"}
			  {foreach from=$t item=b}
			  	<thead>
				
			  <tr><th colspan="7" class="text-center head-table">W skład pakietu szkoleń" {$b.training_name}" wchodzą:</th></tr>
			  <tr><th colspan="7" class="text-right">
			  		<form method="POST" id="remove-order-{$tk}">
					<input type="hidden" value="{$training.index}" name="index">
					<input type="hidden" value="{$b.id_bundle}" name="id_bundle">
					<input type="hidden" value="{$training.id_cart}" name="id_cart">
					<input type="hidden" value="{$b.id_cart_detail}" name="id_cart_detail">
					<input type="submit" class="button-gray btn" name="remove-order" value="Anuluj rezerwacje">
					</form>
					</th></tr>
				<tr>
					<th>Nazwa</th>
					<th>Data rozpoczęcia</th>
					<th>Data zakończenia</th>
					<th>Miejsce szkolenia</th>
					<th>Cena</th>
					<th>Załączniki do szkolenia</th>
				</tr>
			  </thead>
			  <tbody>
				{foreach from=$b.bundle_trainings key=test item=b_trainings}
				<tr>
					{assign var=training_bundle_params value=[
						'id_training' => $b_trainings.id_training,
						'alias' => $b_trainings.alias,
						'page' => 1
					]}
					<td><a href="{$link->getModuleLink('trainings', 'single', $training_bundle_params)}">{$b_trainings.name}</a> </td>
					<td>{$b_trainings.start_time}</td>
					<td>{$b_trainings.end_time}</td>
					<td>{$b_trainings.voivodeship},{$b_trainings.place_of_training}</td>
					
					<td>{$b_trainings.price} zł</td>
					<td>
					  {foreach from=$files item=file}
						  {foreach from=$file item=f}
						  {$part = strripos($f, '-')}
							  {if $b_trainings.id_training  == substr($f, 0, $part) }
								 <p> <a rel="nofollow" target="_blank" href="http://swiatwebmasterow.pl/poldent/modules/trainings/files/{$f}">{$f}</a></p>
							  {/if}
						  {/foreach}
					{/foreach}
					</td>
				</tr>
				{/foreach}
				{/foreach}
			  </tbody>
			  {/if}
			  {/foreach}
			</table>
			</div>
		  </div>
		</div>
		
	  </div>
	</div>
	{/if}



{/foreach}
{else}
<p class="alert alert-warning">Brak historii szkoleń.</p>
{/if}
<pre>{$results|@var_dump}</pre>
<pre>{$remove_error|@var_dump}</pre>







