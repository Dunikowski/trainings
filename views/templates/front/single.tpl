{capture name="path"}
	<a href="{$link->getPageLink('module-trainings-frontpage', true)|escape:'html':'UTF-8'}">
		{l s='Szkolenia'}
	</a>
	<span class="navigation-pipe">{$navigationPipe}</span>
		<a href="{$base_dir}szkolenia/{$breadcrumbs.category_name|lower}-{$breadcrumbs.id_category}/1"> {$breadcrumbs.category_name} </a>
	<span class="navigation-pipe">{$navigationPipe}</span>
	{$training.name}

	{/capture}
	

<section id="section-trainings-single">
<div class="wrap">
    <div class="col-xl-8 col-lg-8 col-md-8 col-xs-12 col-12 box-left">
		   <p class="free-slots">
		   {if $free_places > 0}
			   <span>{$free_places}</span> wolnych miejsc
			   {elseif $free_places == 0 || $free_places <= 0}
			   <span>Brak</span> wolnych miejsc
			   {/if}
		   </p>
			
			<div class="logo-tranings">
			{if $training.logo}
				<img src="{$base_dir}modules/{$training.logo}"/>
			{/if}
			</div>
		<p id="name" class="name">{$training.name}</p>
		   {assign var=params value=[
				'id_instructor' => $instructor.id_training_instructor
			]}
			<p class="text-trainings">
				<span> Prelegent: </span>
				<a href="{$link->getModuleLink('trainings', 'singleInstructor', $params)}">{$instructor.name}</a>
			</p>
		

    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-xs-12 col-12 box-right">
    <p>{if $training.for_whom = 1}
        <span class="text-bold">Szkolenie dla:</span> studentów
    {elseif $training.for_whom = 2}
		 <span class="text-bold">Szkolenie dla:<span> lekarzy
    {/if} </p>

	<div class="time-training"><p class="text-bold time-left">Data i godzina: </p> 
		<p class="time-right"><span>{$training.start_time|date_format:"%d.%m.%Y %H:%M" } - </span> 
		<span>{$training.end_time|date_format:"%d.%m.%Y %H:%M" }</span>
		</p>
	</div>
   
    <p><span class="text-bold">Miejsce:</span> {$training.voivodeship}, {$training.place_of_training}</p>
    <p class="trainings-price" >Cena za szkolenie: <span>{$training.price} zł </span></p>

    {if $free_places <= 2 && $free_places > 0}
		<p class="slots-information">Zostały już tylko <span>{$free_places}</span> wolne miejsca, Nie zwlekaj! Kup szkolenie.</p>
		<p> Liczba uczestników: </p>
		<input type="number" value="1" name="num_people" min=1 max={$free_places}>
		<button id="submit" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
			Zapisz się
		</button>
	{elseif $free_places < 0}
		<span>Brak wolnych miejsc</span>
    {elseif $free_places != 0 }
		<p> Liczba uczestników: </p>
		<input type="number" value="1" name="num_people" min=1 max={$free_places}>
		<button id="submit" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
			Zapisz się
		</button>
    {elseif $free_places == 0 && $training.reserve_list == 1}
   
    <form id="savedate" class="form-horizontal" method="POST">
    <fieldset> 
        <!-- Form Name -->
        <p class="slots-information">Powiadom mnie jeżeli zwolni się miejsce na to szkolenie</p>
	
        {if isset($registrationErrors)}
            <div class="alert alert-danger">
                <p>Popraw następujące błędy:</p>
                <ol>
                    {foreach $registrationErrors as $error}
                        {foreach $error as $msg}
                            {$msg} <br>
                        {/foreach}
                    {/foreach}
                </ol>
            </div>
        {/if}
        
    
            <div class="training" data-id_training="{$training.id_training}">
            
                <div class="training-list" data-training-list="{$training.id_training}">
                    
                        <div class="training-list-item" >
                        

                            {assign var="postTraining" value="training_{$training.id_training}"}

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="training_{$training.id_training}[firstname]">Imię</label>
                                <div class="col-md-9">
                                    <input  name="firstname"
                                            type="text"
                                            class="form-control input-md"
                                            required
                                            {strip}value="{if isset($smarty.post.$postTraining.firstname)}{$smarty.post.$postTraining.firstname}{/if}"{/strip}>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="training_{$training.id_training}[lastname]">Nazwisko</label>  
                                <div class="col-md-9">
                                    <input  name="lastname"
                                            type="text"
                                            class="form-control input-md"
                                            required
                                            {strip}value="{if isset($smarty.post.$postTraining.lastname)}{$smarty.post.$postTraining.lastname}{/if}"{/strip}>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="training_{$training.id_training}[email]">Email</label>  
                                <div class="col-md-9">
                                    <input  name="email"
                                            type="text"
                                            class="form-control input-md"
                                            required
                                            {strip}value="{if isset($smarty.post.$postTraining.email)}{$smarty.post.$postTraining.email}{/if}"{/strip}>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="training_{$training.id_training}[telephone]">Telefon</label>  
                                <div class="col-md-9">
                                    <input  name="telephone"
                                            type="text"
                                            class="form-control input-md"
                                            required
                                            {strip}value="{if isset($smarty.post.$postTraining.telephone)}{$smarty.post.$postTraining.telephone}{/if}"{/strip}>
                                </div>
                            </div>
                        </div>
                    
                </div>
            </div>
        
    </fieldset>
        <input type="submit" value="Powiadom mnie jeżeli zwolni się miejsce na to szkolenie" class="btn btn-primary" name="savedate">
    </form> 
    {/if}
	
	{if $bundles_title}
		<p class="info-bundles-box">
		<svg id="Capa_1" enable-background="new 0 0 524.235 524.235" width="20px" height="20px" viewBox="0 0 524.235 524.235" xmlns="http://www.w3.org/2000/svg">
		<path style="fill:#006da0" d="m262.118 0c-144.53 0-262.118 117.588-262.118 262.118s117.588 262.118 262.118 262.118 262.118-117.588 262.118-262.118-117.589-262.118-262.118-262.118zm17.05 417.639c-12.453 2.076-37.232 7.261-49.815 8.303-10.651.882-20.702-5.215-26.829-13.967-6.143-8.751-7.615-19.95-3.968-29.997l49.547-136.242h-51.515c-.044-28.389 21.25-49.263 48.485-57.274 12.997-3.824 37.212-9.057 49.809-8.255 7.547.48 20.702 5.215 26.829 13.967 6.143 8.751 7.615 19.95 3.968 29.997l-49.547 136.242h51.499c.01 28.356-20.49 52.564-48.463 57.226zm15.714-253.815c-18.096 0-32.765-14.671-32.765-32.765 0-18.096 14.669-32.765 32.765-32.765s32.765 14.669 32.765 32.765c0 18.095-14.668 32.765-32.765 32.765z"/></svg>

		To szkolenie możesz kupić 
        {* <span> <a href="{$link->getModuleLink('trainings', 'bundles', $params)}">w pakiecie: </a> </span> *}
		<ul>
		 {foreach $bundles_title as $title} 
		{assign var=params value=[
			'id_bundle' => $title.id_bundle,
			'page' => 1
		]}
		<li class="info-bundles"><a class="training-name" href="{$link->getModuleLink('trainings', 'bundle', $params)}">{$title.title}</a></li>

		{/foreach}
		</ul></p>
	{/if}
</div>
</div>

<div class="wrap">
    <div class="col-xs-12">
		<div id="trainings-features">
            <ul class="nav" role="tablist">
              {if $training.short_desc}
                <li role="presentation" class="active"><a href="#prod-desc" aria-controls="prod-desc" role="tab" data-toggle="tab">Opis szkolenia</a></li>
              {/if}
              {if $training.long_desc}
                <li role="presentation"><a href="#prod-info" aria-controls="prod-info" role="tab" data-toggle="tab">Program</a></li>
              {/if}
            </ul>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="prod-desc">
             		{if $training.short_desc}
					<section class="page-product-box">
						<div class="rte">{$training.short_desc}</div>
					</section>
					{/if}
              </div>
              <div role="tabpanel" class="tab-pane" id="prod-info">
			  
 		{if $training.long_desc}
			<section class="page-product-box">
				<div class="rte">{$training.long_desc}</div>
			</section>
		{/if}
            </div>
		
		</div>
   </div>
</div>

</section>



{addJsDef training_ajaxurl=$ajaxurl}

{addJsDef trainingId=$training.id_training}
{addJsDef trainingPayment=$training.dotpay_payment}
