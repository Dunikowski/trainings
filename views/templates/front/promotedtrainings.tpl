<div id="trainings">
	<div class="col-xl-8 col-lg-8 col-md-12 col-xs-12 col-12">
		<div class="trainings-left">
		{if $upcoming_trainings}
			<h3 class="title">Rekomendowane szkolenia: </h3>
		<div class="content">
			{foreach from=$upcoming_trainings item=training}
		<div class="upcoming-content">
			{assign var=params value=[
				'id_training' => $training.id_training,
				'alias' => $training.alias,
				'page' => 1
			]}
			<div class="container">
			<div class="col-xl-1 col-lg-1 col-md-2 col-xs-12"> 
				<p class="date-promoted"> {$training.start_time|date_format:"%d %b"}</p>
			</div>
			<div class="col-xl-8 col-lg-8 col-md-8 col-xs-12 col-12">
				<p class="training-name"> <a href="{$link->getModuleLink('trainings', 'single', $params)}">{$training.name}</a> </p>
				<p class="name-instructor "><span>
				{assign var=instructor_t value=[
					'id_instructor' => {$training.instructors.id_training_instructor}
				]}
				 <a href="{$link->getModuleLink('trainings', 'singleInstructor', $instructor_t)}">{$training.instructors.name}</a></span>
				<span class="place-promoted "><i class="icon-map-marker"></i>{$training.place_of_training}</span></p>
				<p>
				{if $training.slots == 2 || $training.slots < 2}
					<span class="span-orange"> Ostatnie miejsca </span>
				{/if}
				{if $smarty.now <= strtotime($training.training_time_create)+(60*60*24*20)} 
					<span class="span-new"> Nowość </span>
				{/if}
				</p>
				
			</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-xs-12 col-12">
			<a class="button-check" href="{$link->getModuleLink('trainings', 'single', $params)}">Sprawdź ></a>
			</div>
			</div>
		</div>
			{/foreach}
			</div>
		{/if}
		</div>
	</div>
	<div class="trainings col-xl-4 col-lg-4 col-md-12 col-xs-12 col-12">
		<div class="trainings-right">
			{if $promoted_trainings}
				<h3 class="title">Wyróżnione szkolenia: </h3>
				
				<div class="slick-promoted">
				{foreach from=$promoted_trainings item=training}
				<div class="promoted-content">
					{assign var=params value=[
						'id_training' => $training.id_training,
						'alias' => $training.alias,
						'page' => 1
					]}
				<div class="img-training">
					<a href="{$link->getModuleLink('trainings', 'single', $params)}">
						<img src="{$base_dir}modules/{$training.image}"/>
					</a>
				</div>
				<p class="date-promoted information"> {$training.start_time|date_format:"%d %B %Y"}</p>
				<p class="information"> <a href="{$link->getModuleLink('trainings', 'single', $params)}">{$training.name|truncate:220:'...'}</a> </p>
				<p class="name-instructor information"><span>
				{assign var=instructor value=[
					'id_instructor' => {$training.instructors.id_training_instructor}
				]}
				 <a href="{$link->getModuleLink('trainings', 'singleInstructor', $instructor)}">{$training.instructors.name}</a></span>
				<span class="place-promoted">{$training.place_of_training}</span></p>
				
			
				</div>
				{/foreach}
				
				</div>
			{/if}
		</div>
	</div>
</div>




