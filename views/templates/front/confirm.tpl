
{capture name="path"}Potwierdzenie uczestnictwa{/capture}
<div class="background-white confirm">

{if isset($rodo_ok)}
    <p class="title-block-confirm">Dziękujemy za potwierdzenie</p>
{else}
    <p class="title-block-confirm">Potwierdź uczestnictwo w szkoleniu.
    <form method="post">
        <p class="check-confirm"><input type="checkbox" name="rodo_ok"> <label> Wyrażam zgodę na rodo</label></p>
        <p class="button-form"><input type="submit" name="submit-rodo" value="Potwierdź"></p>
    </form>
{/if}

</div> 