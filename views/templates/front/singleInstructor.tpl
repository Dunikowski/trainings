{capture name="path"}{$instructor.name}{/capture}
<section id="section-trainings-instructor">
<div class="wrap">
	<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12"> <img src="{$base_dir}modules/trainings/img/instructors/{$instructor.instructor_image}"> </div>

	<div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
		<p><span class="text-bold blue">{$instructor.name}</span></p>
		<p>{$instructor.description}</p>
	</div>
</div>

<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 trainings-instructor">
	<p>Aktualnie prowadzone szkolenia: </p>
	<div class="table-responsive">
		<table class="table table-bordered">
			<tr>
				<td>Nazwa</td>
				<td>Data rozpoczęcia</td>
				<td>Data zakończenia</td>
				<td>Miejsce szkolenia</td>
				<td>Województwo</td>
			</tr>
		{foreach from=$trainings item=training}
		{if $training.active == 1}
			{assign var=params value=[
				'id_training' => $training.id_training,
				'alias' => $training.alias,
				'page' => 1
			]}
			<tr>    
				<td class="text-bold"> <a href="{$link->getModuleLink('trainings', 'single', $params)}">{$training.name}</a></td>
				<td> {$training.start_time}</td>
				<td> {$training.end_time}</td>
				<td>{$training.place_of_training}</td>
				<td>{$training.voivodeship}</td>
			</tr>
			{/if}
		{/foreach}
		</table>
	</div>
</div>

</section>


