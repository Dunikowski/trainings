<?php

class TrainingOrder extends ObjectModel
{
    public $id_training_order;
    public $index;
    public $id_customer;
    public $id_cart;
    public $status;

    public static $definition = array(
        'table' => 'training_order',
        'primary' => 'id_training_order',
        'multilang' => false,
        'fields' => array(
            'index' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_cart' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'status' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true)
        ),
    );

    public static function getOrderTrainings($id)
    {
        if($id){
            $trainings = Db::getInstance()->executeS('
            SELECT t.* FROM `' . _DB_PREFIX_ . 'training_order` o
            LEFT JOIN `' . _DB_PREFIX_ . 'training_cart_detail` t
            ON o.`id_cart` = t.`id_cart`
            WHERE o.`id_training_order` = ' . $id . ' AND t.`id_training` IS NOT NULL
        ');
       
        foreach ($trainings as &$training) {
            $signedParticipants = Db::getInstance()->executeS('
                SELECT * FROM `' . _DB_PREFIX_ . 'training_participant`
                WHERE `id_cart` = ' . $training['id_cart'] . '
                AND `id_training` = ' . $training['id_training'] . '
            ');
            $training['signedParticipants'] = $signedParticipants;
        }

        return $trainings;
        }
       
    }
    public static function getOrderBundles($id)
    {
       
         $bundles = Db::getInstance()->executeS('
         SELECT t.* FROM `' . _DB_PREFIX_ . 'training_order` o
         INNER JOIN `' . _DB_PREFIX_ . 'training_cart_detail` t
         ON o.`id_cart` = t.`id_cart`
         WHERE o.`id_customer` = ' . $id . ' AND t.`id_bundle` IS NOT NULL
     ');

        if($bundles){
            foreach ($bundles as $bundle) {
                $trainings = Db::getInstance()->executeS('
                    SELECT t.* FROM `' . _DB_PREFIX_ . 'bundle` b
                    INNER JOIN `' . _DB_PREFIX_ . 'bundle_trainings` bt ON b.`id_bundle` = bt.`id_bundle`
                    INNER JOIN `' . _DB_PREFIX_ . 'training` t ON bt.`id_training` = t.`id_training`
                    WHERE b.`id_bundle` = ' . $bundle['id_bundle'] . '
                
                ');
                $bundles['trainings'] = $trainings;
            }
        }
        return $bundles;
    }

    public static function getOrders($id_customer){
        $data = Db::getInstance()->executeS('
        SELECT t_o.`id_training_order`,t_o.`id_cart`,t_o.`status`,t_o.`index`, tc.`dotpay_payment` FROM `' . _DB_PREFIX_ . 'training_order` t_o
        INNER JOIN `' . _DB_PREFIX_ . 'training_cart` tc ON t_o.`id_cart` = tc.`id_cart`
        WHERE t_o.`id_customer` = ' . $id_customer . '
    ');
    return $data;
    }

    public static function getOrderBundlesBackend($id,$index)
    {
       
         $bundles = Db::getInstance()->executeS('
         SELECT t.* FROM `' . _DB_PREFIX_ . 'training_order` o
         INNER JOIN `' . _DB_PREFIX_ . 'training_cart_detail` t
         ON o.`id_cart` = t.`id_cart`
         WHERE o.`id_customer` = ' . $id . ' AND o.`index` = \'' . $index . '\' AND t.`id_bundle` IS NOT NULL
     ');

        if($bundles){
            foreach ($bundles as $bundle) {
                $trainings = Db::getInstance()->executeS('
                    SELECT t.* FROM `' . _DB_PREFIX_ . 'bundle` b
                    INNER JOIN `' . _DB_PREFIX_ . 'bundle_trainings` bt ON b.`id_bundle` = bt.`id_bundle`
                    INNER JOIN `' . _DB_PREFIX_ . 'training` t ON bt.`id_training` = t.`id_training`
                    WHERE b.`id_bundle` = ' . $bundle['id_bundle'] . '
                
                ');
                $bundles['trainings'] = $trainings;
            }
        }
        return $bundles;
    }
    public static function getAllTrainingOrder($id)
    {
        $index = Db::getInstance()->executeS('
           SELECT * FROM `' . _DB_PREFIX_ . 'training_order`
           WHERE `id_customer` = ' . $id . '
       ');
        return $index;
    }


    //Funkcja pobierająca certyfikat dla danego użytkownika


    public static function getParticipantCertyficate($id_order)
    {
        $attachments = array();

        $directory = _PS_MODULE_DIR_ . 'trainings/certificates/' . $id_order;

        if (file_exists($directory)) {
            $allFiles = scandir($directory);

            if ($allFiles) {
                foreach ($allFiles as $file) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    // $find = "-";
                    //$string = strripos($file, $find);
                    // $new_file = substr($file, 0, $string + 1);
                    //if (substr($file, 0, 2) == $id . '-')
                    // if ($new_file == $id_training_participant . '-')
                    $attachments[] = $file;
                }
            }
        }

        return $attachments;
    }

    public static function deleteCart($id_cart){
        
        Db::getInstance()->delete('training_order', 'id_cart = '.$id_cart);
    }
}
