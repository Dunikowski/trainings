<?php

class Training extends ObjectModel
{

    /**
     * Odwzorowanie pól z bazy danych
     */

    public $id_training;
    public $id_training_category;
    public $name;
    public $alias;
    public $author;
    public $id_training_instructor;
    public $start_time;
    public $end_time;
    public $price;
    public $discount_price;
    public $slots;
    public $short_desc;
    public $long_desc;
    public $sms_notifications;
    public $need_doc_number;
    public $can_be_sold_alone;
    public $is_promoted;
    public $eligible_group;
    public $training_provider;
    public $dotpay_payment;
    public $date_add;
    public $place_of_training;
    public $voivodeship;
    public $for_whom;
    public $reserve_list;
    public $active;
    public static $definition = array(
        'table' => 'training',
        'primary' => 'id_training',
        'multilang' => false,
        'fields' => array(
           
            'id_training_category' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'name' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
            'alias' => array('type' => self::TYPE_STRING, 'validate' => 'isLinkRewrite', 'required' => true),
            'author' => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true),
            'id_training_instructor' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'start_time' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
            'end_time' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
            'price' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
            'discount_price' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
            'slots' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'short_desc' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml'),
            'long_desc' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml'),
            'sms_notifications' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'need_doc_number' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'can_be_sold_alone' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'is_promoted' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'eligible_group' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'training_provider' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'dotpay_payment' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'place_of_training' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'voivodeship' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'for_whom' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'reserve_list' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
        ),
    );

    /** 
     * Funkcja pobierająca wszystkie szkolenia
     */

    public static function getAllTrainings()
    {
        $trainings = Db::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'training`');
        return $trainings;
    }

    /**
     * Funckaj zwracająca ilość wszystkich szkoleń
     */

    public static function getNbTrainings()
    {
        $nb_trainings = Db::getInstance()->executeS('SELECT COUNT(*) FROM `' . _DB_PREFIX_ . 'training`');
        return (int) $nb_trainings[0]['COUNT(*)'];
    }

    /**
     * Funkja zwracająca ilość szkoleń w kategorii
     */

    public static function getNbTrainingsCategory($id)
    {
        $nb_trainings = Db::getInstance()->executeS('SELECT COUNT(*) FROM `' . _DB_PREFIX_ . 'training` WHERE `id_training_category` = ' . (int) $id);
        return (int) $nb_trainings[0]['COUNT(*)'];
    }

    /**
     * Funkcja pobierająca wszystkie szkolenia z podanego zakresu paginacji
     */

    public static function getTrainings($start, $end)
    {
        $trainings = Db::getInstance()->executeS(
            'SELECT * FROM `' . _DB_PREFIX_ . 'training` LIMIT ' . (int) $start . ',' . (int) $end
        );

        foreach ($trainings as &$training) {
            $training['attachments'] = Training::getTrainingAttachments((int) $training['id_training']);
        }

        return $trainings;
    }

    /**
     * Funkcja pobierjąca szkolenia z danej kategorii
     */

    public static function getTrainingsFromCategory($start = 1, $end = 10, $id_category = 1, $voivodeship = '')
    {
        if($voivodeship == ''){
            $trainings = Db::getInstance()->executeS(
                'SELECT * FROM `' . _DB_PREFIX_ . 'training`
                WHERE `id_training_category` = ' . (int) $id_category . ' AND `active` = 1 LIMIT ' . (int) $start . ',' . (int) $end
            );
        } else {
            $trainings = Db::getInstance()->executeS(
                'SELECT * FROM `' . _DB_PREFIX_ . 'training`
                WHERE `id_training_category` = ' . (int) $id_category . ' AND `active` = 1 AND `voivodeship` = "'.$voivodeship.'" LIMIT ' . (int) $start . ',' . (int) $end
            ); 
        }
        

        foreach ($trainings as &$training) {
            $training['attachments'] = Training::getTrainingAttachments((int) $training['id_training']);
        }

        return $trainings;
    }

    /**
     * Funckcja pobierająca pojedyncze szkolenie
     */

    public static function getSingleTraining($id)
    {
        $training = Db::getInstance()->getRow('SELECT * FROM `' . _DB_PREFIX_ . 'training` WHERE  `id_training` = ' . (int) $id);
        return $training;
    }

    /**
     * Funckja pobierająca załączniki do szkolenia
     */

    public static function getTrainingAttachments($id)
    {
        $attachments = array();

        $directory = _PS_MODULE_DIR_ . 'trainings/files';

        if (file_exists($directory)) {
            $allFiles = scandir($directory);

            if ($allFiles) {
                foreach ($allFiles as $file) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    $find = "-";
                    $string = strripos($file, $find);
                    $new_file = substr($file, 0, $string + 1);
                    //if (substr($file, 0, 2) == $id . '-')
                    if ($new_file == $id . '-')
                        $attachments[] = $file;
                }
            }
        }

        return $attachments;
    }

    /**
     * Funkcja pobierająca nadchodzące szkolenia
     */

    public static function getUpcomingTrainings($id_training_category = 0, $voivodeship = 'All')
    {
        if($id_training_category != 0 && $voivodeship != 'All'){
            $trainings = Db::getInstance()
            ->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'training` WHERE `active` = 1 AND `start_time` > NOW() AND `id_training_category` IN ('.$id_training_category.') AND `voivodeship` = "'.$voivodeship.'" ORDER BY `start_time` ASC');

        return $trainings;

        } else if($id_training_category != 0){
            $trainings = Db::getInstance()
            ->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'training` WHERE `active` = 1 AND `start_time` > NOW() AND `id_training_category` IN ('.$id_training_category.')  ORDER BY `start_time` ASC');

            return $trainings;

        }else if($voivodeship != 'All'){

            $trainings = Db::getInstance()
            ->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'training` WHERE `active` = 1 AND `start_time` > NOW() AND `voivodeship` = "'.$voivodeship.'"  ORDER BY `start_time` ASC');

            return $trainings;

        } else {

            $trainings = Db::getInstance()
            ->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'training` WHERE `active` = 1 AND `start_time` > NOW() ORDER BY `start_time` ASC');

        return $trainings;
        }
       
    }

    /**
     * Funkcja pobierająca promowane szkolenia
     */

    public static function getPromotedTrainings()
    {
        $trainings = Db::getInstance()
            ->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'training` WHERE `is_promoted` = 1 AND `active` = 1');
        return $trainings;
    }

    /**
     * Funkcja zapisująca użytkownika na szkolenie
     */

    public static function signParticipant($firstname, $lastname, $email, $phone, $id_training, $id_bundle = null, $id_cart, $moving_reserve_list = false, $send_email = true)
    {
        $mailsDir = _PS_ROOT_DIR_ . '/modules/trainings/views/templates/mails/';
        $token = md5(uniqid($email, true));
        $confirmLink = Context::getContext()->shop->getBaseURL(true) . 'potwierdz-uczestnictwo-w-szkoleniu?token=' . $token;

       
            $sql = Db::getInstance()->insert('training_participant', array(
                'id_training' => $id_training,
                'id_bundle' => $id_bundle,
                'name' => $firstname,
                'surname' => $lastname,
                'email' => $email,
                'phone' => $phone,
                'rodo_ok' => '0',
                'token' => $token,
                'id_cart' => $id_cart
            ));

            if (!$id_shop) {
                $id_shop = Context::getContext()->shop->id;
            }
            if (file_exists(_PS_IMG_DIR_.Configuration::get('PS_LOGO', null, null, $id_shop))) {
                $logo = _PS_IMG_DIR_.Configuration::get('PS_LOGO', null, null, $id_shop);
            } else {
                $logo = '';
            }

            if ($sql) {
                if($send_email){

                    if(!$moving_reserve_list ){
                        Mail::send(
                            1,
                            'confirm',
                            'Potwierdź uczestnictwo w szkoleniu',
                            array(
                                '{shop_logo}' => $logo,
                                '{shop_name}' => Tools::safeOutput(Configuration::get('PS_SHOP_NAME', null, null, $id_shop)),
                                '{shop_url}' => Context::getContext()->link->getPageLink('index', true, Context::getContext()->language->id, null, false, $id_shop),
                                '{confirmLink}' => $confirmLink
                            ),
                            $email,
                            null,
                            null,
                            null,
                            null,
                            null,
                            $mailsDir
                        );

                    } else {
                        Mail::send(
                            1,
                            'confirmmovinglist',
                            'Potwierdź uczestnictwo w szkoleniu',
                            array(
                                '{shop_logo}' => $logo,
                                '{shop_name}' => Tools::safeOutput(Configuration::get('PS_SHOP_NAME', null, null, $id_shop)),
                                '{shop_url}' => Context::getContext()->link->getPageLink('index', true, Context::getContext()->language->id, null, false, $id_shop),
                                '{confirmLink}' => $confirmLink
                            ),
                            $email,
                            null,
                            null,
                            null,
                            null,
                            null,
                            $mailsDir
                        );
                    }
                }
               
              
            }
    }

    // Zwiększenie liczby miejsc po usunięciu użytkownika z panelu
    public static function updateSlots($id)
    {
        $slots = Db::getInstance()->execute('
            UPDATE `' . _DB_PREFIX_ . 'training`
            SET `slots` = `slots` + 1
            WHERE `id_training` = ' . $id . '
            ');
        return $slots;
    }
    public static function updateActive()
    {
      
         Db::getInstance()->execute('
            UPDATE `' . _DB_PREFIX_ . 'training`
            SET `active` = 0
            WHERE `end_time` < "' . Tools::displayDate(date('Y-m-d H:i:s'), null, 1) . '"
            ');
      
    }
    /**
     * Funkcja zapisująca użytkownika na listę rezerwową
     */
    public static function signReserveList($id_training, $firstname, $lastname, $email, $telephone)
    {
        Db::getInstance()->insert('training_reserve_list', array(
            'id_training' => $id_training,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'telephone' => $telephone
        ));
    }

    /**
     * Funkcja pobierająca wszystkich chętnych zapisanych w liście rezerwowej
     */

    public static function getTrainingReserveList($id)
    {
        $list = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_reserve_list` WHERE `id_training` = ' . (int) $id . '
        ');
        return $list;
    }

    /**
     * Funkcja pobierająca informacje czy dane szkolenie posiada listę rezerwową
     */

    public static function hasReserveList($id)
    {
        $bool = Db::getInstance()->executeS('
            SELECT `reserve_list` FROM `' . _DB_PREFIX_ . 'training` WHERE `id_training` = ' . (int) $id . '
        ');
        return $bool;
    }

    /**
     * Funkcja pobierająca informacje o ilości dostępnych miejsc
     */

    public static function getNumberTrainingPlaces($id)
    {
        $bool = Db::getInstance()->getValue('
            SELECT `slots` FROM `' . _DB_PREFIX_ . 'training` WHERE `id_training` = ' . (int) $id . '
        ');
        return $bool;
    }

    /**
     * Funkcja pobierająca dane osoby na liście rezerwowej
     */

    public static function getPeronWithReserveList($id)
    {
        $person = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_reserve_list` WHERE `id_training_reserve_list` = ' . (int) $id . '
        ');
        return $person;
    }


    /**
     * Funkcja usuwająca użytkownika z listy rezerwowej
     */

    public static function deletePeronWithReserveList($id)
    {
        $personDelete = Db::getInstance()->execute('
            DELETE FROM `' . _DB_PREFIX_ . 'training_reserve_list` WHERE `id_training_reserve_list` = ' . (int) $id . '
        ');
        return $personDelete;
    }


    /**
     * Funkcja sprawdzająca czy dany uczestnik jest już zapisany na szkolenia
     */

    public static function isParticipantSigned($email, $id)
    {
        $userExists = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_participant`
            WHERE `id_training` = ' . pSQL($id) . '
            AND `email` = "' . pSQL($email) . '"
        ');

        return $userExists;
    }

    // public static function getTrainigName($id) {
    //     $name = Db::getInstance()->executeS('SELECT `name` AS name FROM ' . _DB_PREFIX_ . 'training WHERE `id_training` = ' . pSQL($id));

    // return $name;
    // }

    /**
     * Funkcja pobierająca wszystkich uczestników danego szkolenia
     */

    public static function getTrainingParticipants($id)
    {
        $participants = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_participant` WHERE `id_training` = ' . (int) $id . '
        ');
        return $participants;
    }

    /**
     * Funkcja pobierająca wszystkie szkolenia, na które jest zapisany klient
     */

    public static function getCustomerTrainings($id)
    {
        $trainings = Db::getInstance()->executeS('
            SELECT t.* FROM `' . _DB_PREFIX_ . 'training_order` o
            LEFT JOIN `' . _DB_PREFIX_ . 'training_cart` c
            ON o.`id_cart` = c.`id_cart`
            LEFT JOIN `' . _DB_PREFIX_ . 'training_cart_detail` cd
            ON c.`id_cart` = cd.`id_cart`
            LEFT JOIN `' . _DB_PREFIX_ . 'training` t
            ON cd.`id_training` = t.`id_training`
            WHERE o.`id_customer` = ' . (int) $id . '
        ');

        return $trainings;
    }
    /**
     * Funkcja pobierająca różne szkolenia klienta
     */

    public static function getDistinctCustomerTrainings($id)
    {
        $trainings = Db::getInstance()->executeS('
        SELECT DISTINCT *, concat(ti.`name`, " ", ti.`surname`) AS instructor, t.`name` AS name_training FROM `' . _DB_PREFIX_ . 'training_order` o
        LEFT JOIN `' . _DB_PREFIX_ . 'training_cart` c
        ON o.`id_cart` = c.`id_cart`
        LEFT JOIN `' . _DB_PREFIX_ . 'training_cart_detail` cd
        ON c.`id_cart` = cd.`id_cart`
        LEFT JOIN `' . _DB_PREFIX_ . 'training` t
        ON cd.`id_training` = t.`id_training`
        LEFT JOIN `' . _DB_PREFIX_ . 'training_instructor` ti
        ON t.`id_training_instructor` = ti.`id_training_instructor`
        WHERE o.`id_customer` = ' . (int) $id . ' AND t.`id_training` IS NOT NULL
    ');

        return $trainings;
    }
    /**
     * Funstion get all bundles which haw training
     * 
     */
    public static function getBundlesTraining($id)
    {
        $instructorTrainings = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'bundle` b
            INNER JOIN `' . _DB_PREFIX_ . 'bundle_trainings` bt
            ON b.`id_bundle` = bt.`id_bundle`
            WHERE bt.`id_training` = ' . (int) $id . ' AND b.`active` = \'1\'
        ');

        return $instructorTrainings;
    }
    /**
     * Funkcja pobierająca wszystkie szkolenia, które prowadzi instruktor
     */

    public static function getInstructorTrainings($id)
    {
        $instructorTrainings = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training`
            WHERE `id_training_instructor` = ' . (int) $id . '
        ');

        return $instructorTrainings;
    }

    // Funkcja usuwająca plik 

    public function deleteFile()
    {

        if (Tools::getIsset('mod_delete_file')) {
            $deletefile = pSQL($_POST['deletefile']);
        }
    }

    public static function getImage($name, $id)
    {
        return Db::getInstance()->getValue('training', array(
            'image' => $name
        ), 'id_training = ' . $id);
    }

    public static function getLogo($name, $id)
    {
        return Db::getInstance()->getValue('training', array(
            'logo' => $name
        ), 'id_training = ' . $id);
    }

    public static function getVoivodeship()
    {
        return Db::getInstance()->executeS('
            SELECT DISTINCT `voivodeship`  FROM `' . _DB_PREFIX_ . 'training` WHERE `active` = 1 AND `start_time` > NOW()
        ');
    }
    public static function searchTrainings($id_lang,$keyword = null,$start = 1,$end = 10)
    {
        if ($keyword == null) {
            return false;
        }
       
        $keyword = pSQL($keyword);
        $words = explode(' ', Search::sanitize($keyword, $id_lang, false, $context->language->iso_code));
        $words = join("|",$words);
         $sql = 'SELECT * FROM '._DB_PREFIX_.'training t
                 WHERE  t.`active` = 1 AND
                 (t.`name` RLIKE "('.$words.')" OR
                  t.`author` RLIKE "('.$words.')" OR
                  t.`short_desc` RLIKE "('.$words.')" OR
                  t.`long_desc` RLIKE "('.$words.')") ORDER BY t.`id_training` DESC LIMIT ' . (int) $start . ',' . (int) $end.'';
         if (!$result = Db::getInstance()->executeS($sql)) {
             return false;
         }

        return $result;
    }
}
