<?php

class TrainingParticipant extends ObjectModel
{
    public $id_training_participant;
    public $id_training;
    public $name;
    public $surname;
    public $email;
    public $rodo_ok;
    public $token;
    public $id_cart;

    public static $definition = array(
        'table' => 'training_participant',
        'primary' => 'id_training_participant',
        'multilang' => false,
        'fields' => array(
            'id_training' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'name' => array('type' => self::TYPE_STRING, 'validate' => 'isName'),
            'surname' => array('type' => self::TYPE_STRING, 'validate' => 'isName'),
            'email' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail'),
            'phone' => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber'),
            'rodo_ok' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'token' => array('type' => self::TYPE_STRING, 'validate' => 'isMd5'),
            'id_cart' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
        )
    );

    public static function getParticipants()
    {
        $participants = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_participant`
        ');
        return $participants;
    }

    public static function getParticipant($id)
    {
        $participant = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_participant` WHERE `id_training_participant` = ' . $id . '
        ');
        return $participant;
    }
    public static function getNbParticipant($id)
    {
        if($id){
            $participant = Db::getInstance()->getValue('
            SELECT count(*) FROM `' . _DB_PREFIX_ . 'training_participant` WHERE `id_training` = ' . $id . '
        ');
        return $participant;
        }
       
    }
    public static function getEmailParticipant($id)
    {
        $participant = Db::getInstance()->getValue('
            SELECT email FROM `' . _DB_PREFIX_ . 'training_participant` WHERE `id_training_participant` = ' . $id . '
        ');
        return $participant;
    }
    public static function deleteParticipant($id)
    {
        return Db::getInstance()->execute('
            DELETE FROM `' . _DB_PREFIX_ . 'training_participant` WHERE `id_training_participant` = ' . $id . '
            ');
    }

  
    public static function delateParticipantBundleTrainings($id_bundle, $id_cart) {
       
        Db::getInstance()->delete('training_participant', 'id_cart = '.$id_cart.' AND id_bundle = '.$id_bundle);
    }

    public static function getParticipantTraining($id)
    {
        $participantTrainings = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_participant` tp
            LEFT JOIN `' . _DB_PREFIX_ . 'training` t
            ON tp.`id_training` = t.`id_training`
            WHERE `id_training_participant` = ' . (int) $id . '
        ');

        return $participantTrainings;
    }

    //Funkcja zapisująca plik dla uczestnika 


    public static function saveCertificateFile($id, $name_file)
    {
        $certificate = Db::getInstance()->execute('
            UPDATE `' . _DB_PREFIX_ . 'training_participant`
            SET `certificate` = ' . $name_file . '
            WHERE `id_training_participant` = ' . $id . '
            ');
        return $certificate;
    }
}
