<?php

class TrainingInstructor extends ObjectModel
{
    public $id_training_instructor;
    public $name;
    public $surname;
    public $description;

    public static $definition = array(
        'table' => 'training_instructor',
        'primary' => 'id_training_instructor',
        'multilang' => false,
        'fields' => array(
            'name' => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true),
            'surname' => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true),
            'description' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
        ),
    );

    public static function getInstructors()
    {
        $instructors = Db::getInstance()->executeS('
            SELECT `id_training_instructor`, concat(`name`, " ", `surname`) AS name, `description`, image as instructor_image  FROM `' . _DB_PREFIX_ . 'training_instructor`;
        ');

        return $instructors;
    }

    public static function getInstructorById($id)
    {
        $instructor = Db::getInstance()->getRow('
            SELECT `id_training_instructor`, concat(`name`, " ", `surname`) AS name, `description`, image as instructor_image FROM `' . _DB_PREFIX_ . 'training_instructor`
            WHERE `id_training_instructor` = ' . $id . '
        ');

        return $instructor;
    }

    public static function setImageName($name, $id)
    {
        return Db::getInstance()->update('training_instructor', array(
            'image' => $name
        ), 'id_training_instructor = ' . $id);
    }

    public static function getInstructorImageLink($id)
    {
        $name = Db::getInstance()->getValue('
            SELECT `image` FROM `' . _DB_PREFIX_ . 'training_instructor`
            WHERE `id_training_instructor` = ' . $id . '
        ');

        $link = _PS_BASE_URL_ . '/modules/trainings/img/instructors/' . $name;

        return $link;
    }


    public static function getNameInstructor($id)
    {

        $name = Db::getInstance()->executeS('
            SELECT ti.`name`, ti.`surname`, t.`id_training` FROM `' . _DB_PREFIX_ . 'training_instructor` ti
            LEFT JOIN `' . _DB_PREFIX_ . 'training` t
            ON ti.`id_training_instructor` = t.`id_training_instructor` 
            WHERE t.`id_training` = ' . $id . '
        ');
        return $name;
    }
}
