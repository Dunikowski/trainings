<?php

class SearchPoldent {

    public static function searchProduct($keyword = null,$start = 0,$end = 10,$orderby= false,$orderway = false)
    {
        
       
        $words = self::prepareWords($keyword);
        $order = '';
        if($orderby && $orderby != 'all' && $orderway){
            if($orderby !='name'){
                $order = 'ORDER BY p.`'.$orderby.'` '.$orderway;
            }else{
                $order = 'ORDER BY pla.`'.$orderby.'` '.$orderway;
            }
            
        }

        if($words != '' && $words) {

            $sql = 'SELECT p.*, pla.`name`,pla.`link_rewrite`,i.`id_image`,c.`link_rewrite` as category_link_rewrite,pfs.`quantity` as quantity_for_sale FROM '._DB_PREFIX_.'product_lang pla
            INNER JOIN `' . _DB_PREFIX_ . 'product` p
            ON pla.`id_product` = p.`id_product`
            LEFT OUTER join `' . _DB_PREFIX_ . 'product_sale` pfs
            ON p.`id_product` = pfs.`id_product`
            INNER JOIN `' . _DB_PREFIX_ . 'image` i
            ON p.`id_product` = i.`id_product`
            INNER JOIN `' . _DB_PREFIX_ . 'category_lang` c
            ON p.`id_category_default` = c.`id_category`
            WHERE  p.`active` = 1 AND p.`id_product` IN (SELECT id_product FROM '._DB_PREFIX_.'product_lang where `description` RLIKE "('.$words.')" OR
            `description_short` RLIKE "('.$words.')" OR
            `name` RLIKE "('.$words.')" ) AND i.`cover` = 1 '.$order.' LIMIT ' . (int) $start * (int) $end. ',' . (int) $end.'';
            
               if (!$result = Db::getInstance()->executeS($sql)) {
                return false;
                }
                
                foreach($result as &$value){
                    foreach($value as $k => &$v){
                       if($k == 'quantity_for_sale'){
                           if($v == null){
                            $v = 0;
                           }
                       }
                    } 
                }
    
            return $result;
        } else {
            return false;
        }
   
    }
    public static function countSearchProduct($keyword = null,$start = 0,$end = 10)
    {
        
       
        $words = self::prepareWords($keyword);
        if($words != '' && $words) {

            $sql = 'SELECT count(p.`id_product`) FROM `'._DB_PREFIX_.'product_lang` pla
            INNER JOIN `' . _DB_PREFIX_ . 'product` p
            ON pla.`id_product` = p.`id_product`
            WHERE  p.`active` = 1 AND p.`id_product` IN (SELECT id_product FROM '._DB_PREFIX_.'product_lang where `description` RLIKE "('.$words.')" OR
            `description_short` RLIKE "('.$words.')" OR
            `name` RLIKE "('.$words.')" ) 
            ';
            
              $result = Db::getInstance()->getValue($sql);

            return $result;
        } 
   
    }

    public static function searchTrainings($keyword = null, $start = 0,$end = 10,$orderby= false,$orderway = false)
    {
        
       
        $words = self::prepareWords($keyword);

        $order = '';
        if($orderby && $orderby != 'all' && $orderway){

            $order = 'ORDER BY t.`'.$orderby.'` '.$orderway;
        }

        if($words != '' && $words) {

            $sql = 'SELECT t.*, concat(ti.`name`, " ", ti.`surname`) AS instructor_name FROM '._DB_PREFIX_.'training t
            LEFT JOIN `' . _DB_PREFIX_ . 'training_instructor` ti
            ON t.`id_training_instructor` = ti.`id_training_instructor`
            WHERE  t.`active` = 1 AND
            (t.`name` RLIKE "('.$words.')" OR
            t.`author` RLIKE "('.$words.')" OR
            t.`short_desc` RLIKE "('.$words.')" OR
            t.`long_desc` RLIKE "('.$words.')") '.$order.' LIMIT ' . (int) $start * (int) $end . ',' . (int) $end.'';
            
               if (!$result = Db::getInstance()->executeS($sql)) {
                return false;
                }

            return $result;
        } else {
            return false;
        }
    }
    public static function countSearchTrainings($keyword = null)
    {
        
       
        $words = self::prepareWords($keyword);

        if($words != '' && $words) {

            $sql = 'SELECT count(*) FROM '._DB_PREFIX_.'training t
            WHERE  t.`active` = 1 AND
            (t.`name` RLIKE "('.$words.')" OR
            t.`author` RLIKE "('.$words.')" OR
            t.`short_desc` RLIKE "('.$words.')" OR
            t.`long_desc` RLIKE "('.$words.')") ' ;
            
               $result = Db::getInstance()->getValue($sql);

            return $result;
        }  
    }
    public static function searchBlog($keyword = null,$start = 0,$end = 10)
    {
        $words = self::prepareWords($keyword);

        if($words != '' && $words) {

            $sql = 'SELECT spl.`id_smart_blog_post`, spl.`meta_title`,spl.`link_rewrite`,spl.`short_description`,sp.`image`  FROM '._DB_PREFIX_.'smart_blog_post sp 
                INNER JOIN '._DB_PREFIX_.'smart_blog_post_lang spl ON sp.`id_smart_blog_post` = spl.`id_smart_blog_post`
                 WHERE  sp.`active` = 1 AND
                 (spl.`content` RLIKE "('.$words.')" OR
                 spl.`meta_title` RLIKE "('.$words.')" OR
                 spl.`meta_description` RLIKE "('.$words.')") ORDER BY sp.`created` DESC LIMIT ' . (int) $start * (int) $end. ',' . (int) $end.'';
            
               if (!$result = Db::getInstance()->executeS($sql)) {
                return false;
                }

            return $result;
        } else {
            return false;
        }
    }
    public static function countBlog($keyword = null)
    {
        $words = self::prepareWords($keyword);

        if($words != '' && $words) {

            $sql = 'SELECT count(spl.`id_smart_blog_post`) FROM '._DB_PREFIX_.'smart_blog_post sp 
            INNER JOIN '._DB_PREFIX_.'smart_blog_post_lang spl ON sp.`id_smart_blog_post` = spl.`id_smart_blog_post`
                 WHERE  sp.`active` = 1 AND
                 (spl.`content` RLIKE "('.$words.')" OR
                 spl.`meta_title` RLIKE "('.$words.')" OR
                 spl.`meta_description` RLIKE "('.$words.')")';
            
            $result = Db::getInstance()->getValue($sql);
                

            return $result;
        } 
    }
    public static function prepareWords($keyword) {

        if ($keyword == null) {
            return false;
        }
       
        $keyword = pSQL($keyword);
        $keyword = trim(preg_replace('/\s+/', ' ', $keyword));
        $keyword = preg_replace('/\s+/', '|', $keyword);
       
        return  $keyword;
    }
}