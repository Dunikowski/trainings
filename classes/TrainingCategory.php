<?php

class TrainingCategory extends ObjectModel
{
    public $id_training_category;
    public $name;
    public $alias;
    public $id_parent;
    public $level_depth;

    public static $definition = array(
        'table' => 'training_category',
        'primary' => 'id_training_category',
        'multilang' => false,
        'fields' => array(
            'id_parent' => array('type' => self::TYPE_INT),
            'name' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
            'alias' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
           
        ),
    );
    // Pobierz kategorie szkoleń 
    public static function getTrainingCategories()
    {
        $categories = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_category`
        ');
        return $categories;
    }

    // Pobierz szkolenia należące do kategorii

    public static function getTrainings($id)
    {
        $trainings = Db::getInstance()->executeS('
            SELECT *, t.`name` AS `nazwa_szkolenia` FROM `' . _DB_PREFIX_ . 'training` t
            LEFT JOIN `' . _DB_PREFIX_ . 'training_category`tc 
            ON t.`id_training_category` = tc.`id_training_category` 
            WHERE tc.`id_training_category` = ' . $id . '
            
        ');
        return $trainings;
    }
    public static function getCategory($id)
    {
        if($id){

            $category = Db::getInstance()->getRow('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_category` 
            WHERE `id_training_category` = ' . $id . '
                
            ');
            return $category;
        }

    }
    public static function getCategoryParent($id)
    {
        if($id){

            $categorys = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_category` 
            WHERE `id_parent` = ' . $id . '
                
            ');
            return $categorys;
        }

    }
}
