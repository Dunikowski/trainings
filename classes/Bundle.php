<?php

class Bundle extends ObjectModel
{
    public $id_bundle;
    public $title;
    public $description;
    public $id_training;
    public $discount;
    public $active;
    
    public static $definition = array(
        'table' => 'bundle',
        'primary' => 'id_bundle',
        'multilang' => false,
        'fields' => array(
            'title' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
            'description' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
           
            'discount' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
        )
    );


    public static function getBundle()
    {
        $bundles = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'bundle` where `active` = 1
        ');
        return $bundles;
    }

    /**
     * Funckcja zapisująca szkolenia
     */

    public static function clearBundleTrainngs($id_bundle){
       return Db::getInstance()->delete('bundle_trainings', 'id_bundle = '.$id_bundle);
        
    }

    public static function saveTraninings($id_bundle, $id_training)
    {
        //$id_training = Tools::getValue('id_training');
        
        $insert = array(
            'id_bundle' => $id_bundle,
            'id_training' => $id_training,
        );
        Db::getInstance()->insert('bundle_trainings', $insert);
    }

    // Funkcja pobierająca zapisane szkolenia dla danego pakietu
    public static function getAllTrainingsBundle($id)
    {
        $trainings = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'bundle_trainings` WHERE `id_bundle` = ' . (int) $id . '
        ');
        return $trainings;
    }

    public static function getBundleById($id)
    {
        $bundle = Db::getInstance()->getRow('
            SELECT `id_bundle`, `title`, `description`, `discount` FROM `' . _DB_PREFIX_ . 'bundle`
            WHERE `id_bundle` = ' . $id . '
        ');

        return $bundle;
    }

    /**
     * Funkcja pobierająca wszystkie szkolenia, na które jest zapisany klient
     */

    public static function getBundleTrainings($id)
    {
        $trainings_bundle = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training` t
            LEFT JOIN `' . _DB_PREFIX_ . 'bundle_trainings` bt
            ON t.`id_training` = bt.`id_training`
            LEFT JOIN `' . _DB_PREFIX_ . 'bundle` b
            ON bt.`id_bundle` = b.`id_bundle`
            WHERE b.`id_bundle` = ' . (int) $id . '
        ');
        return $trainings_bundle;
    }

    public static function getAllActiveBundle()
    {
        $trainings_bundle = Db::getInstance()->executeS('
            SELECT bt.*  FROM `' . _DB_PREFIX_ . 'bundle` b
            INNER JOIN `' . _DB_PREFIX_ . 'bundle_trainings` bt
            ON b.`id_bundle` = bt.`id_bundle`
            WHERE b.`active` = 1
        ');
        return $trainings_bundle;
    }


    public static function checkBundleTrainingsAvilability($id){
        $trainings_bundle = Db::getInstance()->executeS('
        SELECT t.`id_training` FROM `' . _DB_PREFIX_ . 'training` t
        RIGHT JOIN `' . _DB_PREFIX_ . 'bundle_trainings` bt
        ON t.`id_training` = bt.`id_training`
        WHERE bt.`id_bundle` = ' . (int) $id . '
    ');
    return $trainings_bundle;
    }

    public static function getBundleSumTrainings($id)
    {
        $pricebundle = Db::getInstance()->getValue('
        SELECT sum(t.`price`) FROM  `' . _DB_PREFIX_ . 'training` t
        WHERE t.`id_training` IN (select bt.`id_training` FROM `' . _DB_PREFIX_ . 'bundle_trainings` bt WHERE bt.`id_bundle` ='.$id.' )
        ');
        return $pricebundle;
    }

    public static function countBundles()
    {
        $count = Db::getInstance()->getValue('
        SELECT count(*) FROM  `' . _DB_PREFIX_ . 'bundle` b
        WHERE b.`active` = 1
        ');
        return $count;
    }

    public static function getBundleForPagination($page = 1,$nb_per_page = 10){

        if($page == 1) {
            $limit = 'LIMIT '.$nb_per_page;
        } else {
            $limit = 'LIMIT '.$nb_per_page.'  OFFSET '.($page - 1) * $nb_per_page;
        }

        $bundles = Db::getInstance()->executeS('
                    SELECT * FROM `' . _DB_PREFIX_ . 'bundle` where `active` = 1 '.$limit.' 
                ');

        return $bundles;
    }

}
