<?php

class TrainingCart extends ObjectModel
{
    public $id_cart;
    public $id_customer;
    public $is_bought;
    public $dotpay_payment;

    public static $definition = array(
        'table' => 'training_cart',
        'primary' => 'id_cart',
        'multilang' => false,
        'fields' => array(
            'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'is_bought' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'dotpay_payment' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId')
        ),
    );

    public static function addProduct($id, $quantity, $id_cart)
    {
        $product_exists = Db::getInstance()->getRow(
            'SELECT * FROM `' . _DB_PREFIX_ . 'training_cart_detail`
            WHERE `id_training` = ' . $id . ' AND `id_cart` = ' . $id_cart
        );

        $tr = new Training($id);

        if ($product_exists) {
            Db::getInstance()->execute(
                'UPDATE `' . _DB_PREFIX_ . 'training_cart_detail`
                SET `quantity` = `quantity` + ' . (int) $quantity . '
                WHERE `id_training` = ' . (int) $id
            );
        } else {

            Db::getInstance()->insert('training_cart_detail', array(
                'id_training' => pSQL($id),
                'training_name' => $tr->name,
                'quantity' => pSQL($quantity),
                'id_cart' => pSQL($id_cart),
                'price' => $tr->price
            ));

            $training = new Training($id);
            $where = 'id_cart = '.$id_cart;
            Db::getInstance()->update('training_cart', array(
                'dotpay_payment' => $training->dotpay_payment
            ),$where);
        }
    }



    // public static function deleteProduct($id)
    // {
    //     return Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'training_cart_detail` WHERE `id_training` = ' . $id);
    // } 

   

    public static function updateCart($training_id, $qty)
    {
        return Db::getInstance()->execute('UPDATE `' . _DB_PREFIX_ . 'training_cart_detail` SET `quantity` = `quantity` + ' . $qty . ' WHERE `id_training` = ' . $training_id);
    }
    
    public static function updateBundleCart($bundle_id, $qty)
    {
        return Db::getInstance()->execute('UPDATE `' . _DB_PREFIX_ . 'training_cart_detail` SET `quantity` = `quantity` + ' . $qty . ' WHERE `id_bundle` = ' . $bundle_id);
    }

    public static function getCartProducts($id_cart)
    {
        if ($id_cart) {
            $cartItems = Db::getInstance()->executeS('
                SELECT * FROM `' . _DB_PREFIX_ . 'training_cart_detail`
                WHERE `id_cart` = ' . $id_cart . ' 
            ');

            return $cartItems;
        }
    }
   
    public static function getCartValue($id_cart)
    {

        $value = Db::getInstance()->getValue('
            SELECT SUM(`quantity` * `price`) AS cartSum FROM `' . _DB_PREFIX_ . 'training_cart_detail`
            WHERE `id_cart` = ' . $id_cart . '
        ');
        return $value;
    }

    public static function getActiveCart($id_customer)
    {
        $ctx = Context::getContext();

        if ((bool) $ctx->customer->isLogged()) {
            $cart_id = Db::getInstance()
                ->executeS('SELECT DISTINCT id_cart FROM `' . _DB_PREFIX_ . 'training_cart` WHERE `id_customer` = ' . $id_customer . ' AND `is_bought` = 0');
            if ($cart_id)
                return $cart_id[0]['id_cart'];
        }
    }

    public static function makeBought($id)
    {
        return Db::getInstance()->update('training_cart', array(
            'is_bought' => 1
        ), "`id_cart` = '$id'");
    }

    public static function checkPaymentCompatibility($method,$id_cart)
    {
        $compatibility = Db::getInstance()->executeS('
            SELECT COUNT(*) FROM `' . _DB_PREFIX_ . 'training_cart_detail` tcd
            INNER JOIN `' . _DB_PREFIX_ . 'training` t
            ON tcd.`id_training` = t.`id_training`
            WHERE t.`dotpay_payment` != ' . $method . '
            AND tcd.`id_cart` = ' . $id_cart . '
            ');

        return $compatibility[0]['COUNT(*)'];
    }

    public static function checkPaymentCompatibilityTmp($method, $id_cart)
    {
        $compatibility = Db::getInstance()->executeS('
            SELECT COUNT(*) FROM `' . _DB_PREFIX_ . 'training_cart_tmp` tcd
            INNER JOIN `' . _DB_PREFIX_ . 'training` t
            ON tcd.`id_training` = t.`id_training`
            WHERE t.`dotpay_payment` != ' . $method . '
            AND tcd.`id_cart` = ' . $id_cart . '
            ');

        return $compatibility[0]['COUNT(*)'];
    }
    // SELECT COUNT(*) FROM `ps_training_cart_tmp` tcd INNER JOIN `ps_training` t ON tcd.`id_training` = t.`id_training` WHERE t.`dotpay_payment` != 1 AND tcd.`id_cart` = 18;	
    // Faza testowa 
    public static function addProductTmp($id, $quantity, $id_cart)
    {
        $product_exists = Db::getInstance()->getRow(
            'SELECT * FROM `' . _DB_PREFIX_ . 'training_cart_tmp`
        WHERE `id_training` = ' . $id . ' AND `id_cart` = ' . $id_cart
        );

        $tr = new Training($id);

        if ($product_exists) {
            Db::getInstance()->execute(
                'UPDATE `' . _DB_PREFIX_ . 'training_cart_tmp`
            SET `quantity` = `quantity` + ' . (int) $quantity . '
            WHERE `id_training` = ' . (int) $id
            );
        } else {

            Db::getInstance()->insert('training_cart_tmp', array(
                'id_training' => pSQL($id),
                'training_name' => $tr->name,
                'quantity' => pSQL($quantity),
                'id_cart' => pSQL($id_cart),
                'price' => $tr->price
            ));

            $training = new Training($id);

            Db::getInstance()->update('training_cart_tmp', array(
                'dotpay_payment' => $training->dotpay_payment
            ));
        }
    }
    

    public static function getBundleCartProducts($id_cart)
    {
        if ($id_cart) {
            $cartItems = Db::getInstance()->executeS('
                SELECT `id_bundle` FROM `' . _DB_PREFIX_ . 'training_cart_detail`
                WHERE `id_cart` = ' . $id_cart . ' AND `id_bundle` IS NOT NULL 
            ');

            return $cartItems;
        }
    }
  

    public static function addBundleProducts($id, $quantity, $id_cart)
    {
        $product_exists = Db::getInstance()->getRow(
            'SELECT * FROM `' . _DB_PREFIX_ . 'training_cart_detail`
            WHERE `id_bundle` = ' . $id . ' AND `id_cart` = ' . $id_cart
        );

        $tr = new Bundle($id);

        if ($product_exists) {
            Db::getInstance()->execute(
                'UPDATE `' . _DB_PREFIX_ . 'training_cart_detail`
                SET `quantity` = `quantity` + ' . (int) $quantity . '
                WHERE `id_bundle` = ' . (int) $id
            );
        } else {

            Db::getInstance()->insert('training_cart_detail', array(
                'id_bundle' => pSQL($id),
                'training_name' => $tr->title,
                'quantity' => pSQL($quantity),
                'id_cart' => pSQL($id_cart),
                'price' => Bundle::getBundleSumTrainings($id)
            ));
          
        }
    }
   
    public static function getCartProductsTmp($id_cart)
    {
        if ($id_cart) {
            $cartItems = Db::getInstance()->executeS('
                SELECT * FROM `' . _DB_PREFIX_ . 'training_cart_tmp`
                WHERE `id_cart` = ' . $id_cart . ' 
            ');

            return $cartItems;
        }
    }
    public static function countItemsByIdCart($id_cart){
        
            $value = Db::getInstance()->getValue('
                SELECT Count(`id_cart`) FROM `' . _DB_PREFIX_ . 'training_cart_detail`
                WHERE `id_cart` = ' . $id_cart . '
            ');
            return $value;
       
    }

    public static function deleteBundleProducts($id_bundle,$id_cart)
    {
       return Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'training_cart_detail` WHERE `id_bundle` = ' . $id_bundle .' AND `id_cart` = ' . $id_cart);
    }

    public static function deleteProduct($id_training,$id_cart)
    {
        return Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'training_cart_detail` WHERE `id_training` = ' . $id_training .' AND `id_cart` = ' . $id_cart);
    }

    public static function deleteProductsByIdCart($id_cart)
    {
        return Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'training_cart_detail` WHERE `id_cart` = ' . $id_cart);
    }

    public static function getCartDetailsTrainings($id_cart){
        $data = Db::getInstance()->executeS('
        SELECT DISTINCT td.`id_cart_detail`,td.`quantity`,td.`price` as total_price,tp.`id_training_participant`,t.* FROM `' . _DB_PREFIX_ . 'training_cart_detail` td
        INNER JOIN `' . _DB_PREFIX_ . 'training` t ON td.`id_training` = t.`id_training`
        INNER JOIN `' . _DB_PREFIX_ . 'training_participant` tp ON t.`id_training` = tp.`id_training`
        WHERE td.`id_cart` = ' . $id_cart . ' And td.`id_training` is not NULL AND (tp.`id_bundle` = 0 OR tp.`id_bundle` = NULL) AND tp.`id_cart`= ' . $id_cart . '
    ');
    return $data;
    }
    
    public static function getCartDetailsBundle($id_cart){

        $data = Db::getInstance()->executeS('
        SELECT td.`id_cart_detail`,td.`quantity`,td.`price` as total_price,td.`id_bundle`,td.`training_name` FROM `' . _DB_PREFIX_ . 'training_cart_detail` td
        WHERE td.`id_cart` = ' .$id_cart . ' AND td.`id_bundle` is not NULL
    ');

    return $data;
    }

    public static function getTraningDataForMyTranings($id_cart,$id_bundle){
        $data = Db::getInstance()->executeS('
        SELECT tp.`id_bundle`,tp.`id_cart`, t.*  FROM `' . _DB_PREFIX_ . 'training` t 
        INNER JOIN `' . _DB_PREFIX_ . 'training_participant` tp ON t.`id_training` = tp.`id_training`
        WHERE tp.`id_bundle` = '.$id_bundle.' AND tp.`id_cart` = ' . $id_cart . '
    ');
    return $data;
    }

    public static function cancelTrainingBundel($id_cart_detail) {
       
        Db::getInstance()->delete('training_cart_detail', 'id_cart_detail = '.$id_cart_detail);
    }

    public static function deleteCart($id_cart){
        
        Db::getInstance()->delete('training_cart', 'id_cart = '.$id_cart);
    }

    public static function getCustomerPhoneNumber($id){
        if($id){
            return Db::getInstance()->getValue('SELECT `phone`  FROM `' . _DB_PREFIX_ . 'address` WHERE `id_customer`='.$id.' ');
        }
        
    }
}
