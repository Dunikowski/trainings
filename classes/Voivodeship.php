<?php

class Voivodeship {

    private static $options = [

         array(

            'voivodeship_name' => 'Dolnośląskie', 

            'name' => 'Dolnośląskie' 

         ),
        array(

            'voivodeship_name' => 'Kujawsko-Pomorskie', 

            'name' => 'Kujawsko Pomorskie' 

        ),
         array(

            'voivodeship_name' => 'Lubelskie', 

            'name' => 'Lubelskie' 

         ),
          array(

            'voivodeship_name' => 'Łódzkie', 

            'name' => 'Łódzkie' 

          ),
           array(

            'voivodeship_name' => 'Małopolskie', 

            'name' => 'Małopolskie' 

           ),
            array(

            'voivodeship_name' => 'Mazowieckie', 

            'name' => 'Mazowieckie' 

            ),
             array(

            'voivodeship_name' => 'Opolskie', 

            'name' => 'Opolskie' 

             ),
              array(

            'voivodeship_name' => 'Podkarpackie', 

            'name' => 'Podkarpackie' 

              ),
              array(

            'voivodeship_name' => 'Podlaskie', 

            'name' => 'Podlaskie' 

              ),
              array(

            'voivodeship_name' => 'Pomorskie', 

            'name' => 'Pomorskie' 

              ),
              array(

            'voivodeship_name' => 'Śląskie', 

            'name' => 'Śląskie' 

              ),
              array(

            'voivodeship_name' => 'Świętokrzyskie', 

            'name' => 'Świętokrzyskie' 

              ),
              array(

            'voivodeship_name' => 'Warmińsko-Mazurskie', 

            'name' => 'Warmińsko-Mazurskie' 

              ),
              array(

            'voivodeship_name' => 'Wielkopolskie', 

            'name' => 'Wielkopolskie' 

              ),
              array(

            'voivodeship_name' => 'Zachodniopomorskie', 

            'name' => 'Zachodniopomorskie' 

        )
              ];

  public static function getOptions() {
      return self::$options;
  }
}
