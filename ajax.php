<?php

require_once(dirname(__FILE__) . '../../../config/config.inc.php');
require_once(_PS_MODULE_DIR_ . 'trainings/classes/TrainingCart.php');
require_once(_PS_MODULE_DIR_ . 'trainings/trainings.php');

if ($_POST['method']) {
    $prod_id = $_POST['id'];
    $quantity = $_POST['qty'] ?? '';
    $id_cart = $_POST['id_cart'];
    //For search query
    $name = $_POST['name'];
    $page = $_POST['page'];
    $original_query = $_POST['original_query'];
    $selected = $_POST['selected'];
    switch ($_POST['method']) {
        case "t_addToCart":
            t_addToCart($prod_id, $quantity);
            break;
        case "t_addToCartBundle":
            t_addToCartBundle($prod_id, $quantity);
            break;
        case "t_deleteFromCart":
            t_deleteFromCart($prod_id,$id_cart);
            break;
        case "b_deleteFromCart":
            b_deleteFromCart($prod_id,$id_cart);
            break;
        case "t_updateCart":
            t_updateCartQty($prod_id, $quantity);
            break;
        case "b_updateCart":
            b_updateCartQty($prod_id, $quantity);
            break;
        case "search_poldent":
            search_poldent($name, $page, $original_query, $selected);
        break;
    }
}

function t_addToCart($id, $quantity)
{
    $context = Context::getContext();
    $customer_id = $context->customer->id;
    $cart_id = TrainingCart::getActiveCart($customer_id);
    $training = new Training($id);
    $payment_method = $training->dotpay_payment;


    if ($cart_id) {
        // if (!TrainingCart::checkPaymentCompatibility($payment_method)) {
        if (!TrainingCart::checkPaymentCompatibility($payment_method, $cart_id)) {
            TrainingCart::addProduct($id, $quantity, $cart_id);
            //TrainingCart::addProductTmp($id, $quantity, $cart_id);
            echo 'add_to_cart_ok';
        } else {
            echo 'different_payment';
        }
    } else {
        $cart = new TrainingCart();
        $cart->id_customer = $customer_id;
        $cart->save();

        TrainingCart::addProduct($id, $quantity, $cart->id);
        $where = 'id_cart = '.$id_cart;
        Db::getInstance()->update('training_cart', array(
            'dotpay_payment' => '1'
        ),$where);
       // TrainingCart::addProductTmp($id, $quantity, $cart->id);
        echo 'add_to_cart_ok';
    }
}

function t_addToCartBundle($id, $quantity){
    $context = Context::getContext();
    $customer_id = $context->customer->id;
    $cart_id = TrainingCart::getActiveCart($customer_id);
    
    if ($cart_id) {
        TrainingCart::addBundleProducts($id, $quantity, $cart_id);
        
        echo 'add_to_cart_ok';
    } else {

        $cart = new TrainingCart();
        $cart->id_customer = $customer_id;
        $cart->save();
        
        TrainingCart::addBundleProducts($id, $quantity, $cart->id);
       
        echo 'add_to_cart_ok';
    }
}

function t_deleteFromCart($prod_id,$id_cart)
{
    //TrainingCart::deleteProduct($prod_id);
    TrainingCart::deleteProduct($prod_id,$id_cart);
  
}
function b_deleteFromCart($prod_id,$id_cart)
{
    // TrainingCart::deleteBundleProduct($prod_id,$id_cart);
    // $i = TrainingCart::productsInCart($id_cart);
    // if($i == 0){
    //     TrainingCart::delateCartProduct($id_cart);
    // }
    TrainingCart::deleteBundleProducts($prod_id,$id_cart);
}
function t_updateCartQty($prod_id, $qty)
{
    TrainingCart::updateCart($prod_id, $qty);
}
function b_updateCartQty($prod_id, $qty)
{
    TrainingCart::updateBundleCart($prod_id, $qty);
}

function search_poldent($name, $page, $original_query, $selected = false){

    $per_page = (Configuration::get('PS_PRODUCTS_PER_PAGE'))?Configuration::get('PS_PRODUCTS_PER_PAGE'):10;
    $pieces = [];
    if($name == 'trainings'){

        if($selected){
            $pieces = explode(":", $selected);
        }
        if(!empty($pieces) && $pieces[0] != 'all'){
            $training_search = SearchPoldent::searchTrainings($original_query,$page - 1, $per_page, $pieces[0], $pieces[1]);
        } else {
            $training_search = SearchPoldent::searchTrainings($original_query,$page - 1, $per_page);
        }
       
        $result[] = array('name'=> $name,'results'=> $training_search);
        

        echo json_encode($result);
    }else if($name == 'blog'){

        $blog_search = SearchPoldent::searchBlog($original_query,$page - 1, $per_page);
        $result[] = array('name'=> $name,'results'=> $blog_search);
        echo json_encode($result);
    }else if($name == 'products'){

        if($selected){
            $pieces = explode(":", $selected);
        }

        if(!empty($pieces) && $pieces[0] != 'all'){
            $search = SearchPoldent::searchProduct($original_query,$page - 1, $per_page, $pieces[0],$pieces[1]);
        } else {
            $search = SearchPoldent::searchProduct($original_query, $page - 1, $per_page);
        }
       
        $result[] = array('name'=> $name,'results'=>  $search);
        echo json_encode($result);
    }else {
        $result = [];
        echo json_encode($result);
    }
        
}