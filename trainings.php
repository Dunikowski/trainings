﻿<?php

/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__) . '/classes/TrainingCategory.php');
require_once(dirname(__FILE__) . '/classes/Training.php');
require_once(dirname(__FILE__) . '/classes/TrainingCart.php');
require_once(dirname(__FILE__) . '/classes/TrainingOrder.php');
require_once(dirname(__FILE__) . '/classes/Bundle.php');
require_once(dirname(__FILE__) . '/classes/TrainingInstructor.php');
require_once(dirname(__FILE__) . '/classes/TrainingParticipant.php');
require_once(dirname(__FILE__) . '/classes/Voivodeship.php');
require_once(dirname(__FILE__) . '/classes/SearchPoldent.php');

class Trainings extends Module
{
    public function __construct()
    {
        $this->name = 'trainings';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Łukasz Łuczyński, Dawid Dunikowski - Grupa VIST';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Szkolenia');
        $this->description = $this->l('Moduł do tworzenia i sprzedaży szkoleń');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {

        include(dirname(__FILE__) . '/sql/install.php');

        if (
            !$this->installTab('', 'TrainingsTab', 'Szkolenia') ||
            !$this->installTab('TrainingsTab', 'AdminTrainings', 'Szkolenia') ||
            !$this->installTab('TrainingsTab', 'AdminTrainingsCategories', 'Kategorie') ||
            !$this->installTab('TrainingsTab', 'AdminTrainingsOrders', 'Zamówienia') ||
            !$this->installTab('TrainingsTab', 'AdminBundle', 'Pakiety szkoleń') ||
            !$this->installTab('TrainingsTab', 'AdminTrainingsInstructors', 'Prowadzący szkolenie') ||
            !$this->installTab('TrainingsTab', 'AdminTrainingsParticipants', 'Uczestnicy szkoleń') ||
            !$this->installTab('TrainingsTab', 'AdminTrainingsInActive', 'Nieaktywne szkolenia')||
            !$this->installTab('TrainingsTab', 'AdminTrainingsInActiveBundle', 'Nieaktywne pakiety')
        )
            return false;

        /**
         * Funkcja instalująca przykładowe szkolenia i kategorie - po skończeniu modułu można usunąć
         */
        //$this->installDummyData();

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('moduleRoutes') &&
            $this->registerHook('displayCustomerAccount') &&
            !$this->registerHook('displayHome') &&
			$this->registerHook('displayFooterProduct') && 
			$this->registerHook('displayShop');
    }

    public function uninstall()
    {

        //include(dirname(__FILE__) . '/sql/uninstall.php');

        if (
            !$this->uninstallTab('TrainingsTab') ||
            !$this->uninstallTab('AdminTrainings') ||
            !$this->uninstallTab('AdminTrainingsCategories') ||
            !$this->uninstallTab('AdminTrainingsOrders') ||
            !$this->uninstallTab('AdminBundle') ||
            !$this->uninstallTab('AdminTrainingsInstructors') ||
            !$this->uninstallTab('AdminTrainingsInActive') ||
            !$this->uninstallTab('AdminTrainingsInActiveBundle')
        )
            return false;

        return parent::uninstall();
    }

    /**
     * Install && uninstall admin tab functions
     */

    public function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent = (int) Tab::getIdFromClassName($parent);
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang)
            $tab->name[$lang['id_lang']] = $name;
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->active = 1;
        return $tab->add();
    }

    public function uninstallTab($class_name)
    {
        $id_tab = (int) Tab::getIdFromClassName($class_name);
        $tab = new Tab((int) $id_tab);
        return $tab->delete();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool) Tools::isSubmit('submitTrainingsModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        return $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitTrainingsModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm(), $this->dotpayConfigForm(), $this->bankAccountConfigForm(), $this->serviceEmailSetForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Ustawienia'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'col' => 1,
                        'name' => 'TRAININGS_NB_PER_PAGE',
                        'label' => $this->l('Liczba szkoleń na stronie'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function dotpayConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => 'Dane dotpay',
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'name' => 'TRAININGS_DOTPAY_PIN',
                        'label' => 'Pin'
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'TRAININGS_DOTPAY_ID',
                        'label' => 'ID sklepu'
                    )
                ),
                'submit' => array(
                    'title' => 'Zapisz'
                ),
            )
        );
    }

    protected function bankAccountConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => 'Dane konta bankowego',
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'name' => 'TRAININGS_BANK',
                        'label' => 'Nazwa banku'
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'TRAININGS_BANK_ACCOUNT',
                        'label' => 'Numer konta bankowego'
                    )
                ),
                'submit' => array(
                    'title' => 'Zapisz'
                ),
            )
        );
    }
    protected function serviceEmailSetForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => 'Obsługa klienta (mail na który wysyłana będzie informacjo o anulowaniu zlecenia)',
                    'icon' => 'icon-cogs',
                ),

                'input' => array(
                    array(
                        'type' => 'select',
                        'name' => 'POLDENT_MAIL_SERVICES',
                        'label' => 'Email pracownika.',
                        'options' => array(
                            'query' => Contact::getContacts($this->context->language->id),
                            'id' => 'email',
                            'name' => 'email'
                        )
                    )
                ),

                'submit' => array(
                    'title' => 'Zapisz'
                ),
            )
        );
    }
    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'TRAININGS_NB_PER_PAGE' => Configuration::get('TRAININGS_NB_PER_PAGE', '9'),
            'TRAININGS_DOTPAY_PIN' => Configuration::get('TRAININGS_DOTPAY_PIN', ''),
            'TRAININGS_DOTPAY_ID' => Configuration::get('TRAININGS_DOTPAY_ID', ''),
            'TRAININGS_BANK' => Configuration::get('TRAININGS_BANK', ''),
            'TRAININGS_BANK_ACCOUNT' => Configuration::get('TRAININGS_BANK_ACCOUNT', ''),
            'POLDENT_MAIL_SERVICES' => Configuration::get('POLDENT_MAIL_SERVICES', ''),
        );
    }

    /**
     * Module routes
     */

    public function hookModuleRoutes()
    {
        return array(
            'module-trainings-frontpage' => array(
                'controller' => 'frontpage',
                'rule' => 'nasze-szkolenia',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'trainings',
                    'controller' => 'frontpage'
                )
            ),
            'module-trainings-category' => array(
                'controller' => 'category',
                'rule' => 'szkolenia{/:name}{-:id_category}{/:page}',
                'keywords' => array(
                    'name' => array(
                        'regexp' => '[_a-zA-Z-]*',
                        'param' => 'name'
                    ),
                    'id_category' => array(
                        'regexp' => '[\d]+',
                        'param' => 'id_category'
                    ),
                    'page' => array(
                        'regexp' => '[\d]+',
                        'param' => 'page'
                    )
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'trainings',
                    'controller' => 'category'
                )
            ),
            'module-trainings-single' => array(
                'controller' => 'single',
                'rule' => 'szkolenie/{:alias}{/:id_training}',
                'keywords' => array(
                    'id_training' => array(
                        'regexp' => '[\d]+',
                        'param' => 'id_training'
                    ),
                    'alias' => array(
                        'regexp' => '[\w-]+',
                        'param' => 'alias'
                    )
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'trainings',
                    'controller' => 'single'
                )
            ),
            'module-trainings-bundles' => array(
                'controller' => 'bundles',
                'rule' => 'pakiety-szkolen{/:page}',
                'keywords' => array(
                    'page' => array(
                        'regexp' => '[\d]+',
                        'param' => 'page'
                    )
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'trainings',
                    'controller' => 'bundles'
                )
            ),
            'module-trainings-cart' => array(
                'controller' => 'cart',
                'rule' => 'koszyk-szkolenia',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'trainings',
                    'controller' => 'cart'
                )
            ),
            'module-trainings-confirm' => array(
                'controller' => 'confirm',
                'rule' => 'potwierdz-uczestnictwo-w-szkoleniu',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'trainings',
                    'controller' => 'confirm'
                )
            ),
            'module-trainings-myTrainings' => array(
                'controller' => 'myTrainings',
                'rule' => 'moje-szkolenia',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'trainings',
                    'controller' => 'myTrainings'
                )
            ),
            'module-trainings-instructors' => array(
                'controller' => 'instructors',
                'rule' => 'wykladowcy',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'trainings',
                    'controller' => 'instructors'
                )
            ),
            'module-trainings-singleInstructor' => array(
                'controller' => 'singleInstructor',
                'rule' => 'wykladowca/{id_instructor}',
                'keywords' => array(
                    'id_instructor' => array(
                        'regexp' => '[\d]+',
                        'param' => 'id_instructor'
                    )
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'trainings',
                    'controller' => 'singleInstructor'
                )
            )
        );
    }

    public function hookDisplayCustomerAccount()
    {
        return $this->display(__FILE__, 'displayCustomerAccount.tpl');
    }

    public function hookdisplayHome() {
        
        
        $this->context->smarty->assign(array(
            'upcoming_trainings' => $this->getTrainingsWithInstructors(Training::getUpcomingTrainings()),
            'promoted_trainings' => $this->getTrainingsWithInstructors(Training::getPromotedTrainings())
          
        ));

		return $this->display(__FILE__, 'views/templates/front/promotedtrainings.tpl');
    }
	
	public function hookdisplayFooterProduct() {
        
        
        $this->context->smarty->assign(array(
            'promoted_trainings' => $this->getTrainingsWithInstructors(Training::getPromotedTrainings())
          
        ));

		return $this->display(__FILE__, 'views/templates/front/productPagePromotedTrainings.tpl');
    }
	

   	public function hookdisplayShop() {
        
        
        $this->context->smarty->assign(array(
            'promoted_trainings' => $this->getTrainingsWithInstructors(Training::getPromotedTrainings())
          
        ));

		return $this->display(__FILE__, 'views/templates/front/productPagePromotedTrainings.tpl');
    }
    public function getTree($resultParents, $resultIds, $maxDepth, $id_category = null, $currentDepth = 0)
	{
		if (is_null($id_category))
			return false;
		$children = array();
		if (isset($resultParents[$id_category]) && count($resultParents[$id_category]) && ($maxDepth == 0 || $currentDepth < $maxDepth))
			foreach ($resultParents[$id_category] as $subcat)
				$children[] = $this->getTree($resultParents, $resultIds, $maxDepth, $subcat['id_training_category'], $currentDepth + 1);
		if (isset($resultIds[$id_category]))
		{
            $params =[
                'name' => trim($resultIds[$id_category]['alias']),
                'id_category' => trim($resultIds[$id_category]['id_training_category']),
                'page' => 1
            ];
			$name = $resultIds[$id_category]['name'];
            $link = $this->context->link->getModuleLink('trainings', 'category', $params);
		}
		else
			$name = '';

		$return = array(
			'id' => $id_category,
            'name' => $name,
            'level_depth'=> $currentDepth,
            'link' => $link,
			'children' => $children
		);
		return $return;
	}
    
    public function installDummyData()
    {

        $ins1 = new TrainingInstructor();
        $ins1->name = 'Łukasz';
        $ins1->surname = 'Łuczyński';
        $ins1->description = 'Lorem ipsum';
        $ins1->save();

        $cat1 = new TrainingCategory();
        $cat1->name = 'Laser';
        $cat1->alias = 'laser';
        $cat1->save();

        $cat2 = new TrainingCategory();
        $cat2->name = 'Chirurgia';
        $cat2->alias = 'chirurgia';
        $cat2->save();

        $tr1 = new Training();
        $tr1->id_training_category = 1;
        $tr1->name = 'Zastosowanie terapii laserowej w praktyce stomatologicznej.';
        $tr1->alias = 'zastosowanie_terapii_laserowej';
        $tr1->author = 'Daniel Nowakowski';
        $tr1->id_training_instructor = 1;
        $tr1->start_time = date_create('now')->format('Y-m-d H:i:s');
        $tr1->end_time = date_create('tomorrow')->format('Y-m-d H:i:s');
        $tr1->price = '1199.99';
        $tr1->discount_price = '1099.99';
        $tr1->slots = '15';
        $tr1->short_desc = 'Krótki opis';
        $tr1->long_desc = 'Długi opis';
        $tr1->sms_notifications = 1;
        $tr1->need_doc_number = 0;
        $tr1->can_be_sold_alone = 0;
        $tr1->is_promoted = 1;
        $tr1->save();

        $tr2 = new Training();
        $tr2->id_training_category = 2;
        $tr2->name = 'Mój pierwszy raz, czyli jak rozpocząć przygodę z implantologią? Podstawowy kurs chirurgiczno-implantologiczny.';
        $tr2->alias = 'jak_rozpoczac_przygode_z_implantologia';
        $tr2->author = 'Jakub Łada';
        $tr2->id_training_instructor = 1;
        $tr2->start_time = date_create('2020-09-02')->format('Y-m-d H:i:s');
        $tr2->end_time = date_create('2020-09-10')->format('Y-m-d H:i:s');
        $tr2->price = '1599.99';
        $tr2->discount_price = '1499.99';
        $tr2->slots = '10';
        $tr2->short_desc = 'Krótki opis';
        $tr2->long_desc = 'Długi opis';
        $tr2->sms_notifications = 1;
        $tr2->need_doc_number = 0;
        $tr2->can_be_sold_alone = 0;
        $tr2->is_promoted = 0;
        $tr2->save();
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path . 'views/js/back.js');
        $this->context->controller->addCSS($this->_path . 'views/css/back.css');
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    private function getTrainingsWithInstructors($trainings = []){

        $results = [];
        $nested_1 =[];

        foreach($trainings as $key => $value){
         
            foreach($value as $k1 => $v1){

                $nested_1[$k1] = $v1;

                if($k1 == 'id_training_instructor' ){
                   
                    $nested_1['instructors'] = TrainingInstructor::getInstructorById($v1); 
                } 
            }

            array_push($results,$nested_1);
        }
        return $results;
    }
}
