<?php

class TrainingsFrontPageModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {
        $this->display_column_left = false;

        parent::initContent();
        $cat_id = 0;
        $voivodeship = 'All';

        if(Tools::getValue('category-upcoming_trainings')){
            if(Tools::getValue('select-category-training')){
                $cat_id = (int) Tools::getValue('select-category-training');
            }

            if(Tools::getValue('select-voivodeship-training')){
                $voivodeship = Tools::getValue('select-voivodeship-training');  
            }
        }

      //Get Tree category
      
        if($results = TrainingCategory::getTrainingCategories()){
            $resultIds = array();
            $resultParents = array();
            $maxdepth = 0;
            
            $CategoryTree = new Trainings();
            foreach ($results as $row)
            {
                $resultParents[$row['id_parent']][] = $row;
                $resultIds[$row['id_training_category']] = $row;

                if($maxdepth < $row['level_depth']){
                    $maxdepth = $row['level_depth'];
                }
            }
            $blockCategTree = [];
              foreach ($results as $row)
              {
                if($row['id_parent'] == 0){
                    array_push($blockCategTree,$CategoryTree->getTree($resultParents, $resultIds, $maxdepth, $row['id_training_category']));
                }
              }
        }
       

        $this->context->smarty->assign(array(
            'categories' => $blockCategTree,
            'branche_tpl_path'=> Context::getContext()->shop->getBaseURL(true) . 'modules/trainings/views/templates/front/category-tree-branch.tpl',
            'upcoming_trainings' => $this->getTrainingsWithInstructors(Training::getUpcomingTrainings($cat_id,$voivodeship)),
            'selected_upcoming_trainings' => $cat_id,
            'voivodeship' => Training::getVoivodeship(),
            'select_voivodeship' => $voivodeship,
            'promoted_trainings' => $this->getTrainingsWithInstructors(Training::getPromotedTrainings()),
            'bundle' => Bundle::getBundle(),
            'item' =>  $this->getBundleData(),
        ));

        $this->setTemplate('frontpage.tpl');
    }

    private function getActiveTrainingsWidthInstructors($id_bundle)
    {
        $result = Db::getInstance()->executeS('
            SELECT t.*,ti.`id_training_instructor` ,ti.`name` as instructor_name, ti.`surname` as instructor_surname  FROM  `' . _DB_PREFIX_ . 'training` t
            LEFT JOIN `' . _DB_PREFIX_ . 'training_instructor` ti
            ON t.`id_training_instructor` = ti.`id_training_instructor`
            WHERE t.`id_training` IN (select bt.`id_training` FROM `' . _DB_PREFIX_ . 'bundle_trainings` bt WHERE bt.`id_bundle` ='.$id_bundle.' ) AND t.`active` = 1
        ');
        return $result;
    }

    private function getBundleData(){

        $bundle = Bundle::getBundle();
        $results = [];
        $nested_1 =[];

        foreach($bundle as $key => $value){
         
            foreach($value as $k1 => $v1){

                $nested_1[$k1] = $v1;

                if($k1 == 'id_bundle' ){
                   
                    $nested_1['trainings'] = $this->getActiveTrainingsWidthInstructors($v1); 
                } 
                if($k1 == 'discount' ){
                    
                    $price = Bundle::getBundleSumTrainings($value['id_bundle']);
                    $priceD = $price - $v1;

                    if($priceD < 0){
                        $priceD = 0;
                    }

                    $nested_1['price'] = $price;
                    $nested_1['priceD'] = (string) $priceD;
                } 
               
            }

            array_push($results,$nested_1);
        }
        return $results;
    }

    private function getTrainingsWithInstructors($trainings = []){

        $results = [];
        $nested_1 =[];

        foreach($trainings as $key => $value){
         
            foreach($value as $k1 => $v1){

                $nested_1[$k1] = $v1;

                if($k1 == 'id_training_instructor' ){
                   
                    $nested_1['instructors'] = TrainingInstructor::getInstructorById($v1); 
                } 
            }

            array_push($results,$nested_1);
        }
        return $results;
    }
}
