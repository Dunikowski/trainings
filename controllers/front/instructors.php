<?php

class TrainingsInstructorsModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {
        $this->display_column_left = false;

        parent::initContent();

        $instructors = TrainingInstructor::getInstructors();

        $this->context->smarty->assign(array(
            'instructors' => $instructors
        ));

        $this->setTemplate('instructors.tpl');
    }
}
