<?php

class TrainingsSingleInstructorModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {
        $this->display_column_left = false;

        parent::initContent();

        $id_instructor = (int) Tools::getValue('id_instructor');

        $instructor = TrainingInstructor::getInstructorById($id_instructor);

        $this->context->smarty->assign(array(
            'instructor' => $instructor,
            'trainings' => Training::getInstructorTrainings($instructor['id_training_instructor'])
        ));

        $this->setTemplate('singleInstructor.tpl');
    }
}
