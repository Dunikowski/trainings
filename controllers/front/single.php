<?php

class TrainingsSingleModuleFrontController extends ModuleFrontController
{

    // public function init()
    // {
    //     $this->page_name = 'test_page_name';
    //     return parent::init();
    // }

    public function initContent()
    {
        if (Tools::getValue('id_training') != '')
            $id = (int) Tools::getValue('id_training');

        $training = Training::getSingleTraining($id);
        $free_places = $training['slots'] - count(Training::getTrainingParticipants($id));
        $instructor = TrainingInstructor::getInstructorById($training['id_training_instructor']);
        $bundles = Training::getBundlesTraining($id);
        $this->context->smarty->tpl_vars['meta_title']->value = $training['name'];
        $this->display_column_left = false;
        $tc = new TrainingCategory($training['id_training_category']);
        $breadcrumbs = [];
        $breadcrumbs['category_name'] = $tc->name;
		$breadcrumbs['id_category'] = $tc->id_training_category;
        $breadcrumbs['url'] = Context::getContext()->link->getModuleLink('szkolenie','category-'.$tc->id_training_category.'/'.$tc->id_training_category, array());

        parent::initContent();
        if (Tools::getIsset('savedate')) {
            $listToSubmit = array();
            $errors = array();

            $firstname = pSQL($_POST['firstname']);
            $lastname = pSQL($_POST['lastname']);
            $email = pSQL($_POST['email']);
            $telephone = pSQL($_POST['telephone']);
            $error = array();


            if (empty($error)) {
                $participant = array();
                $participant['firstname'] = $firstname;
                $participant['lastname'] = $lastname;
                $participant['email'] = $email;
                $participant['telephone'] = $telephone;
                $participant['training_id'] = $id;

                array_push($listToSubmit, $participant);
            } else {
                array_push($errors, $error);
            }


            if (empty($errors)) {
                // Zapis na listę rezerwową

                foreach ($listToSubmit as $participant) {
                    Training::signReserveList(
                        $participant['training_id'],
                        $participant['firstname'],
                        $participant['lastname'],
                        $participant['email'],
                        $participant['telephone']

                    );
                }
            }
        }

        $this->context->smarty->assign(array(
            'training' => $training,
            'free_places' => $free_places,
            'instructor' => $instructor,
            'ajaxurl' =>Context::getContext()->shop->getBaseURL(true) . 'modules/trainings/ajax.php',
            'bundles_title' => $bundles,
            'breadcrumbs' =>$breadcrumbs
        ));

        $this->setTemplate('single.tpl');
    }



    public function setMedia()
    {
        parent::setMedia();
        $this->path = __PS_BASE_URI__ . 'modules/trainings';

        $this->context->controller->addJS($this->path . '/views/js/single.js');
    }
}
