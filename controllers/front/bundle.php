<?php

class TrainingsBundleModuleFrontController extends ModuleFrontController
{

    // public function init()
    // {
    //     $this->page_name = 'test_page_name';
    //     return parent::init();
    // }

    public function initContent()
    {
        if (Tools::getValue('id_bundle') != '')
            $id = (int) Tools::getValue('id_bundle');
       
        $bundle = Bundle::getBundleById($id);
        $price = Bundle::getBundleSumTrainings($id);
        $priceD = $price - $bundle['discount'];

        if( $priceD < 0){
            $priceD = 0;
        }

        $this->context->smarty->tpl_vars['meta_title']->value = $bundle['title'];
        $this->display_column_left = false;

        parent::initContent();

        $this->context->smarty->assign(array(
            'bundle' => $bundle,
            'ajaxurl' => Context::getContext()->shop->getBaseURL(true) . 'modules/trainings/ajax.php',
            'trainings' => Bundle::getAllTrainingsBundle($id),
            'trainings_bundle' => $this->getTrainingsWithInstructors(Bundle::getBundleTrainings($id)),
            'price' => $price,
            'priceD' => (string )$priceD
        ));


        $this->setTemplate('bundle.tpl');
    }

    private function getTrainingsWithInstructors($trainings = []){

        $results = [];
        $nested_1 =[];

        foreach($trainings as $key => $value){
         
            foreach($value as $k1 => $v1){

                $nested_1[$k1] = $v1;

                if($k1 == 'id_training_instructor' ){
                   
                    $nested_1['instructors'] = TrainingInstructor::getInstructorById($v1); 
                } 
            }

            array_push($results,$nested_1);
        }
        return $results;
    }


    public function setMedia()
    {
        parent::setMedia();
        $this->path = Context::getContext()->shop->getBaseURL(true) . 'modules/trainings';

        $this->context->controller->addJS($this->path . '/views/js/bundle.js');
    }
}
