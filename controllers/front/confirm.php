<?php

class TrainingsConfirmModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $this->display_column_left = false;

        parent::initContent();

        $token = Tools::getValue('token');

        if (Tools::getIsset('submit-rodo')) {
            if (Tools::getIsset('rodo_ok')) {
                Db::getInstance()->update('training_participant', array(
                    'rodo_ok' => 1
                ), '`token` = "' . $token . '"');
                $this->context->smarty->assign(array(
                    'rodo_ok' => true
                ));
            }
        }

        $this->setTemplate('confirm.tpl');
    }
}
