<?php

class TrainingsBundlesModuleFrontController extends ModuleFrontController
{

   


    public function initContent()
    {
        
        $this->display_column_left = false;

        parent::initContent();

        $nb_bundles = Bundle::countBundles();
        $nb_per_page = (int) Configuration::get('TRAININGS_NB_PER_PAGE');
        $nb_pages = ceil($nb_bundles / $nb_per_page);
        $page = 1;

        if (Tools::getValue('page') != '')
            $page = (int) $_GET['page'];

        $start = ($page - 1) * $nb_per_page;
        $end = $nb_per_page;
        $this->context->smarty->assign(array(
            'bundles' => $this->getBundleData($page,$nb_per_page),
            'nb_pages' => $nb_pages,
            'page' => $page
        ));


        $this->setTemplate('bundles.tpl');
    }

    private function getBundleData($page,$nb_per_page){

        $bundle = Bundle::getBundleForPagination($page,$nb_per_page);
        $results = [];
        $nested_1 =[];

        foreach($bundle as $key => $value){
         
            foreach($value as $k1 => $v1){

                $nested_1[$k1] = $v1;

                if($k1 == 'id_bundle' ){
                   
                    $nested_1['trainings'] = $this->getActiveTrainingsWidthInstructors($v1); 
                } 
                
                if($k1 == 'discount' ){
                    
                    $price = Bundle::getBundleSumTrainings($value['id_bundle']);
                    $priceD = $price - $v1;

                    if($priceD < 0){
                        $priceD = 0;
                    }

                    $nested_1['price'] = $price;
                    $nested_1['priceD'] = (string) $priceD;
                } 
               
            }

            array_push($results,$nested_1);
        }
        return $results;
    }

    private function getActiveTrainingsWidthInstructors($id_bundle)
    {
        $result = Db::getInstance()->executeS('
            SELECT t.*,ti.`id_training_instructor` ,ti.`name` as instructor_name, ti.`surname` as instructor_surname  FROM  `' . _DB_PREFIX_ . 'training` t
            LEFT JOIN `' . _DB_PREFIX_ . 'training_instructor` ti
            ON t.`id_training_instructor` = ti.`id_training_instructor`
            WHERE t.`id_training` IN (select bt.`id_training` FROM `' . _DB_PREFIX_ . 'bundle_trainings` bt WHERE bt.`id_bundle` ='.$id_bundle.' ) AND t.`active` = 1
        ');
        return $result;
    }
}
