﻿<?php

class TrainingsMyTrainingsModuleFrontController extends ModuleFrontController
{
    
    public function initContent()
    {
       
        $remove_error = [];
        if(Tools::getIsset('remove-order')){

            $index = (isset($_POST['index']))?$_POST['index']:'';
            $id_cart_detail = (isset($_POST['id_cart_detail']))?$_POST['id_cart_detail']:false;
            $id_cart = (isset($_POST['id_cart']))?$_POST['id_cart']:false;
            $id_bundle = (isset($_POST['id_bundle']))?$_POST['id_bundle']:false;
            $id_training_participant = (isset($_POST['id_training_participant']))?$_POST['id_training_participant']:false;
            $mail_data['index'] = $index;
            $mail_data['orders'] = TrainingCart::getCartProducts($id_cart);
            $mail_data['total_price'] = TrainingCart::getCartValue($id_cart);
            
            $service_url = Configuration::get('POLDENT_MAIL_SERVICES');
          
            if(!$service_url){
               
                array_push($remove_error, 'Z przyczyn technicznych obsługa sklepu nie została poinformowana o anulowaniu szkolenia. Prosimy o kontakt telefoniczny.');
            }
           
            $this->sendMail($mail_data, $service_url);

        
            if($id_training_participant && $id_cart_detail && $service_url){
                 TrainingCart::cancelTrainingBundel($id_cart_detail);
                 TrainingParticipant::deleteParticipant($id_training_participant);  
            }

             if($id_cart_detail && $id_cart && $id_bundle && $service_url){
                TrainingCart::cancelTrainingBundel($id_cart_detail);
                TrainingParticipant::delateParticipantBundleTrainings($id_bundle, $id_cart);
               
            }

             if(TrainingCart::countItemsByIdCart($id_cart) == 0){
                 TrainingCart::deleteCart($id_cart);
                 TrainingOrder::deleteCart($id_cart);
             }         
        }

        if (Tools::getValue('id_training') != '' && Tools::getValue('id_training') != null)
            $id = (int) Tools::getValue('id_training');

        $this->display_column_left = false;

        $directory = $_SERVER['DOCUMENT_ROOT'] . '/modules/trainings/files';

        parent::initContent();
        $trainings = Training::getDistinctCustomerTrainings($this->context->customer->id);



        foreach ($trainings as $training) {
            $id = pSQL($training['id_training']);
            $ids[] = $id;
            $file[] = Training::getTrainingAttachments($id);
            $id_order = $training['id_training_order'];
        }
       
         $participiants = TrainingOrder::getOrderTrainings($id_order);
         $bundles = TrainingOrder::getOrderBundles($this->context->customer->id);

        $this->context->smarty->assign(array(
            'trainings' => Training::getDistinctCustomerTrainings($this->context->customer->id),
            'certyficate' =>  TrainingOrder::getParticipantCertyficate($id_order),
            'directory' => $directory,
            'results'=>  $this->createDataForMyTrainingsTpl(),
            'remove_error' => $remove_error
        ));


        $this->setTemplate('myTrainings.tpl');
    }

   

    private function createDataForMyTrainingsTpl(){

        $orders = TrainingOrder::getOrders($this->context->customer->id);    
        $results = [];
        $nested_1 = [];
        
        foreach($orders as $val) {
        
            foreach($val as $k1 => $v1){
                if($k1 == 'id_cart' ){
                    $nested_1[$k1] = $v1;
                    $cart_trainings = TrainingCart::getCartDetailsTrainings($v1);
                    $cart_bundle = TrainingCart::getCartDetailsBundle($v1);

                    if(!empty($cart_trainings) && !empty($cart_bundle)){

                        for($i = 0;$i < count($cart_bundle);$i++){
                            $trainings_in_bundle = TrainingCart::getTraningDataForMyTranings($v1,$cart_bundle[$i]['id_bundle']);
                            foreach($trainings_in_bundle as &$v2){
                                $files['files'] = Training::getTrainingAttachments($v2['id_training']);
                                $v2['files'] =  $files['files'];
                            }
                            $cart_bundle[$i]['bundle_trainings'] = $trainings_in_bundle;
                        }

                        for($i = 0;$i < count($cart_trainings);$i++){
                            $cart_trainings[$i]['files'] = Training::getTrainingAttachments($cart_trainings[$i]['id_training']);
                        }
                        
                        $nested_1['cart_items'] = ['trainings'=>$cart_trainings,'bundles'=>$cart_bundle];

                    }else if(count($cart_trainings) > 0){

                        for($i = 0;$i < count($cart_trainings);$i++){
                            $cart_trainings[$i]['files'] = Training::getTrainingAttachments($cart_trainings[$i]['id_training']);
                        }

                        $nested_1['cart_items'] = ['trainings'=>$cart_trainings];

                    } else if(count($cart_bundle) > 0){

                        for($i = 0;$i < count($cart_bundle);$i++){
                            $cart_bundle[$i]['bundle_trainings'] = TrainingCart::getTraningDataForMyTranings($v1,$cart_bundle[$i]['id_bundle']);
                        }

                        $nested_1['cart_items'] = ['bundles'=>$cart_bundle];

                    } else {
                        
                        $nested_1['cart_items'] = [];
                    }
                    
                }else{
                    $nested_1[$k1] = $v1;
                }
               
            }
            $nested_1['total_price_cart'] = TrainingCart::getCartValue($val['id_cart']);
            $nested_1['invoice'] = $this->displayInvoice($val['id_training_order']);
            array_push($results,$nested_1);
        
        }
        return $results;
    }

    private function sendMail($array = [], $service_url){
        $mailsDir = _PS_ROOT_DIR_ . '/modules/trainings/views/templates/mails/';
        $topic = 'Anulowanie zamówienia';
        $date = date("Y-m-d h:i");
        $details = '';
        foreach($array['orders'] as $item) {
            if($item['id_training']){
                $details .= 'Szkolenie: '.$item['training_name']."\r\n";
            }else if($item['id_bundle']){
                $details .= 'Pakiet: '.$item['training_name']."\r\n";
            }
        }
        
        
         if($service_url){
             //Customer

             if (!$id_shop) {
                $id_shop = Context::getContext()->shop->id;
            }
            if (file_exists(_PS_IMG_DIR_.Configuration::get('PS_LOGO', null, null, $id_shop))) {
                $logo = _PS_IMG_DIR_.Configuration::get('PS_LOGO', null, null, $id_shop);
            } else {
                $logo = '';
            }

          Mail::send(
            1,
             'remove_order',
             $topic,
             array(
                '{shop_logo}' => $logo,
                '{shop_name}' => Tools::safeOutput(Configuration::get('PS_SHOP_NAME', null, null, $id_shop)),
                '{shop_url}' => Context::getContext()->link->getPageLink('index', true, Context::getContext()->language->id, null, false, $id_shop),
                 '{date}' => $date,
                 '{index}' => $array['index'],
                 '{details}' => $details
              ),
             $this->context->customer->email,
             null,
             null,
             null,
            null,
             null,
             $mailsDir
        );
        //Service worker
            Mail::send(
                1,
                 'remove_order',
                $topic,
                array(
                    '{shop_logo}' => $logo,
                    '{shop_name}' => Tools::safeOutput(Configuration::get('PS_SHOP_NAME', null, null, $id_shop)),
                    '{shop_url}' => Context::getContext()->link->getPageLink('index', true, Context::getContext()->language->id, null, false, $id_shop),
                     '{date}' => $date,
                     '{index}' => $array['index'],
                     '{details}' => $details
                  ),
                  $service_url,
                 null,
                 null,
                 null,
                 null,
                 null,
                 $mailsDir
             );
         }
           
        return $details;
        
    }
    private function displayInvoice($id_order){
        if(!$id_order) return false;
        $path = _PS_MODULE_DIR_ . 'trainings/invoices/' . $id_order;
        $files_to_diplay  = [];
        if(file_exists($path)){
            $files_to_diplay = glob($path."/*.*",GLOB_NOSORT);
            
        }
        return $files_to_diplay;
    }
}
