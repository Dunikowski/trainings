<?php

class TrainingsAfterPaymentModuleFrontController extends ModuleFrontController
{
	public function initContent()
	{
		parent::initContent();

		$this->context->smarty->assign(array(
			'status' => $_GET['status']
		));

		$this->setTemplate('afterPayment.tpl');
	}
}
