<?php

class TrainingsCategoryModuleFrontController extends ModuleFrontController
{

    public function init()
    {
        $this->page_name = 'Kategorie';
        parent::init();
    }

    public function initContent()
    {
        $this->display_column_left = false;

        parent::initContent();

        $category = new TrainingCategory(Tools::getValue('id_category'));

        $nb_trainings = Training::getNbTrainingsCategory($category->id_training_category);
        $nb_per_page = (int) Configuration::get('TRAININGS_NB_PER_PAGE');
        $nb_pages = ceil($nb_trainings / $nb_per_page);
        $page = 1;

        if (Tools::getValue('page') != '')
            $page = (int) $_GET['page'];

        $start = ($page - 1) * $nb_per_page;
        $end = $nb_per_page;
        $voivodeship = '';
        
        if(Tools::getValue('voivodeship-for-category')){
         
            if(Tools::getValue('voivodeship-for-category-select')){
                $voivodeship = Tools::getValue('voivodeship-for-category-select');
                if($voivodeship == 'All') {
                    $voivodeship = '';
                }
            }
            
        }
        $this->context->smarty->assign(array(
            'trainings' => Training::getTrainingsFromCategory($start, $end, $category->id_training_category,$voivodeship),
            'nb_pages' => $nb_pages,
            'page' => $page,
            'category' => $category,
            'voivodeship' => Voivodeship::getOptions(),
            'select_voivodeshop' => $voivodeshop,
        ));

        $this->setTemplate('category.tpl');
    }
}
