<?php

class TrainingsCartModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {
        $this->display_column_left = false;

        parent::initContent();

        // Tools::dieObject($this->context->customer);
       
        $training_place = [];

        $active_cart = TrainingCart::getActiveCart($this->context->customer->id);
        $isTraining = TrainingCart::countItemsByIdCart($active_cart);
        if($isTraining > 0){
        if (Tools::getIsset('submit-list')) {
            $boundle_array;
            $listToSubmit = array();
            $errors = array();
            
            ksort($_POST);
           
            foreach ($_POST as $key => $val) {
                
                $pos = strpos($key, "training_");
                $b_pos = strpos($key, "bundle_");
              
                    if($pos === 0) {
                        $id = explode('_', $key)[1];
                       
                        for($i = 0; $i < count($val);$i++){

                            if(!empty($training_place)){
                                       
                                    if(array_key_exists((string)$id,$training_place)){
                                        $training_place[(string)$id] += 1;
                                    } else {
                                        $training_place[(string)$id] = 1; 
                                    }
                            
                            } else{
                                $training_place[$id] = 1; 
                            }
                        }
                        foreach ($val as $item) {
                            $firstname = pSQL($item['firstname']);
                            $lastname = pSQL($item['lastname']);
                            $email = pSQL($item['email']);
                            $phone = pSQL($item['phone']);
                            $error = array();

                            if (!Validate::isName($firstname)) {
                                $error['firstname'] = "Imię: $firstname jest niepoprawne";
                            }
                            if (!Validate::isName($lastname)) {
                                $error['lastname'] = "Nazwisko: $lastname jest niepoprawne";
                            }
                        
                            if (!Validate::isEmail($email)) {
                                $error['email'] = "Adres email: $email jest niepoprawny";
                            }
                            if(!Validate::isPhoneNumber($phone)){
                                $error['phone'] = "Nr. telefonu: $phone jest niepoprawny";
                            }
                            if (count(Training::isParticipantSigned($email, $id)) > 0) {
                            
                                $error['email'] = "Użytkownik o podanym adres email: $email, jest już uczestnikiem szkolenia.";
                            }

                            if(isset($item['send_info_mail']) && !Validate::isEmail($email)){
                                $error['send_info_mail'] = "Aby wysłać informacje o udziale w szkoleniu, należy dla uczestnika:  $firstname  $lastname podać poprawny adres email";
                            }

                            if (empty($error)) {
                                $participant = array();
                                $participant['firstname'] = $firstname;
                                $participant['lastname'] = $lastname;
                                $participant['email'] = $email;
                                $participant['phone'] = $phone;
                                $participant['training_id'] = $id;
                                $participant['id_bundle'] = null;
                                $participant['active_cart'] = TrainingCart::getActiveCart($this->context->customer->id);
                                $participant['send_email'] = isset($item['send_info_mail'])?true:false;
                                array_push($listToSubmit, $participant);
                               
                            } else {
                                array_push($errors, $error);
                            }
                        
                     }
                      
                    } else if($b_pos === 0){
                        $id = explode('_', $key)[1];
                       
                        $boundle_slots = Bundle::checkBundleTrainingsAvilability($id);
   
                          
                          for($i = 0; $i < count($val);$i++){
                            if(!empty($training_place)){
                             
                                       
                                         foreach($boundle_slots as $value){

                                            if(array_key_exists($value['id_training'],$training_place)){
                                                $training_place[$value['id_training']] += 1;
                                            } else {
                                                $training_place[$value['id_training']] = 1; 
                                            }
                                         }
                                        
                                 } else{
                                    foreach($boundle_slots as $value){
                                        $training_place[(string)$value['id_training']] = 1; 
                                     } 
                                   
                                 }  
                          }
                          $bundle_trainings = Bundle::getBundleTrainings($id);

                          foreach($bundle_trainings as $bt){
                            foreach ($val as $item) {
                                $firstname = pSQL($item['firstname']);
                                $lastname = pSQL($item['lastname']);
                                $email = pSQL($item['email']);
                                $phone = pSQL($item['phone']);
                                $error = array();
    
                                if (!Validate::isName($firstname)) {
                                    $error['firstname'] = "Imię: $firstname jest niepoprawne";
                                }
                                if (!Validate::isName($lastname)) {
                                    $error['lastname'] = "Nazwisko: $lastname jest niepoprawne";
                                }
                            
                                if (!Validate::isEmail($email)) {
                                    $error['email'] = "Adres email: $email jest niepoprawny";
                                }
                                if(!Validate::isPhoneNumber($phone)){
                                    $error['phone'] = "Nr. telefonu: $phone jest niepoprawny";
                                }
                                if (count(Training::isParticipantSigned($email, $bt['id_training'])) > 0) {
                                
                                    $error['email'] = "Użytkownik o podanym adres email: $email, jest już uczestnikiem szkolenia: ".$bt['name'];
                                }
    
                                if(isset($item['send_info_mail']) && !Validate::isEmail($email)){
                                    $error['send_info_mail'] = "Aby wysłać informacje o udziale w szkoleniu, należy dla uczestnika:  $firstname  $lastname podać poprawny adres email";
                                }
    
                                if (empty($error)) {
                                    $participant = array();
                                    $participant['firstname'] = $firstname;
                                    $participant['lastname'] = $lastname;
                                    $participant['email'] = $email;
                                    $participant['phone'] = $phone;
                                    $participant['training_id'] = $bt['id_training'];
                                    $participant['id_bundle'] = $id;
                                    $participant['active_cart'] = TrainingCart::getActiveCart($this->context->customer->id);
                                    $participant['send_email'] = isset($item['send_info_mail'])?true:false;
                                    array_push($listToSubmit, $participant);
                                   
                                } else {
                                    array_push($errors, $error);
                                }
                            }
                          }

                    }        
            }

           
            //Check free training places
            if(!empty($training_place)){
                foreach($training_place as $key_id => $digits){
                    $training = new Training((int)$key_id);
                    $free_places = $training->slots - count(Training::getTrainingParticipants((int)$key_id));
                    if ($digits > $free_places) {
                        $error = array('slots' => 'Dla szkolenia: '.$training->name.' Liczba wolnych miejsc jest '.$free_places.' Zmniejsz liczbę ucestników.');
                        array_push($errors, $error);
                    }
                }
            }
           
            if (empty($errors)) {
                // zapisywanie na szkolenia

                /**
                 * Przenieść w odpowiednie miejsce
                 * aby liczba miejsc zmienjszała się dopiero
                 * po zaakceptowaniu płatności
                 */

                foreach ($listToSubmit as $participant) {
                    Training::signParticipant(
                        $participant['firstname'],
                        $participant['lastname'],
                        $participant['email'],
                        $participant['phone'],
                        $participant['training_id'],
                        $participant['id_bundle'],
                        $participant['active_cart'],
                        false,
                        $participant['send_email']
                    );
                }

                $ac = $active_cart;//TrainingCart::getActiveCart($this->context->customer->id);
                $cart = new TrainingCart($ac);
                $mp = $this->checkPaymentForm($ac);

                if($mp != ''){
                    if($cart->dotpay_payment != $mp){
                        $cart->dotpay_payment = $mp;
                    }
                }
               
                /**
                 * Tworzenie zamówienia prestashop
                 */

                $prestaCart = new Cart();
                $prestaCart->id_currency = 1;
                $prestaCart->id_lang = 1;
                $prestaCart->save();

                $prestaOrder = new Order();
                $prestaOrder->id_address_delivery = 0;
                $prestaOrder->id_address_invoice = 0;
                $prestaOrder->id_cart = $prestaCart->id;
                $prestaOrder->id_currency = 1;
                $prestaOrder->id_lang = 1;
                $prestaOrder->id_customer = $this->context->customer->id;
                $prestaOrder->id_carrier = 0;
                $prestaOrder->payment = $cart->dotpay_payment ? 'Dotpay' : 'Przelew na konto';
                $prestaOrder->module = 'test';
                $prestaOrder->total_paid = TrainingCart::getCartValue($cart->id_cart);
                $prestaOrder->total_paid_real = TrainingCart::getCartValue($cart->id_cart);
                $prestaOrder->total_products = TrainingCart::getCartValue($cart->id_cart);
                $prestaOrder->total_products_wt = TrainingCart::getCartValue($cart->id_cart);
                $prestaOrder->total_paid_tax_incl = TrainingCart::getCartValue($cart->id_cart);
                $prestaOrder->conversion_rate = 1;
                $prestaOrder->reference = Order::generateReference();
                $prestaOrder->save();

                $order = new TrainingOrder();
                $order->id_cart = $cart->id_cart;
                $order->id_customer = $this->context->customer->id;
                $order->status = 0;
                $order->index = $prestaOrder->reference;
                $order->save();


                if ($cart->dotpay_payment == 1) {
                    /**
                     * DOTPAY DATA
                     */
                    $pin = Configuration::get('TRAININGS_DOTPAY_PIN');
                    $api_version = 'dev';
                    $shop_id = Configuration::get('TRAININGS_DOTPAY_ID');
                    $amount = TrainingCart::getCartValue($order->id_cart);
                    $currency = 'PLN';
                    $desc = 'test';
                    $control = $order->id_cart;
                    $url = _PS_BASE_URL_ . '/index.php?fc=module&module=trainings&controller=afterPayment';
                    $type = 0;
                    $sign =
                        $pin .
                        $api_version .
                        $shop_id .
                        $amount .
                        $currency .
                        $desc .
                        $control .
                        $url .
                        $type;
                    $signature = hash('sha256', $sign);

                    $this->context->smarty->assign(array(
                        'dotpaySummary' => true,
                        'api_version' => $api_version,
                        'shop_id' => $shop_id,
                        'amount' => $amount,
                        'desc' => $desc,
                        'control' => $control,
                        'url' => $url,
                        'type' => $type,
                        'chk' => $signature
                    ));
                   
                } else {
                    TrainingCart::makeBought($cart->id);
                    $this->context->smarty->assign(array(
                        'bankSummary' => true,
                        'bankName' => Configuration::get('TRAININGS_BANK'),
                        'bankAccount' => Configuration::get('TRAININGS_BANK_ACCOUNT')
                    ));
                   
                }

            } else {
                $this->context->smarty->assign(array(
                    'registrationErrors' => $errors
                ));
            }
        }

        if($active_cart ){

            // Checking if can exchange trainings for bundle
            while($this->isCanExchangeTraniningForBundle($active_cart,false) != ''){
                // exchange trainings for bundle
                $this->ExchangeTraniningForBundle($this->isCanExchangeTraniningForBundle($active_cart),$active_cart);
            }
   
            // Checking if can give propose aditional trainings exchaneg for bundle. If  different between booked trainings and bundles is no more than 1
           $propose_bundle = $this->isCanExchangeTraniningForBundle($active_cart,true);

           if($propose_bundle != '' && is_numeric($propose_bundle)){
                $propose_add_training = Training::getSingleTraining($propose_bundle);
            }

        }
        
        
        
        $buyer = array(
            'firstname' => $this->context->customer->firstname,
            'lastname' => $this->context->customer->lastname,
            'email' => $this->context->customer->email,
            'id' => $this->context->customer->id,
            'number' => TrainingCart::getCustomerPhoneNumber($this->context->customer->id)
        );

        if ($active_cart) {
            $this->context->smarty->assign(array(
                'cartSum' => TrainingCart::getCartValue($active_cart),
                'id_cart'=>$active_cart
            ));

            $this->context->smarty->assign(array(
                'trainings' => TrainingCart::getCartProducts($active_cart),
                'ajaxurl' => Context::getContext()->shop->getBaseURL(true) . 'modules/trainings/ajax.php',
                'buyer' => $buyer,
                'boundle'=>  TrainingCart::getActiveCart($this->context->customer->id),
                'propose_add_training' => $propose_add_training,
                
            ));
    
        }

        
            $this->setTemplate('cart.tpl');
        } else {
            $this->context->smarty->assign(array(
                'trainings' => array(),     
            ));
            $this->setTemplate('cart.tpl');
        }
    }

    private function isCanExchangeTraniningForBundle($active_cart = null,$propose = false){

        if($id_cart == null){
            $id_cart = TrainingCart::getActiveCart($this->context->customer->id);
        }
       
        $trainings = TrainingCart::getCartProducts($active_cart);
        $training_bundle = Bundle::getAllActiveBundle();
        $colection_trainings_bundle = []; 

        foreach($training_bundle as $value){
            $colection_trainings_bundle[$value['id_bundle']][]=$value['id_training'];
    
        }

        uasort($colection_trainings_bundle, function($i,$j){

            $i = count($i);
            $j = count($j);

            if ($i==$j) return 0;
            return ($i>$j)?-1:1;
        });

        $tempry_a = [];

        foreach($trainings  as $item_train){
            if($item_train['id_training'] != null){
                array_push($tempry_a, $item_train['id_training']);
            }
        }

        $match_bundle = '';

        if(!$propose){
           
            foreach($colection_trainings_bundle as $key => $val){
                sort($val);
                sort($tempry_a);

                if(count($val) == count(array_intersect($val,$tempry_a))){
                    $match_bundle = $key;
                    break;
                }
            }

            return $match_bundle;

        } else {

            foreach($colection_trainings_bundle as $key => $val){
                sort($val);
                sort($tempry_a);
                $together_trainings = array_intersect($val,$tempry_a);
                
                if(count($val) == (count($together_trainings) + 1)){
                    $match_bundle .= array_diff($val,$together_trainings)[0];
                    break;
                } 
            }

            return $match_bundle;
        }
         
    }

    private function ExchangeTraniningForBundle($id, $id_cart=null){

        if($id_cart == null){
            $id_cart = TrainingCart::getActiveCart($this->context->customer->id);
        }

        TrainingCart::addBundleProducts($id, $quantity = 1, $id_cart);
        $trainings = [];
         foreach(Bundle::getAllTrainingsBundle($id) as $item){
            array_push($trainings,$item['id_training']);
            TrainingCart::deleteProduct($item['id_training'],$id_cart);
         }
    }

    private function checkPaymentForm($active_cart = null){
      
        if($id_cart == null){
            $id_cart = TrainingCart::getActiveCart($this->context->customer->id);
            
        }

      

        $bundle = TrainingCart::getBundleCartProducts($id_cart);
        $dotpay_payment = '';
        $typ_payment = [];
        
        if(!empty($bundle)){
            
            foreach($bundle as $value){
                foreach(Bundle::getBundleTrainings($value['id_bundle']) as $v){
                    array_push($typ_payment, $v['dotpay_payment']);
                }
                
            }

            if(in_array('0',$typ_payment) && in_array('1',$typ_payment)) {
                $dotpay_payment = '0';
            } else if(in_array('0',$typ_payment)){
                $dotpay_payment = '0';
            } else{
                $dotpay_payment = '1';
            }

        }
       

       return  $dotpay_payment;
    }
    private function validateForm(){
       
        
    }
    public function setMedia()
    {
        parent::setMedia();
        $this->path = Context::getContext()->shop->getBaseURL(true) . 'modules/trainings';

        $this->context->controller->addJS($this->path . '/views/js/cart.js');
    }
}
