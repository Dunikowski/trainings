<?php

class AdminTrainingsOrdersController extends ModuleAdminController
{
   
    public function __construct()
    {
        $this->table = 'training_order';
        $this->className = 'TrainingOrder';

        $this->fields_list = array(
            'id_training_order' => array('title' => 'ID'),
            'index' => array('title' => 'Indeks'),
            'customer_name' => array('title' => 'Klient'),
            'status' => array('title' => 'Status')
        );

        $this->bootstrap = true;

        $this->addRowAction('view');
        $this->addRowAction('delete');

        parent::__construct();


        $this->_select = 'CONCAT(c.`firstname`, " ", c.`lastname`) AS customer_name';
        $this->_join = 'LEFT JOIN `' . _DB_PREFIX_ . 'customer` c ON c.`id_customer` = a.`id_customer`';

        $this->meta_title = 'Lista rezerwacji szkoleń';
        $this->toolbar_title[] = $this->meta_title;
       
    }

    public function renderView()
    {

        $link = $this->context->link->getAdminLink('AdminTrainingsParticipants', false);
        $token = Tools::getAdminTokenLite('AdminTrainingsParticipants');
        $link_training = $this->context->link->getAdminLink('AdminTrainings', false);
        $token_training = Tools::getAdminTokenLite('AdminTrainings');
        $tpl = $this->context->smarty->createTemplate(dirname(__FILE__) . '/../../views/templates/admin/orders.tpl');

        $order = new TrainingOrder(Tools::getValue('id_training_order'));
        $certyficates = TrainingOrder::getParticipantCertyficate(Tools::getValue('id_training_order'));
        $customer = new Customer($order->id_customer);

        $tpl->assign(array(
            'order' => $order,
            'customer' => $customer,
            'trainings' => $order->getOrderTrainings($order->id_training_order),
            'bundle' => $order->getOrderBundlesBackend($order->id_customer,$order->index),
            'link' => $link,
            'token' => $token,
            'link_training' => $link_training,
            'token_training' => $token_training,
            'certyficates' => $certyficates,
            'mypath' => isset($_POST['remove_certyficate_training_order']),
            'invoice' => $this->displayInvoice()
           
        ));

        return $tpl->fetch();
    }

    public function initContent()
    {
        $this->display_column_left = false;
        $this-> errors = [];
       


        if (Tools::getIsset('save-invoice')) {
            $invoice_file = pSQL($_POST['invoice']);
            $tmpName = (isset($_FILES['invoice']['tmp_name']))?$_FILES['invoice']['tmp_name']:'';
            $name = (isset($_FILES['invoice']['name']))?$_FILES['invoice']['name']:'';
            $error = isset($_FILES['invoice']['error'])?$_FILES['invoice']['error']:[];
           
        
            $path = _PS_MODULE_DIR_ . 'trainings/invoices/' . $this->id_object;
           
            $newName = _PS_MODULE_DIR_ . 'trainings/invoices/' . $this->id_object . '/'.$name;
            

           
            if (file_exists($path)) {
                if ($error === UPLOAD_ERR_OK) {
                   
                    if (!file_exists($newName)) {
                        if(file_exists($path)){
                            $files_to_remove = glob($path."/*.*");
                            foreach($files_to_remove as $item_to_remove){
                                unlink($item_to_remove);
                            }
                            
                        }
                        if (!move_uploaded_file($tmpName, $newName)) {
                   
                            return false;
                        }  
                    } else {
                        if(file_exists($path)){
                            $files_to_remove = glob($path."/*.*");
                            foreach($files_to_remove as $item_to_remove){
                                unlink($item_to_remove);
                            }
                            
                        }
                        if (!move_uploaded_file($tmpName, $newName)) {
                   
                            return false;
                        }  
                    }
                }
     
            } else {
               
                mkdir(_PS_MODULE_DIR_ . 'trainings/invoices/' . $this->id_object, 0777);
                if ($error === UPLOAD_ERR_OK) {
                    if (!file_exists($newName)) {
                        if (!move_uploaded_file($tmpName, $newName)) {

                            return false;
                        } 
                    }
                }
            }

            if(isset($_FILES['invoice'])){
                unset($_FILES['invoice']);
            }
        }


        if (Tools::getIsset('save-certificate')) {
           
            $id_training_participant = pSQL($_POST['id_training_participant']);
            
            $tmpName = $_FILES['certificate']['tmp_name'];
            $error = $_FILES['certificate']['error'];
            $path = _PS_MODULE_DIR_ . 'trainings/certificates/' . $this->id_object;

            $name = $id_training_participant . '-' . $_FILES['certificate']['name'];
            $newName = _PS_MODULE_DIR_ . 'trainings/certificates/' . $this->id_object . '/' . $name;
   
            if (file_exists($path)) {
                if ($error === UPLOAD_ERR_OK) {
                    if (!file_exists($newName)) {
                        if (!move_uploaded_file($tmpName, $newName)) {
                            return false;
                        } else {
                        }
                    }
                }
            } else {
                mkdir(_PS_MODULE_DIR_ . 'trainings/certificates/' . $this->id_object, 0777);
                if ($error === UPLOAD_ERR_OK) {
                    if (!file_exists($newName)) {
                        if (!move_uploaded_file($tmpName, $newName)) {
                            return false;
                        }
                    }
                }
            }
        }
        if(Tools::getIsset('remove_certyficate_training_order')){
            $remove_certyficate = $_POST['remove_certyficate_name'];
            $path= _PS_MODULE_DIR_ . 'trainings/certificates/'. $this->id_object . '/'.$remove_certyficate;
           
            if(file_exists($path)){
                unlink($path);
            }
        }


        parent::initContent();
    }

    private function displayInvoice(){
        $path = _PS_MODULE_DIR_ . 'trainings/invoices/' . $this->id_object;
        $files_to_diplay  = [];
        if(file_exists($path)){
            $files_to_diplay = glob($path."/*.*",GLOB_NOSORT);
            
        }
        return $files_to_diplay;
    }
}
