<?php

class AdminTrainingsCategoriesController extends ModuleAdminController
{
   
    private $id_training_category = null;
    public function __construct()
    {
        $this->table = 'training_category';
        $this->className = 'TrainingCategory';

        $this->fields_list = array(
            'id_training_category' => array('title' => 'ID', 'align' => 'center', 'width' => 25),
            'name' => array('title' => 'Nazwa', 'width' => 50),
            'alias' => array('title' => 'Alias', 'width' => 50),
           

        );

        $this->bootstrap = true;
        $this->addRowAction('view');
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        $this->id_training_caregory = Tools::getValue("id_training_category");

        if($this->id_training_caregory){
            $this->_filter .= ' AND a.`id_parent` = '.$this->id_training_caregory;
        } else {
            $this->_filter .= ' AND a.`id_parent` = 0';
        }
        
        parent::__construct();
    }
    public function initProcess()
    {
        if (Tools::getIsset('configure')) {
            if (Tools::getIsset('viewconfiguration')) {
                /**
                 * Przekierowanie do strony edycji 
                 * szkolenia 
                 */
                $id_training = Tools::getValue('id_training');
                $token = Tools::getAdminTokenLite('AdminTrainings');
                Tools::redirectAdmin(

                    $this->context->link->getAdminLink('AdminTrainings', false) .
                        '&id_training=' . $id_training . '&updatetraining&token=' . $token
                );
            }
        }

        parent::initProcess();
       
    }

  
    public function renderForm()
    {
        // $this->initToolbar();
        $this->context = Context::getContext();
        $this->context->controller = $this;
        
        if (Tools::getIsset('addtraining_category')) {
            $this->fields_form = array(
                'legend' => array('title' => 'Dodaj / edytuj kategorie'),
                'input' => array(
                    array('type' => 'text', 'label' => 'Nazwa', 'name' => 'name', 'required' => true),
                    array('type' => 'text', 'label' => 'Alias', 'name' => 'alias', 'required' => true),
               
                ),
                'submit' => array('title' => 'Zapisz')
            );
            return parent::renderForm();
        } else {
            $categorys = (Tools::getValue("id_training_category"))?$this->getCaterorys():TrainingCategory::getTrainingCategories();
            $options = [];
            array_push($options,array(
  
                'id_training_category' => 0, 

                'name' => 'Brak' 

            ));
            
              foreach($categorys as $item){
                array_push($options,$item);
              }

            $this->fields_form = array(
                'legend' => array('title' => 'Dodaj / edytuj kategorie'),
                'input' => array(
                    array('type' => 'text', 'label' => 'Nazwa', 'name' => 'name', 'required' => true),
                    array('type' => 'text', 'label' => 'Alias', 'name' => 'alias', 'required' => true),
                  
                    array(
                        'type' => 'select',
                        'label' => 'Wybierz kategorię nadrzędną',
                        'name' => 'id_parent',
                        'options' => array(
                            'query' => $options,
                            'id' => 'id_training_category',
                            'name' => 'name'
                        ),
                       
                    )
                ),
                'submit' => array('title' => 'Zapisz')
            );
            return parent::renderForm() . $this->generateList();
        }

        
    }

    public function generateList()
    {
        $fieldsList = array(
            'id_training' => array(
                'title' => 'Id',
                'type' => 'text'
            ),
            'nazwa_szkolenia' => array(
                'title' => 'Nazwa',
                'type' => 'text'
            ),
            'price' => array(
                'title' => 'Cena',
                'type' => 'text'
            ),
            'start_time' => array(
                'title' => 'Data rozpoczęcia',
                'type' => 'text'
            ),
            'end_time' => array(
                'title' => 'Data zakończenia',
                'type' => 'text'
            ),
            'place_of_training' => array(
                'title' => 'Miejsce szkolenia',
                'type' => 'text'
            ),
            'voivodeship' => array(
                'title' => 'Województwo',
                'type' => 'text'
            ),
        );

        $trainings = TrainingCategory::getTrainings($this->id_training_caregory);
        $helper = new HelperList();


        $helper->shopLinkType = '';
        $helper->actions = array('view');
        $helper->identifier = 'id_training';
        $helper->show_toolbar = true;
        $helper->title = 'Lista szkoleń należacych do kategorii';
        $helper->name = 'test';

        $helper->token = Tools::getAdminTokenLite('AdminTrainings');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=';
        return $helper->generateList($trainings, $fieldsList);
    }

    public function renderView()
    {
      
         return parent::renderList();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAddtraining_category')) {
            $id_training_category = Tools::getValue("id_parent");
            $trainingcategory = TrainingCategory::getCategory($id_training_category);
            if($id_training_category == 0){
                Db::getInstance()->update('training_category', array(
                    'level_depth' => 0
                  ),'id_training_category = '.Tools::getValue("id_training_category"));
            } else {
                Db::getInstance()->update('training_category', array(
                    'level_depth' => $trainingcategory['level_depth'] + 1
                  ),'id_training_category = '.Tools::getValue("id_training_category"));
            }
            
        }

        parent::postProcess();

    }

    private function getCaterorys() {

        $categories = Db::getInstance()->executeS('
            SELECT * FROM `' . _DB_PREFIX_ . 'training_category` where `id_training_category` <> '.Tools::getValue("id_training_category").'
        ');
        
        return $categories;
    }
    
}
