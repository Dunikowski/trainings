<?php

class AdminTrainingsController extends ModuleAdminController
{
    public function __construct()
    {
        $this->table = 'training';
        $this->className = 'Training';
        $this->_where = 'AND (a.`active` = 1 AND a.`end_time` > \''.Tools::displayDate(date('Y-m-d H:i:s'), null, 1).'\') ';
        $this->fields_list = array(
            'id_training' => array('title' => 'ID', 'align' => 'left', 'width' => 45),
            'name' => array('title' => 'Nazwa', 'width' => 150),
            'price' => array('title' => 'Cena', 'width' => 50),
            'slots' => array('title' => 'Miejsca', 'width' => 45)

        );

        Training::updateActive();
        $this->bootstrap = true;

        $this->addRowAction('edit');
      

        parent::__construct();
    }

    public function initContent()
    {
        
        parent::initContent();
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::getIsset('configure')) {

            if (Tools::getIsset('viewconfiguration')) {
                $id_training = Tools::getValue('id_training');
                $slotsTab = (int)Training::getNumberTrainingPlaces(Tools::getValue('id_training'));
                $nb_participant = (int)TrainingParticipant::getNbParticipant(Tools::getValue('id_training'));
                $person = Training::getPeronWithReserveList(Tools::getValue('id_training_reserve_list'));
                if(($slotsTab - $nb_participant) > 0){

                     foreach ($person as $data) {
                     $firstname = $data['firstname'];
                     $lastname = $data['lastname'];
                     $id_training = $data['id_training'];
                     $email = $data['email'];
                     $phone = $data['phone'];
                 }
                 $id_cart = 0;
              
                 Training::signParticipant(
                     $firstname,
                     $lastname,
                     $email,
                     $phone,
                     $id_training,
                     $id_bundle,
                     $id_cart,
                     true,
                     true
                 );
                
                 Training::deletePeronWithReserveList(Tools::getValue('id_training_reserve_list'));
                

                } else {
                    $this->errors[] = Tools::displayError('Nie można dodać dodatkowego uczestnika do szkolenia. Za mało miejsc.');
                }
               
            }
            

            if (Tools::getIsset('updateconfiguration')) {
                /**
                 * Przekierowanie do strony edycji 
                 * uczestnika 
                 */
                $id_training_participant = Tools::getValue('id_training_participant');
                $token = Tools::getAdminTokenLite('AdminTrainingsParticipants');

                Tools::redirectAdmin(

                    $this->context->link->getAdminLink('AdminTrainingsParticipants', false) .
                    '&id_training_participant=' . $id_training_participant . '&updatetraining_participant&token=' . $token
                );
            }

            if (Tools::getIsset('deleteconfiguration')) {
                $participant = TrainingParticipant::getParticipant(Tools::getValue('id_training_participant'));
                foreach ($participant as $data) {
                    $id_training = $data['id_training'];
                }
               

                TrainingParticipant::deleteParticipant(Tools::getValue('id_training_participant'));
            }
        }
    }
    public function renderList()
	{
	
		$this->addRowAction('Moi');
		return parent::renderList();
    }
    
    public function displayMoiLink($token = null, $id, $name = null)
	{
		 $tpl = $this->createTemplate('helpers/list/list_action_view.tpl');
        if (!array_key_exists('View', self::$cache_lang)) {
            self::$cache_lang['View'] = $this->l('Dodaj do szkolenia', 'Helper');
        }

        $tpl->assign(array(
            'href' => AdminController::$currentIndex . '&configure=participantList'. '&id_training='.Tools::getValue('id_training').'&id_training_reserve_list='.$id.'&viewconfiguration&token='.($token != null ? $token : $this->token),
            'action' => self::$cache_lang['View'],
        ));
	
		return $tpl->fetch();
    }
    
    public function renderForm()
    {
        $query = array(
            array(
                'value' => 1,
                'label' => 'Tak'
            ),
            array(
                'value' => 0,
                'label' => 'Nie'
            )
        );

        $this->context = Context::getContext();
        $this->context->controller = $this;
        $this->fields_form = array(
            'legend' => array('title' => 'Dodaj / edytuj szkolenie'),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => 'ID szkolenia',
                    'name' => 'id_training',
                   
                ),
                array(
                    'type' => 'select',
                    'label' => 'Kategoria',
                    'name' => 'id_training_category',
                    'required' => true,
                    'default_value' => 1,
                    'options' => array(
                        'query' => TrainingCategory::getTrainingCategories(),
                        'id' => 'id_training_category',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => 'Nazwa',
                    'name' => 'name',
                    'size' => 30,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Alias',
                    'name' => 'alias',
                    'size' => 30,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Autor',
                    'name' => 'author',
                    'size' => 30,
                    'required' => true
                ),
                array(
                    'type' => 'select',
                    'label' => 'Prowadzący szkolenie',
                    'name' => 'id_training_instructor',
                    'required' => true,
                    'options' => array(
                        'query' => TrainingInstructor::getInstructors(),
                        'id' => 'id_training_instructor',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => 'Cena',
                    'name' => 'price',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Cena promocyjna',
                    'name' => 'discount_price',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Liczba miejsc',
                    'name' => 'slots',
                    'required' => true
                ),
                array(
                    'type' => 'textarea',
                    'autoload_rte' => true,
                    'label' => 'Krótki opis',
                    'name' => 'short_desc',
                    'required' => true
                ),
                array(
                    'type' => 'textarea',
                    'autoload_rte' => true,
                    'label' => 'Szczegółowy opis',
                    'name' => 'long_desc',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'autoload_rte' => true,
                    'label' => 'Miejsce szkolenia',
                    'name' => 'place_of_training',
                    'required' => true
                ),
                array(
                    'type' => 'select',
                    'autoload_rte' => true,
                    'label' => 'Województwo',
                    'name' => 'voivodeship',
                    'required' => true,
                    'options' => array(
                        'query' => Voivodeship::getOptions(),
                        'id' => 'voivodeship_name',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'file',
                    'multiple' => true,
                    'label' => 'Załączniki do szkolenia',
                    'name' => 'file',
                    'required' => false
                ),
                array(
                    'type' => 'file',
                    'multiple' => false,
                    'label' => 'Dodaj logo szkolenia',
                    'name' => 'logo',
                    'required' => false
                ),
                array(
                    'type' => 'checkbox',
                    'label' => 'Usuń logo',
                    'name' => 'remove_logo',
                     'values' => array(
                         'query' => array(
                             array( 'val' => '1'),
                             ),
                         'id' => 'id',
                         'name' => 'name')
                    
                ),
                array(
                    'type' => 'file',
                    'multiple' => false,
                    'label' => 'Dodaj zdjecie szkolenia',
                    'name' => 'picture',
                    'required' => false
                ),
                array(
                    'type' => 'checkbox',
                    'label' => 'Usuń zdjęcie',
                    'name' => 'remove_picture',
                     'values' => array(
                         'query' => array(
                             array( 'val' => '1'),
                             ),
                         'id' => 'id',
                         'name' => 'name')
                    
                ),
                array(
                    'type' => 'datetime',
                    'label' => 'Data rozpoczęcia',
                    'name' => 'start_time',
                    'size' => 30,
                    'required' => true
                ),
                array(
                    'type' => 'datetime',
                    'label' => 'Data zakończenia',
                    'name' => 'end_time',
                    'size' => 30,
                    'required' => true
                ),
                $options = array(
                    array(
                        'id_option' => 1,
                        'name' => 'Student'
                    ),
                    array(
                        'id_option' => 2,
                        'name' => 'Lekarz'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => 'Dla kogo szkolenie?',
                    'name' => 'for_whom',
                    'required' => false,
                    'options' => array(
                        'query' => $options,
                        'id' => 'id_option',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Powiadomienia sms',
                    'name' => 'sms_notifications',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Czy wymagany numer lekarza?',
                    'name' => 'need_doc_number',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Czy szkolenie może być sprzedawane jako pojedyncze?',
                    'name' => 'can_be_sold_alone',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Promowane na stronie głównej?',
                    'name' => 'is_promoted',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Płatności dotpay',
                    'name' => 'dotpay_payment',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Lista rezerwowa',
                    'name' => 'reserve_list',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Szkolenie aktywne',
                    'name' => 'active',
                    'default_value' => Tools::getValue('active'),
                    'values' => $query
                ),

            ),
            'submit' => array('title' => 'Zapisz')
        );

        if (Tools::getIsset('addtraining')) {
            return parent::renderForm();
        } else {
            return parent::renderForm() . $this->generateList()  . $this->reserveList() . $this->listAtachments();
        }
    }

    public function generateList()
    {

        $fieldsList = array(
            'id_training_participant' => array(
                'title' => 'Id',
                'type' => 'text'
            ),
            'name' => array(
                'title' => 'Imię',
                'type' => 'text'
            ),
            'surname' => array(
                'title' => 'Nazwisko',
                'type' => 'text'
            ),
            'email' => array(
                'title' => 'e-mail',
                'type' => 'text'
            ),
            'phone' => array(
                'title' => 'Telefon',
                'type' => 'text'
            ),
            'rodo_ok' => array(
                'title' => 'Potwierdzony',
                'type' => 'bool'
            )
        );
        $participants = Training::getTrainingParticipants(Tools::getValue('id_training'));
        $helper = new HelperList();

        $helper->shopLinkType = '';
        $helper->actions = array('edit', 'delete');
        $helper->identifier = 'id_training_participant';
        $helper->show_toolbar = false;

        $helper->title = 'Lista uczestników';
        $helper->name = 'test';
        $helper->no_link = true;
        $helper->token = Tools::getAdminTokenLite('AdminTrainings');

        //  $helper->currentIndex = $this->context->link->getAdminLink('AdminTrainingsParticipants', false) . '&configure=participantList';
        $helper->currentIndex = AdminController::$currentIndex . '&configure=participantList';

        return $helper->generateList($participants, $fieldsList);
    }

    public function reserveList()
    {

        $boolTab = Training::hasReserveList(Tools::getValue('id_training'));
        foreach ($boolTab as $bools) {
            foreach ($bools as $hasList) {
                $hasList;
                //  var_dump($bool);
            }
        }
        
        if ($hasList == "1") {

            $fieldsList = array(
                'id_training_reserve_list' => array(
                    'title' => 'Id chętnego',
                    'type' => 'text'
                ),
                'id_training' => array(
                    'title' => 'Id szkolenia',
                    'type' => 'text'
                ),
                'firstname' => array(
                    'title' => 'Imię',
                    'type' => 'text'
                ),
                'lastname' => array(
                    'title' => 'Nazwisko',
                    'type' => 'text'
                ),
                'email' => array(
                    'title' => 'E-mail',
                    'type' => 'text'
                ),
                'telephone' => array(
                    'title' => 'Telefon',
                    'type' => 'text'
                ),
               
            );
            $participants = Training::getTrainingReserveList(Tools::getValue('id_training'));
            $helper = new HelperList();


            $helper->shopLinkType = '';

            $helper->identifier = 'id_training_reserve_list';

              $helper->submit_action = 'Moi';
              $helper->actions = array('Moi');
            
            //  $helper->submit_action = 'view';
            //  $helper->actions = array('view');
            $helper->show_toolbar = true;
            $helper->title = 'Lista rezerwowa';
            //$helper->table = 'firstname';
            $helper->no_link = false;

            $helper->token = Tools::getAdminTokenLite('AdminTrainings');
            $firstname = Tools::getValue('firstname');


            
            $helper->currentIndex = AdminController::$currentIndex . '&configure=participantList'. '&id_training='.Tools::getValue('id_training');
           
          
            return $helper->generateList($participants, $fieldsList);
        }
    }

    public function processSave()
    {

        if (isset($_FILES['file'])) {

            foreach ($_FILES['file']['name'] as $i => $name) {
                $tmpName = $_FILES['file']['tmp_name'][$i];
                $error = $_FILES['file']['error'][$i];
                $newName = _PS_MODULE_DIR_ . 'trainings/files/' . $this->id_object . '-' . $name;

                if ($error === UPLOAD_ERR_OK) {

                    if (!file_exists($newName)) {
                        if (!move_uploaded_file($tmpName, $newName)) {
                            return false;
                        }
                    }
                }
            }
        }
       
        if (isset($_FILES['picture']) && $_FILES['picture']['name'] != '') {
            
            $this->savePicture('picture');
        }

        if (isset($_FILES['logo']) && $_FILES['logo']['name'] != '') {

            $this->savePicture('logo');
        }

        if(isset($_POST['remove_logo_']) && $_POST['remove_logo_'] == 1){

             Db::getInstance()->update('training', array(
                'logo' => null
             ),'id_training = '.$this->id_object);
        }
       
        if(isset($_POST['remove_picture_']) && $_POST['remove_picture_'] == 1){

            Db::getInstance()->update('training', array(
               'image' => null
            ),'id_training = '.$this->id_object);
       }
       
        return parent::processSave();
    }


    public function listAtachments()
    {


        $training = Tools::getValue('id_training');
        $fieldsList = Training::getTrainingAttachments($training);

        //$directory = $_SERVER['DOCUMENT_ROOT'] . '/modules/trainings/files';
        $directory = _PS_MODULE_DIR_ . 'trainings/files';


        foreach ($fieldsList as $file) {

            $file = '<li><a style="color: #555" rel="nofollow" target="_blank" href="' . $directory . '/' . $file . '">' . $file . '</a></li>
            <form class="form-horizontal" method="POST">
                <input name="deletefile" type="hidden" value="' . $file . '"/>
                <button type="submit" name="mod_delete_file">Usuń plik</button>
            </form>';
            $attachments[] = $file;
        };
        // $deleteFile = Training::deleteFile($file);
        //implode("", $attachments)
        return '<div class="col-md-12" style="background: #fff; padding: 20px 15px; " >
        <p style="color: #555; border-bottom: solid 1px #eee;">LISTA ZAŁĄCZNIKÓW DO SZKOLENIA:</p>
        <ul>' .implode("", $attachments) . '</ul></div>';
    }

    private function savePicture($type){

        $tmpName = (isset($_FILES[$type]['tmp_name']))?$_FILES[$type]['tmp_name']:'';
        $name = (isset($_FILES[$type]['name']))?$_FILES[$type]['name']:'';
        $error = isset($_FILES[$type]['error'])?$_FILES[$type]['error']:[];
       
    
        $path = _PS_MODULE_DIR_ . 'trainings/img/img_trainings/' . $this->id_object;
       
        $newName = _PS_MODULE_DIR_ . 'trainings/img/img_trainings/' . $this->id_object . '/'.$type.'_'.$name;
        

       
        if (file_exists($path)) {
            if ($error === UPLOAD_ERR_OK) {
               
                if (!file_exists($newName)) {
                    if(file_exists($path)){
                        $files_to_remove = glob($path."/".$type."*.*");
                        foreach($files_to_remove as $item_to_remove){
                            unlink($item_to_remove);
                        }
                        
                    }
                    if (!move_uploaded_file($tmpName, $newName)) {
               
                        return false;
                    }  
                } else {
                    if(file_exists($path)){
                        $files_to_remove = glob($path."/".$type."*.*");
                        foreach($files_to_remove as $item_to_remove){
                            unlink($item_to_remove);
                        }
                    }
                    if (!move_uploaded_file($tmpName, $newName)) {
               
                        return false;
                    } 

                }
            }
 
        } else {
           
            mkdir(_PS_MODULE_DIR_ . 'trainings/img/img_trainings/' . $this->id_object, 0777);
        
             if ($error === UPLOAD_ERR_OK) {
                 if (!file_exists($newName)) {
                     if (!move_uploaded_file($tmpName, $newName)) {
                         return false;
                     } 
                 }
             }
        }

        $column_name = '';

        if($type == 'picture'){
            $column_name = 'image';
        } else {
            $column_name = 'logo';
        }

        Db::getInstance()->update('training', array(
            $column_name => 'trainings/img/img_trainings/' . $this->id_object . '/'.$type.'_'.$name
        ), 'id_training = ' . $this->id_object);

    }

   
}
