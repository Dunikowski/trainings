<?php

class AdminTrainingsInstructorsController extends ModuleAdminController
{
    public function __construct()
    {
        $this->table = 'training_instructor';
        $this->className = 'TrainingInstructor';

        $this->fields_list = array(
            'id_training_instructor' => array('title' => 'ID', 'align' => 'left', 'width' => 45),
            'name' => array('title' => 'Name', 'align' => 'left', 'width' => 45),
            'surname' => array('title' => 'Nazwisko', 'align' => 'left', 'width' => 45),
        );

        $this->bootstrap = true;

        $this->addRowAction('edit');
        $this->addRowAction('delete');

        parent::__construct();

        if (Tools::getIsset('id_training_instructor')) {
            $img_url = TrainingInstructor::getInstructorImageLink(Tools::getValue('id_training_instructor'));
            $img = "<img src='$img_url'>";
        }
    }
    public function initProcess()
    {
        parent::initProcess();

        if (Tools::getIsset('configure')) {
            if (Tools::getIsset('viewconfiguration')) {
                /**
                 * Przekierowanie do strony edycji 
                 * szkolenia 
                 */
                $id_training = Tools::getValue('id_training');
                $token = Tools::getAdminTokenLite('AdminTrainings');
                Tools::redirectAdmin(

                    $this->context->link->getAdminLink('AdminTrainings', false) .
                        '&id_training=' . $id_training . '&updatetraining&token=' . $token
                );
            }
        }
    }

    public function renderForm()
    {
        $this->context = Context::getContext();
        $this->context->controller = $this;
        $this->fields_form = array(
            'legend' => array('title' => 'Dodaj / edytuj prowadzącego'),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => 'Imię',
                    'name' => 'name',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Nazwisko',
                    'name' => 'surname',
                    'required' => true
                ),
                array(
                    'type' => 'textarea',
                    'label' => 'Opis',
                    'name' => 'description',
                    'autoload_rte' => true

                ),
                array(
                    'type' => 'file',
                    'label' => 'Zdjęcie',
                    'name' => 'image',
                    'display_image' => true,
                    'image' => $img ?? false,
                    // TODO USUWANIE ZDJĘĆ
                )
            ),
            'submit' => array('title' => 'Zapisz')
        );
        return parent::renderForm() . $this->generateList();
    }

    public function generateList()
    {
        $fieldsList = array(
            'id_training' => array(
                'title' => 'Id',
                'type' => 'text'
            ),
            'name' => array(
                'title' => 'Nazwa',
                'type' => 'text'
            ),
            'price' => array(
                'title' => 'Cena',
                'type' => 'text'
            ),
            'start_time' => array(
                'title' => 'Data rozpoczęcia',
                'type' => 'text'
            ),
            'end_time' => array(
                'title' => 'Data zakończenia',
                'type' => 'text'
            ),
            'place_of_training' => array(
                'title' => 'Miejsce szkolenia',
                'type' => 'text'
            ),
        );
        $training_instructor = Training::getInstructorTrainings(Tools::getValue('id_training_instructor'));
        $helper = new HelperList();

        $helper->shopLinkType = '';
        // $helper->actions = array('edit', 'delete', 'view');
        $helper->actions = array('view');
        $helper->identifier = 'id_training';
        $helper->show_toolbar = true;
        $helper->title = 'Lista prowadzonych szkoleń';
        $helper->name = 'test';

        $helper->token = Tools::getAdminTokenLite('AdminTrainings');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=';
        // $helper->currentIndex = $this->context->link->getAdminLink('AdminTrainings', false) . '&updatetraining';
        return $helper->generateList($training_instructor, $fieldsList);
    }

    public function initContent()
    {
        parent::initContent();
    }

    public function processSave()
    {

        /**
         * TODO - w tym momencie po usunięciu modułu znikają dane z bazy danych
         * ale zdjęcie fizycznie zostają w folderze - trzeba przerobić kod aby
         * jeśli zdjęcie już istnieje usuwał je i wgrywał nowe
         */

        if (isset($_FILES['image'])) {
            $tmpName = $_FILES['image']['tmp_name'];
            $error = $_FILES['image']['error'];
            $name = $this->id_object . '-' . $_FILES['image']['name'];
            $newName = _PS_MODULE_DIR_ . 'trainings/img/instructors/' . $name;

            if ($error === UPLOAD_ERR_OK) {
                if (!file_exists($newName)) {
                    if (!move_uploaded_file($tmpName, $newName)) {
                        return false;
                    } else {
                        TrainingInstructor::setImageName($name, $this->id_object);
                    }
                }
            }
        }

        parent::processSave();
    }
}
