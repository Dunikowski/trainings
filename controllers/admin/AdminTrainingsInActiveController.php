<?php

class AdminTrainingsInActiveController extends ModuleAdminController
{
    public function __construct()
    {
        $this->table = 'training';
        $this->className = 'Training';
        $this->_where = 'AND (a.`active` = 0 OR a.`end_time` < \''.Tools::displayDate(date('Y-m-d H:i:s'), null, 1).'\') ';
        $this->fields_list = array(
            'id_training' => array('title' => 'ID', 'align' => 'left', 'width' => 45),
            'name' => array('title' => 'Nazwa', 'width' => 150),
            'price' => array('title' => 'Cena', 'width' => 50),
            'slots' => array('title' => 'Miejsca', 'width' => 45)
        );
        Training::updateActive();
        $this->bootstrap = true;
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        parent::__construct();
    }

    public function initContent()
    {
        
        parent::initContent();
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::getIsset('configure')) {

            if (Tools::getIsset('viewconfiguration')) {

               


                $person = Training::getPeronWithReserveList(Tools::getValue('id_training_reserve_list'));

                foreach ($person as $data) {
                    $firstname = $data['firstname'];
                    $lastname = $data['lastname'];
                    $id_training = $data['id_training'];
                    $email = $data['email'];
                    $phone = $data['phone'];
                }
                $id_cart = 0;
                // TODO jeśli liczba miejsc jest większa niż 0
                // if ($slot == "0") {
                Training::signParticipant(
                    $firstname,
                    $lastname,
                    $email,
                    $phone,
                    $id_training,
                    $id_cart
                );
                
                Training::deletePeronWithReserveList(Tools::getValue('id_training_reserve_list'));
            }
            // }

            if (Tools::getIsset('updateconfiguration')) {
                /**
                 * Przekierowanie do strony edycji 
                 * uczestnika 
                 */
                $id_training_participant = Tools::getValue('id_training_participant');
                $token = Tools::getAdminTokenLite('AdminTrainingsParticipants');

                Tools::redirectAdmin(

                    $this->context->link->getAdminLink('AdminTrainingsParticipants', false) .
                        '&id_training_participant=' . $id_training_participant . '&updatetraining_participant&token=' . $token
                );
            }

            if (Tools::getIsset('deleteconfiguration')) {
                $participant = TrainingParticipant::getParticipant(Tools::getValue('id_training_participant'));
                foreach ($participant as $data) {
                    $id_training = $data['id_training'];
                }
              
                TrainingParticipant::deleteParticipant(Tools::getValue('id_training_participant'));
            }
        }
    }

    public function renderForm()
    {
        $query = array(
            array(
                'value' => 1,
                'label' => 'Tak'
            ),
            array(
                'value' => 0,
                'label' => 'Nie'
            )
        );

        $this->context = Context::getContext();
       
        $this->context->controller = $this;
        $this->fields_form = array(
            'legend' => array('title' => 'Dodaj / edytuj szkolenie'),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => 'ID szkolenia',
                    'name' => 'id_training'
                   
                ),
                array(
                    'type' => 'select',
                    'label' => 'Kategoria',
                    'name' => 'id_training_category',
                    'required' => true,
                    'default_value' => 1,
                    'options' => array(
                        'query' => TrainingCategory::getTrainingCategories(),
                        'id' => 'id_training_category',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => 'Nazwa',
                    'name' => 'name',
                    'size' => 30,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Alias',
                    'name' => 'alias',
                    'size' => 30,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Autor',
                    'name' => 'author',
                    'size' => 30,
                    'required' => true
                ),
                array(
                    'type' => 'select',
                    'label' => 'Prowadzący szkolenie',
                    'name' => 'id_training_instructor',
                    'required' => true,
                    'options' => array(
                        'query' => TrainingInstructor::getInstructors(),
                        'id' => 'id_training_instructor',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => 'Cena',
                    'name' => 'price',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Cena promocyjna',
                    'name' => 'discount_price',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Liczba miejsc',
                    'name' => 'slots',
                    'required' => true
                ),
                array(
                    'type' => 'textarea',
                    'autoload_rte' => true,
                    'label' => 'Krótki opis',
                    'name' => 'short_desc',
                    'required' => true
                ),
                array(
                    'type' => 'textarea',
                    'autoload_rte' => true,
                    'label' => 'Szczegółowy opis',
                    'name' => 'long_desc',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'autoload_rte' => true,
                    'label' => 'Miejsce szkolenia',
                    'name' => 'place_of_training',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'autoload_rte' => true,
                    'label' => 'Województwo',
                    'name' => 'voivodeship',
                    'required' => true
                ),
                array(
                    'type' => 'file',
                    'multiple' => true,
                    'label' => 'Załączniki do szkolenia',
                    'name' => 'file',
                    'required' => false
                ),
                array(
                    'type' => 'datetime',
                    'label' => 'Data rozpoczęcia',
                    'name' => 'start_time',
                    'size' => 30,
                    'required' => true
                ),
                array(
                    'type' => 'datetime',
                    'label' => 'Data zakończenia',
                    'name' => 'end_time',
                    'size' => 30,
                    'required' => true
                ),
                $options = array(
                    array(
                        'id_option' => 1,
                        'name' => 'Student'
                    ),
                    array(
                        'id_option' => 2,
                        'name' => 'Lekarz'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => 'Dla kogo szkolenie?',
                    'name' => 'for_whom',
                    'required' => false,
                    'options' => array(
                        'query' => $options,
                        'id' => 'id_option',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Powiadomienia sms',
                    'name' => 'sms_notifications',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Czy wymagany numer lekarza?',
                    'name' => 'need_doc_number',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Czy szkolenie może być sprzedawane jako pojedyncze?',
                    'name' => 'can_be_sold_alone',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Promowane na stronie głównej?',
                    'name' => 'is_promoted',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Płatności dotpay',
                    'name' => 'dotpay_payment',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Lista rezerwowa',
                    'name' => 'reserve_list',
                    'default_value' => 0,
                    'values' => $query
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Szkolenie aktywne',
                    'name' => 'active',
                    'default_value' => 0,
                    'values' => $query
                ),

            ),
            'submit' => array('title' => 'Zapisz')
        );

       
      
       
        return parent::renderForm();
    }
    public function processSave()
    {

        if (isset($_FILES['file'])) {
            foreach ($_FILES['file']['name'] as $i => $name) {
                $tmpName = $_FILES['file']['tmp_name'][$i];
                $error = $_FILES['file']['error'][$i];
                $newName = _PS_MODULE_DIR_ . 'trainings/files/' . $this->id_object . '-' . $name;

                if ($error === UPLOAD_ERR_OK) {

                    if (!file_exists($newName)) {
                        if (!move_uploaded_file($tmpName, $newName)) {
                            return false;
                        }
                    }
                }
            }
        }

        return parent::processSave();
    }

    public function processDelete() {
        $files = glob(_PS_MODULE_DIR_ . 'trainings/files/'. $this->id_object . '-*.*');
        
        if (is_array($files)) {
            foreach($files as $file){
                
                unlink($file);
            }
        }
       
        return parent::processDelete();
    }

}
