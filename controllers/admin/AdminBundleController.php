<?php

class AdminBundleController extends ModuleAdminController
{
    public function __construct()
    {
        $this->table = 'bundle';
        $this->className = 'Bundle';
        $this->_where = 'AND a.`active` = 1 ';
        $this->fields_list = array(
            'id_bundle' => array('title' => 'ID', 'align' => 'left', 'width' => 45),
            'title' => array('title' => 'Tytuł')
        );

        $this->bootstrap = true;

        $this->addRowAction('edit');

        parent::__construct();
       
    }

    public function initContent()
    {

        parent::initContent();
    }
    public function renderForm()
    {
        $query = array(
            array(
                'value' => 1,
                'label' => 'Tak'
            ),
            array(
                'value' => 0,
                'label' => 'Nie'
            )
        );
        $this->context = Context::getContext();
        $this->context->controller = $this;

        $this->fields_form = array(
            'legend' => array('title' => 'Dodaj / edytuj pakiet szkoleń'),
            'input' => array(
                
                array(
                    'type' => 'text',
                    'label' => 'Tytuł',
                    'name' => 'title',
                    'required' => true
                ),
                array(
                    'type' => 'textarea',
                    'autoload_rte' => true,
                    'label' => 'Opis',
                    'name' => 'description'
                ),
                array(
                    'type' => 'select',
                    'label' => 'Dostępne szkolenia',
                    'name' => 'id_training[]',
                    'required' => true,
                    'multiple' => true,

                    'options' => array(
                        'query' => Training::getAllTrainings(),
                        'id' => 'id_training',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => 'Rabat',
                    'name' => 'discount',
                    'required' => true
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Pakiet aktywny',
                    'name' => 'active',
                    'default_value' => 1,
                    'values' => $query,
                ),
                array(
                    'type' => 'file',
                    'multiple' => false,
                    'label' => 'Dodaj zdjecie pakietu',
                    'name' => 'bundle_image'
                ),
                array(
                    'type' => 'checkbox',
                    'label' => 'Usuń zdjęcie',
                    'name' => 'remove_bundle_image',
                     'values' => array(
                         'query' => array(
                             array( 'val' => '1'),
                             ),
                         'id' => 'id',
                         'name' => 'name')
                    
                ),
            ),
            'submit' => array('title' => 'Zapisz')
        );

        return parent::renderForm().$this->addedTrainingInBundle();
    }
        private function addedTrainingInBundle(){
          
            $trainings = $this->getBundleTrainings(Tools::getValue('id_bundle'));
            $name = '';
            foreach($trainings as $item){
                $name .= '<li>'.$item['name'].'</li>';
            }
            return '<div class="col-lg-12 list-added-trainings" >
            <p style="heading">Lista dodanych szkoleń:</p>
            <ul>'.$name.'</ul></div>';
        }
        
    public function postProcess()
    {

        if (Tools::isSubmit('submitAddbundle')) {
            $ids = $_POST['id_training'];
            $id_bundle = Tools::getValue('id_bundle');
            $id_training = Tools::getValue('id_training');
            if(!empty( $ids)) {
                Bundle::clearBundleTrainngs($id_bundle);

                foreach ($ids as $id_training) {
                    Bundle::saveTraninings($id_bundle,  $id_training);
                }
            }
        }

        if (isset($_FILES['bundle_image']) && $_FILES['bundle_image']['name'] != '') {
            
            $this->savePicture('bundle_image');
        }
       
        if(isset($_POST['remove_bundle_image_']) && $_POST['remove_bundle_image_'] == 1){

            Db::getInstance()->update('bundle', array(
               'bundle_image' => null
            ),'id_bundle = '.$this->id_object);
        }

        parent::postProcess();

    }

    private function getBundleTrainings($id_bundle){
        $result = Db::getInstance()->executeS('
        SELECT t.`name` FROM  `' . _DB_PREFIX_ . 'training` t
        WHERE t.`id_training` IN (select bt.`id_training` FROM `' . _DB_PREFIX_ . 'bundle_trainings` bt WHERE bt.`id_bundle` ='.$id_bundle.' )
        ');
        return $result;
    }

    private function savePicture($type){

        $tmpName = (isset($_FILES[$type]['tmp_name']))?$_FILES[$type]['tmp_name']:'';
        $name = (isset($_FILES[$type]['name']))?$_FILES[$type]['name']:'';
        $error = isset($_FILES[$type]['error'])?$_FILES[$type]['error']:[];
       
    
        $path = _PS_MODULE_DIR_ . 'trainings/img/b/' . $this->id_object;
       
        $newName = _PS_MODULE_DIR_ . 'trainings/img/b/' . $this->id_object . '/'.$type.'_'.$name;
        

       
        if (file_exists($path)) {
            if ($error === UPLOAD_ERR_OK) {
               
                if (!file_exists($newName)) {
                    if(file_exists($path)){
                        $files_to_remove = glob($path."/".$type."*.*");
                        foreach($files_to_remove as $item_to_remove){
                            unlink($item_to_remove);
                        }
                        
                    }
                    if (!move_uploaded_file($tmpName, $newName)) {
               
                        return false;
                    }  
                } else {
                    if(file_exists($path)){
                        $files_to_remove = glob($path."/".$type."*.*");
                        foreach($files_to_remove as $item_to_remove){
                            unlink($item_to_remove);
                        }
                    }
                    if (!move_uploaded_file($tmpName, $newName)) {
               
                        return false;
                    } 

                }
            }
 
        } else {
           
            mkdir(_PS_MODULE_DIR_ . 'trainings/img/b/' . $this->id_object, 0777);
            
             if ($error === UPLOAD_ERR_OK) {
                 if (!file_exists($newName)) {
                     if (!move_uploaded_file($tmpName, $newName)) {
                         return false;
                     } 
                 }
             }
        }

        $column_name = '';

        if($type == 'bundle_image'){
            $column_name = 'bundle_image';
        } else {
            $column_name = 'bundle_logo';
        }

        Db::getInstance()->update('bundle', array(
            $column_name => 'trainings/img/b/' . $this->id_object . '/'.$type.'_'.$name
        ), 'id_bundle = ' . $this->id_object);

    }   
}
