<?php

class AdminTrainingsInActiveBundleController extends ModuleAdminController
{
    public function __construct()
    {
        $this->table = 'bundle';
        $this->className = 'Bundle';
        $this->_where = 'AND a.`active` = 0 ';
        $this->fields_list = array(
            'id_bundle' => array('title' => 'ID', 'align' => 'left', 'width' => 45),
            'title' => array('title' => 'Tytuł')
        );

        $this->bootstrap = true;

        $this->addRowAction('edit');
        $this->addRowAction('delete');
        parent::__construct();

    }

    public function renderForm()
    {
        $query = array(
            array(
                'value' => 1,
                'label' => 'Tak'
            ),
            array(
                'value' => 0,
                'label' => 'Nie'
            )
        );
        $this->fields_form = array(
            'legend' => array('title' => 'Dodaj / edytuj pakiet szkoleń'),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => 'Tytuł',
                    'name' => 'title',
                    'required' => true
                ),
                array(
                    'type' => 'textarea',
                    'autoload_rte' => true,
                    'label' => 'Opis',
                    'name' => 'description'
                ),
                array(
                    'type' => 'select',
                    'label' => 'Szkolenia wchodzące w skład pakietu',
                    'name' => 'id_training[]',
                    'required' => true,
                    'multiple' => true,

                    'options' => array(
                        'query' => Training::getAllTrainings(),
                        'id' => 'id_training',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => 'Rabat',
                    'name' => 'discount',
                    'required' => true
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Pakiet aktywny',
                    'name' => 'active',
                    'default_value' => 0,
                    'values' => $query,
                ),
            ),
            'submit' => array('title' => 'Zapisz')

        );

        return parent::renderForm();
    }
    public function postProcess()
    {

        if (Tools::isSubmit('submitAddbundle')) {
            $ids = $_POST['id_training'];
            $id_bundle = Tools::getValue('id_bundle');
             $id_training = Tools::getValue('id_training');
           
             Bundle::clearBundleTrainngs($id_bundle);
             foreach ($ids as $id_training) {
                 Bundle::saveTraninings($id_bundle,  $id_training);
             }
            
        }

        parent::postProcess();
    }


    public function initContent()
    {

        parent::initContent();
    }
}
