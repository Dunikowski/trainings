<?php

class AdminTrainingsParticipantsController extends ModuleAdminController
{
    public function __construct()
    {
        $this->table = 'training_participant';
        $this->className = 'TrainingParticipant';

        $this->fields_list = array(
            'id_training_participant' => array('title' => 'ID', 'align' => 'left', 'width' => 45),
            'id_training' => array('title' => 'Id szkolenia'),
            'name' => array('title' => 'Imię'),
            'surname' => array('title' => 'Nazwisko'),
            'email' => array('title' => 'Email'),
            'phone' => array('title' => 'phone'),
            'rodo_ok' => array('title' => 'Potwierdzone'),
        );


        $this->bootstrap = true;

        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $context = Context::getContext();

        // $link = $context->link->getAdminLink('AdminTrainingsParticipants');
        // Tools::redirectAdmin($link . '&addparticipant');

        parent::__construct();
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::getIsset('configure')) {
            if (Tools::getIsset('viewconfiguration')) {
                /**
                 * Przekierowanie do strony edycji 
                 * szkolenia 
                 */
                $id_training = Tools::getValue('id_training');
                $token = Tools::getAdminTokenLite('AdminTrainings');
                Tools::redirectAdmin(

                    $this->context->link->getAdminLink('AdminTrainings', false) .
                        '&id_training=' . $id_training . '&updatetraining&token=' . $token
                );
            }
        }
      
    }

   
    public function renderForm()
    {

        $this->context = Context::getContext();
        $this->context->controller = $this;
        $this->fields_form = array(
            'legend' => array('title' => 'Dodaj / edytuj uczestnika'),
            'input' => array(

                array(
                    'type' => 'text',
                    'label' => 'id szkolenia na który jest zapisane',
                    'name' => 'id_training',
                    'readonly' => false
                ),
                array(
                    'type' => 'text',
                    'label' => 'id użytkownika ',
                    'name' => 'id_training_participant',
                    'readonly' => true
                ),

                array(
                    'type' => 'text',
                    'label' => 'Imię',
                    'name' => 'name',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Nazwisko',
                    'name' => 'surname',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Email',
                    'name' => 'email',


                ),
                array(
                    'type' => 'text',
                    'label' => 'Telefon',
                    'name' => 'phone'

                ),
                array(
                    'type' => 'text',
                    'label' => 'Rodo',
                    'name' => 'rodo_ok',
                    'readonly' => true
                )
            ),
            'submit' => array('title' => 'Zapisz')
        );
        return parent::renderForm() . $this->generateList() .$this->renderView();
    }
    public function renderView()
    {
        if (!$id_shop) {
            $id_shop = Context::getContext()->shop->id;
        }
        if (file_exists(_PS_IMG_DIR_.Configuration::get('PS_LOGO', null, null, $id_shop))) {
            $logo = _PS_IMG_DIR_.Configuration::get('PS_LOGO', null, null, $id_shop);
        } else {
            $logo = '';
        }
        $resive_email = TrainingParticipant::getEmailParticipant(Tools::getValue('id_training_participant'));
        $mailsDir = _PS_ROOT_DIR_ . '/modules/trainings/views/templates/mails/';
       
        if (Tools::getIsset('submitMessage') && $resive_email) {
            $message = $_POST['message'];
            $topic = $_POST['topic'];
           
             Mail::send(
                 1,
                 'contact_participant',
                 $topic,
                 array(
                     '{shop_logo}' => $logo,
                     '{email}' => Configuration::get('PS_SHOP_EMAIL'),
                     '{message}' => $message,
                     '{shop_name}' => Tools::safeOutput(Configuration::get('PS_SHOP_NAME', null, null, $id_shop)),
                     '{shop_url}' => Context::getContext()->link->getPageLink('index', true, Context::getContext()->language->id, null, false, $id_shop)
                  ),
                 $resive_email,
                 null,
                 null,
                 null,
                null,
                 null,
                 $mailsDir
             );
          
       
        }
        
        $tpl = $this->context->smarty->createTemplate(dirname(__FILE__) . '/../../views/templates/admin/custom_ps_email_form.tpl');
        // $tpl->assign(array(
  
        //     'order' =>  $message.'  lol: '.$topic
        // ));
      


        return $tpl->fetch();
    }
   
    public function generateList()
    {
        $fieldsList = array(
            'id_training' => array(
                'title' => 'Id',
                'type' => 'text'
            ),
            'name' => array(
                'title' => 'Nazwa',
                'type' => 'text'
            ),
            'price' => array(
                'title' => 'Cena',
                'type' => 'text'
            ),
            'start_time' => array(
                'title' => 'Data rozpoczęcia',
                'type' => 'text'
            ),
            'end_time' => array(
                'title' => 'Data zakończenia',
                'type' => 'text'
            ),
            'place_of_training' => array(
                'title' => 'Miejsce szkolenia',
                'type' => 'text'
            ),
            'voivodeship' => array(
                'title' => 'Województwo',
                'type' => 'text'
            ),
        );

        $trainings = TrainingParticipant::getParticipantTraining(Tools::getValue('id_training_participant'));
        $helper = new HelperList();


        $helper->shopLinkType = '';
        $helper->actions = array('view');
        $helper->identifier = 'id_training';
        $helper->show_toolbar = true;
        $helper->title = 'Lista szkoleń uczestnika';
        $helper->name = 'test';
        // $helper->no_link = true;
        // TODO
        $helper->token = Tools::getAdminTokenLite('AdminTrainings');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=';
        //$helper->currentIndex =  $this->context->link->getAdminLink('AdminTrainings', false) . ;

        return $helper->generateList($trainings, $fieldsList);
      
    }
    

    public function processSave()
    {
        return parent::processSave();
    }
}
