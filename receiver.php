<?php

require_once(dirname(__FILE__) . '../../../config/config.inc.php');
require_once(_PS_MODULE_DIR_ . 'trainings/classes/TrainingCart.php');
require_once(_PS_MODULE_DIR_ . 'trainings/trainings.php');

$sign =
    Configuration::get('TRAININGS_DOTPAY_PIN') .
    Tools::getValue('id') .
    Tools::getValue('operation_number') .
    Tools::getValue('operation_type') .
    Tools::getValue('operation_status') .
    Tools::getValue('operation_amount') .
    Tools::getValue('operation_currency') .
    Tools::getValue('operation_original_amount') .
    Tools::getValue('operation_original_currency') .
    Tools::getValue('operation_datetime') .
    Tools::getValue('control') .
    Tools::getValue('description') .
    Tools::getValue('email') .
    Tools::getValue('p_info') .
    Tools::getValue('p_email') .
    Tools::getValue('channel');

$signature = hash('sha256', $sign);

if (!$signature == Tools::getValue('signature'))
    die('signature check failed');

$cart = new TrainingCart(Tools::getValue('control'));
$cartSum = TrainingCart::getCartValue($cart->id_cart);
if ($cartSum != Tools::getValue('operation_original_amount'))
    die('cart value and original amount doesnt match');

TrainingCart::makeBought($cart->id_cart);

$status = Tools::getValue('operation_status') == 'completed' ? 1 : 0;

Db::getInstance()->execute('
UPDATE `' . _DB_PREFIX_ . 'training_order` SET `status` = ' . $status . ' WHERE `id_cart` = ' . Tools::getValue('control') . '
');

$data = array();
$data['post'] = $_POST;
$data['signature'] = $signature;
$data['cartSum'] = $cartSum;
$data['statusToSet'] = $status;

$testData = print_r($data, true);

file_put_contents(dirname(__FILE__) . '/dotpaylog.txt', $testData, FILE_APPEND);

echo "OK";
