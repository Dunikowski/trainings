<?php

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'training` (
    `id_training` int(11) NOT NULL AUTO_INCREMENT,
    `id_training_category` int(11) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `alias` VARCHAR(255) NOT NULL,
    `author` VARCHAR(255) NOT NULL,
    `id_training_instructor` INT(11) NOT NULL,
    `training_time_create` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `start_time` DATETIME NOT NULL,
    `end_time` DATETIME NOT NULL,
    `price` DECIMAL(20,2) NOT NULL,
    `discount_price` DECIMAL(20,2),
    `short_desc` TEXT,
    `long_desc` TEXT,
    `slots` INT(10) NOT NULL,
    `sms_notifications` TINYINT,
    `need_doc_number` TINYINT,
    `can_be_sold_alone` TINYINT,
    `is_promoted` TINYINT,
    `eligible_group` VARCHAR(255),
    `training_provider` VARCHAR(255),
    `dotpay_payment` TINYINT,
    `date_add` TIMESTAMP,
    `place_of_training` VARCHAR(255),
    `voivodeship` VARCHAR(255),
    `for_whom` VARCHAR(255),
    `reserve_list` TINYINT,
    `active` TINYINT,
    `image` VARCHAR(255),
    `logo` VARCHAR(255)
    PRIMARY KEY  (`id_training`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'bundle` (
    `id_bundle` int(11) NOT NULL,
    `title` varchar(255),
    `description` TEXT,
    `id_training` VARCHAR(255),
    `discount` DECIMAL(12,2) NOT NULL,
    `bundle_time_create` DATETIME DEFAULT CURRENT_TIMESTAMP
    `bundle_image` VARCHAR(255),
    `isActive` TINYINT,
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'bundle_trainings` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
        `id_bundle` int(11),
        `id_training` int(11),
        CONSTRAINT PRIMARY key (id),
        CONSTRAINT trainig_bundle_training_fk FOREIGN KEY (id_training) REFERENCES `' . _DB_PREFIX_ . 'training`(id_training) ON DELETE SET NULL,
        CONSTRAINT bundle_bundle_training_fk FOREIGN KEY (id_bundle) REFERENCES `' . _DB_PREFIX_ . 'bundle`(id_bundle) ON DELETE CASCADE

) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'training_category` (
    `id_training_category` int(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `alias` VARCHAR(255) NOT NULL,
    `id_parent` int(11) DEFAULT 0,
    `level_depth` TINYINT DEFAULT 0,
    PRIMARY KEY (`id_training_category`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'training_participant` (
    `id_training_participant` INT(11) AUTO_INCREMENT,
    `id_training` INT(11),
    `id_bundle` INT(11),
    `name` VARCHAR(255),
    `surname` VARCHAR(255),
    `email` VARCHAR(255),
    `phone` VARCHAR(20),
    `rodo_ok` TINYINT,
    `token` CHAR(32),
    `id_cart` int(11),
    `active` ENUM("on","") DEFAULT "on",
    PRIMARY KEY (`id_training_participant`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'training_cart` (
    `id_cart` INT(11) AUTO_INCREMENT,
    `id_customer` INT(11),
    `is_bought` TINYINT,
    `dotpay_payment` TINYINT,
    PRIMARY KEY(`id_cart`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'training_cart_detail` (
    `id_cart_detail` INT(11) AUTO_INCREMENT,
    `id_cart` INT(11),
    `id_training` INT(11),
    `id_bundle` INT(11),
    `training_name` VARCHAR(255),
    `quantity` INT(11),
    `price` FLOAT(20,2),
    `active` ENUM("on","") DEFAULT "on"
    PRIMARY KEY(`id_cart_detail`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'training_order` (
    `id_training_order` INT(11) AUTO_INCREMENT,
    `index` VARCHAR(255),
    `id_customer` INT(11),
    `id_cart` INT(11),
    `status` INT(11),
    PRIMARY KEY(`id_training_order`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'training_instructor` (
    `id_training_instructor` INT(11) AUTO_INCREMENT,
    `name` VARCHAR(255),
    `surname` VARCHAR(255),
    `description` TEXT,
    `image` VARCHAR(255),
    PRIMARY KEY(`id_training_instructor`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';


$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'training_reserve_list` (
    `id_training_reserve_list` INT(11) AUTO_INCREMENT,
    `id_training` INT(11),
    `firstname` VARCHAR(255),
    `lastname` VARCHAR(255),
    `email` VARCHAR(255),
    `telephone` VARCHAR(255),
    PRIMARY KEY(`id_training_reserve_list`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';


$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'training_cart_tmp` (
    `id_training_cart_tmp` INT(11) AUTO_INCREMENT,
    `id_cart` INT(11),
    `id_training` INT(11),
    `id_bundle` INT(11),
    `training_name` VARCHAR(255),
    `quantity` INT(11),
    `price` FLOAT(20,2),
    PRIMARY KEY(`id_training_cart_tmp`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
